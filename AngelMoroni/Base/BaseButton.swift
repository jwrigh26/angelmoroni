//
//  BaseButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/18/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class BaseButton: UIButton, LatencyIndicatable {
  var indicatorStyle:UIActivityIndicatorViewStyle = .white
  
  var backgroundLayer: CALayer?
  var highlightLayer: CALayer?
  var disabledLayer: CALayer?
  
  var textColor: UIColor!
  var backgroundLayerColor: UIColor!
  var highlightLayerColor: UIColor!
  var disabledLayerColor: UIColor!
  
  var indicator: UIActivityIndicatorView?
  
  override var isUserInteractionEnabled: Bool {
    didSet {
      CATransaction.begin()
      CATransaction.setDisableActions(true)
      disabledLayer?.isHidden = isUserInteractionEnabled
      backgroundLayer?.isHidden = !isUserInteractionEnabled
      CATransaction.commit()
      //showIndicator(show: !isUserInteractionEnabled)
    }
  }
  
  override var isEnabled: Bool {
    didSet {
      isUserInteractionEnabled = isEnabled
    }
  }
  
  override var isHighlighted: Bool {
    didSet {
      CATransaction.begin()
      CATransaction.setDisableActions(true)
      highlightLayer?.isHidden = !isHighlighted
      CATransaction.commit()
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
    
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  override func awakeFromNib() {
    guard buttonType == .custom else { fatalError("Oops, you're attempting to use BaseButton but forgot to set the button Type to Custom in the Storyboard or Nib file. Please set type to custom and try again. Thank you!") }
    super.awakeFromNib()
    
  }
  
  override func layoutSubviews() {
    backgroundLayer?.frame = self.bounds
    highlightLayer?.frame = self.bounds
    disabledLayer?.frame = self.bounds
    super.layoutSubviews()
  }
  
  func setButtonTitle(title: String, font: UIFont = ButtonFonts.normal(size: 18).font){
    titleLabel?.font = font
    setTitleWithOutAnimation(title: title)
  }
  
  func setup(){
    setColors()
    
    setTitleColor(textColor, for: .normal)
    setTitleColor(textColor, for: .highlighted)
    setTitleColor(textColor, for: .disabled)
    
    self.contentEdgeInsets = UIEdgeInsetsMake(14, 16, 14, 16)
    drawBackgroundLayer()
    drawHighlightLayer()
    drawDisabledLayer()
    //addIndicator()
  }
  
  func setColors(){
    textColor = GameColor.black.color
    backgroundLayerColor = GameColor.black.color
    highlightLayerColor = GameColor.black.color
    disabledLayerColor = GameColor.black.color(alpha: 0.6)
  }
  
  func drawBackgroundLayer(){
    guard backgroundLayer == nil else { return }
    backgroundLayer = drawColorLayer(color: backgroundLayerColor, index: 0)
  }
  
  func drawHighlightLayer(){
    guard highlightLayer == nil else { return }
    highlightLayer = drawColorLayer(color: highlightLayerColor, index: 1)
    highlightLayer?.isHidden = true
  }
  
  func drawDisabledLayer(){
    guard disabledLayer == nil else { return }
    disabledLayer = drawColorLayer(color: disabledLayerColor, index: 1)
    disabledLayer?.isHidden = true
  }
  
  func drawColorLayer(color: UIColor, index: Int) -> CALayer {
    let layer = CALayer()
    layer.backgroundColor = color.cgColor
    layer.cornerRadius = 5
    self.layer.insertSublayer(layer, at: UInt32(index))
    
    return layer
  }
  
  // MARK: DSGLatency
  
  
  func layoutIndicator(indicator: UIActivityIndicatorView?){
    guard let indicator = indicator else { return }
    indicator.translatesAutoresizingMaskIntoConstraints = false
    let constraints: [NSLayoutConstraint] = [
      indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
      indicator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
    ]
    NSLayoutConstraint.activate(constraints)
    print("We did layout for inidcator")
  }
  
  func addIndicator() {
    guard let indicator = createIndicator(style: indicatorStyle) else { return }
    addSubview(indicator)
    layoutIndicator(indicator: indicator)
    self.indicator = indicator
  }
  
  func createIndicator()->UIActivityIndicatorView? {
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
    indicator.alpha = 1.0
    indicator.hidesWhenStopped = true
    indicator.sizeToFit()
    return indicator
  }
  
  func showIndicator(show: Bool){
    if show {
      self.startIndicator()
    }else{
      self.stopIndicator()
    }
  }
  
}

enum ButtonFonts {
  
  case normal(size: CGFloat)
  
  var font: UIFont {
    
    var font: UIFont?
    switch self {
    case .normal(let size):
      font = GameFont.pixelIntv.font(size: size)
    }
    
    return font!
  }
}

extension UIButton {
  func setTitleWithOutAnimation(title: String?) {
    UIView.setAnimationsEnabled(false)
    
    setTitle(title, for: .normal)
    
    layoutIfNeeded()
    UIView.setAnimationsEnabled(true)
  }
}

class BaseTextButton: BaseButton {
  
  
  var textHighlightColor: UIColor!
  var textDisabledColor: UIColor!
  
  override var isUserInteractionEnabled: Bool {
    didSet {
      CATransaction.begin()
      CATransaction.setDisableActions(true)
      // Custom code goes here
      CATransaction.commit()
      //showIndicator(show: !isUserInteractionEnabled)
    }
  }
  
  override var isHighlighted: Bool {
    didSet {
      CATransaction.begin()
      CATransaction.setDisableActions(true)
      // Custom code goes here
      CATransaction.commit()
    }
  }
  
  override func setColors(){
    backgroundLayerColor = UIColor.clear
    highlightLayerColor = UIColor.clear
    disabledLayerColor = UIColor.clear
    textColor = GameColor.black.color
    textHighlightColor = GameColor.black.color
    textDisabledColor = GameColor.black.color(alpha: 0.6)
  }
  
  override func setup() {
    super.setup()
    indicatorStyle = .gray
    
    setTitleColor(textColor, for: .normal)
    setTitleColor(textHighlightColor, for: .highlighted)
    setTitleColor(textDisabledColor, for: .disabled)
  }
  
  
  override func drawBackgroundLayer(){
    return
  }
  
  override func drawHighlightLayer(){
    return
  }
  
  override func drawDisabledLayer(){
    return
  }
  
}





/*
 Example of how to add it by code
 
 func addTestButton(){
 let button = DSGButton(frame: CGRectZero)
 view.addSubview(button)
 button.setButtonTitle("Test Button")
 button.sizeToFit()
 button.translatesAutoresizingMaskIntoConstraints = false
 button.trailingAnchor.constraintEqualToAnchor(view.trailingAnchor, constant: -32).active = true
 button.leadingAnchor.constraintEqualToAnchor(view.leadingAnchor, constant: 32).active = true
 button.topAnchor.constraintEqualToAnchor(view.topAnchor, constant: 64).active = true
 
 
 button.rx_tap.subscribeNext{ [weak self] _ in
 guard let me = self else { return }
 me.buyButton.userInteractionEnabled = !me.buyButton.userInteractionEnabled
 
 }.addDisposableTo(disposeBag)
 }
 
 */
