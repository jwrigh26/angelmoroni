//
//  BaseView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/17/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

protocol CustomView: class {
  var view: UIView { get }
  
  // Let's a view controller that is subscribed to
  // a touch up inside event know that it's been touched.
  // called in touches ended
  
  func sendAction(_ touches: Set<NSObject>)
  func touchInside(_ touches: Set<NSObject>) -> Bool
  func xibSetup()
  func prepViewLayout()
  func loadViewFromNib(_ nibName: String) -> UIView
  func loadViewFromNibAndPrepViewLayout(_ nibName: String)
}

extension CustomView {
  
  func loadViewFromNib(_ nibName: String) -> UIView {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: nibName, bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
  }
}


class BaseControl: UIControl, CustomView {
  
  var view: UIView = UIView()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    xibSetup()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    xibSetup()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.backgroundColor = UIColor.clear
    view.backgroundColor = UIColor.clear
  }
  
  func xibSetup() {
    // Need to override
    // Example below
    // view = loadViewFromNib(nibName)
    // view.frame = bounds
    // view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, .FlexibleHeight]
    // addSubview(view)
  }
  
  func loadViewFromNibAndPrepViewLayout(_ nibName: String) {
    view = loadViewFromNib(nibName)
    prepViewLayout()
  }
  
  func prepViewLayout() {
    view.frame = bounds
    view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, .flexibleHeight]
    addSubview(view)
  }
  
  func sendAction(_ touches: Set<NSObject>) {
    //
  }
  
  func touchInside(_ touches: Set<NSObject>) -> Bool {
    return false
  }
}
