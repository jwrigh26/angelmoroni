//
//  BaseViewController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import SpriteKit
import SafariServices
import Crashlytics

typealias CallBackHandler = () -> Void


class BaseGameViewController: UIViewController, NotificationListener, SFSafariViewControllerDelegate {
  
  let transition = SKTransition.fade(withDuration: 0.75)
  let device = Device()
  var skView: SKView!
  var didSetupConstraints = false
  var notificationGameState: NSNotificationProxy?
  var notificationGameNotActive: NSNotificationProxy?
  var notificationGameActive: NSNotificationProxy?
  var notificationLaunchSafari: NSNotificationProxy?
  var notificationGameKitAuthentication: NSNotificationProxy?
  var notificationGKCenter: NSNotificationProxy?
  var gameState: GameState {
    get {
      return GameManager.sharedInstance.gameState
    }
    set {
      GameManager.sharedInstance.gameState = newValue
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureSKView()
    listenForNotifications()
  }
  
  func listenForNotifications(){
    guard notificationGameState == nil
      && notificationGameNotActive == nil
      && notificationGameActive == nil
      && notificationLaunchSafari == nil
      && notificationGKCenter == nil
      && notificationGameKitAuthentication == nil else {
      notificationGameState?.stop()
      notificationGameNotActive?.stop()
      notificationGameActive?.stop()
      notificationLaunchSafari?.stop()
      notificationGKCenter?.stop()
      notificationGameKitAuthentication?.stop()
      return
    }
    
    notificationGameNotActive = NSNotificationProxy(notificationKey: Notification.AppResignedActive){ [weak self] sender in
      self?.gameNotActive()
    }
    
    notificationGameActive = NSNotificationProxy(notificationKey: Notification.AppDidBecomeActive){ [weak self] sender in
      self?.gameActive()
    }
    
    notificationGameState = NSNotificationProxy(notificationKey: Notification.GameState){
      [weak self] sender in
      guard let info = (sender as NSNotification).userInfo as? [String : Int],
      let state = info[Notification.GameStateKey],
        let gameState = GameState(rawValue: state) else {
          return
      }
      self?.transitionByState(gameState)
     
    }
    
    notificationLaunchSafari = NSNotificationProxy(notificationKey: Notification.launchSafari){ [weak self] sender in
      guard let info = (sender as NSNotification).userInfo as? [String : String],
        let url = info[Notification.UrlKey] else { return }
      self?.launchSafariVC(withURL: url)
    }
    
    notificationGKCenter = NSNotificationProxy(notificationKey: Notification.presetnGKGameCenterViewController){ [weak self] _ in
      guard let me = self else { return }
      GameKitHelper.sharedInstance.showGKCenterViewController(viewController: me)
    }
    
    notificationGameKitAuthentication = NSNotificationProxy(notificationKey: Notification.presentAuthenticationViewController){ [weak self] _ in
      self?.showAuthenticationViewController()
    }
    GameKitHelper.sharedInstance.authenticateLocalPlayer()
  }
  
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
  }
  
  override var shouldAutorotate : Bool {
    return true
  }
  
  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    if UIDevice.current.userInterfaceIdiom == .phone {
      return UIInterfaceOrientationMask.allButUpsideDown
    } else {
      return UIInterfaceOrientationMask.landscape
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Release any cached data, images, etc that aren't in use.
  }
  
  override var prefersStatusBarHidden : Bool {
    return true
  }

  override func updateViewConstraints() {
    //if !didSetupConstraints {
      setupConstraints()
    //}
    super.updateViewConstraints()
  }
  
  
  
  func transitionByState(_ state: GameState) {
    // Override me please
  }
  
  func setupConstraints(){}
  
  func configureSKView(){
    guard skView == nil else { return }
    skView = self.view as! SKView
    GameManager.sharedInstance.skView = skView
    skView.showsFPS = false
    
    if #available(iOS 10.0, *) {
      skView.preferredFramesPerSecond = 60
    } else {
      // Fallback on earlier versions
      skView.frameInterval = 1
    }
    skView.showsNodeCount = false
    skView.showsDrawCount = false
    
    /* Sprite Kit applies additional optimizations to improve rendering performance */
    skView.ignoresSiblingOrder = true
    
  }
  
  func configureScene(_ scene: SKScene) -> SKScene?{
    guard let skView = self.skView else { return nil}
    
    let size = skView.bounds.size
    /* Set the scale mode to scale to fit the window */
    scene.scaleMode = .aspectFill
    scene.size = size
    return scene
  }
  
  func preloadResources(_ handler: @escaping CallBackHandler){}
  
  func gameNotActive(){}
  func gameActive(){}
  
  func launchSafariVC(withURL url: String){
    let svc = SFSafariViewController(url: URL(string: url)!, entersReaderIfAvailable: true)
    svc.delegate = self
    if #available(iOS 10.0, *) {
      //svc.preferredBarTintColor = GameColor.cloudDarkBlue.color
      svc.preferredControlTintColor = GameColor.cloudDarkBlue.color
    }
    self.present(svc, animated: true, completion: nil)
    
    Answers.logCustomEvent(withName: GameAnalytics.safariView, customAttributes: ["url" : url])
  }
  
  func safariViewControllerDidFinish(_ controller: SFSafariViewController) {}
  
  func showAuthenticationViewController() {
    let gameKitHelper = GameKitHelper.sharedInstance
    if let authVC = gameKitHelper.authenticationViewController {
      self.present(authVC, animated: true, completion: nil)
    }
  }
}
