//
//  BaseModal.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/25/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class BaseModal: BaseView {
  
  @IBOutlet weak var bannerTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var scriptureLabel: UILabel!
  @IBOutlet weak var topView: UIView!
  
  let black =  GameColor.black.color
  let gray =  GameColor.darkGray.color
  
  let navManager = NavigationManager.sharedInstance
  
  let device = Device()
  
  convenience init(parent view: UIView){
    self.init(frame: CGRect.zero)
    self.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(self)
    layoutConstraints()
    
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  override func setup() {
    super.setup()
    view.backgroundColor = UIColor.clear
    topView.backgroundColor = UIColor(netHex: GameColor.black.rawValue)
    
    if let level = NavigationManager.sharedInstance.currentLevelPath?.level {
      scriptureLabel.text = level.title
    }else{
      scriptureLabel.text = ""
    }
    
  }
  
  // No ryhme or reason just tested and found out what adjustments look good for different screen types
  override func layoutConstraints(){
    guard let parent = self.superview else { fatalError("parent has not been added") }
    let container = parent.layoutMarginsGuide
    let width: CGFloat = device.isSmallScreen ? 0.90 : 0.74
    let height: CGFloat = device.isXLargeScreen ? 0.54 :  device.isSmallScreen ? 0.72 : 0.68
    self.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: width).isActive = true
    self.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: height).isActive = true
    self.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
    self.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
    
    let topOffset: CGFloat = device.isXLargeScreen ? -60 : device.isSmallScreen ? -38 : -30
    bannerTopConstraint.constant = topOffset
    self.clipsToBounds = false
  }
  
  override func draw(_ rect: CGRect) {
    var path = UIBezierPath(rect: rect)
    gray.setFill()
    path.fill()
    
    path = UIBezierPath(rect: CGRect(x: 0, y: rect.maxY - 2, width: rect.width, height: 2.0))
    black.setFill()
    path.fill()
    
    path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 2.0, height: rect.height))
    path.fill()
    
    path = UIBezierPath(rect: CGRect(x: rect.maxX - 2, y: 0, width: 2.0, height: rect.height))
    path.fill()
  }
  
}
