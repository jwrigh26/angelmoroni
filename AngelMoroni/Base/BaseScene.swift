//
//  BaseScene.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/12/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

class BaseScene: SKScene, SceneProtocol {
  
  let backgroundOriginalColor = UIColor(netHex: GameColor.skyBlue.rawValue)
  var layers: [SKNode?] = [SKNode]()
  var blackLayer: ColorLayer?
  
  var gameState: GameState {
    get {
      return GameManager.sharedInstance.gameState
    }
    set {
      GameManager.sharedInstance.gameState = newValue
    }
  }
  
  deinit {}
  
  var enableUpdateLoop: Bool {
    var bool = false
    switch gameState {
    case .start, .worldNoAnimate, .world, .level, .playing, .angelFallen, .levelComplete:
     bool = true
    default:
      break
    }
    
    return bool
  }
  
  override init(size: CGSize) {
    super.init(size: size)
    listenForNotifications()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func didMove(to view: SKView){
    self.isUserInteractionEnabled = true // allows for touch
    // Color for sky #3cbcfd
    //backgroundColor = ColorUtils.colorFromHex(0x3cbcfd, alpha: 1.0)
    backgroundColor = backgroundOriginalColor
  }
  
  func doNotContinueDidMoveToView() -> Bool {
    for layer in layers {
      if layer == nil {
        print("Cannot continue. A layer \(layer) is nil")
        return false
      }
    }
    return true
  }
  
  func didMoveToViewDelay(_ hander: @escaping BasicHandler){
    delay(0.35){
      hander()
    }
  }
  
  func addBlackLayer(){
    blackLayer = ColorLayer(color: UIColor.black)
    blackLayer!.zPosition = 99
    addChild(blackLayer!)
    
    let wait = SKAction.wait(forDuration: 0.25)
    let block = SKAction.run{ [weak self] in
      self?.removeBlackLayer()
    }
    run(SKAction.sequence([wait, block]))
  }
  
  func removeBlackLayer(){
    if blackLayer != nil {
      blackLayer!.removeFromParent()
      blackLayer = nil
    }
  }
  
  var notificationPaused: NSNotificationProxy?
  
  func listenForNotifications(){
    guard notificationPaused == nil  else {
      notificationPaused?.stop()
      return
    }
    
    notificationPaused = NSNotificationProxy(notificationKey: Notification.GamePaused){ [weak self] sender in
      self?.onPause()
    }
  }
  
  func onPause(){}
}
