//
//  BaseView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class BaseView:UIView, CustomXibView {
  
  var view: UIView = UIView()
  
  required init?(coder aDecoder: NSCoder){
    super.init(coder: aDecoder)
    xibSetup()
  }
  
  override init(frame: CGRect){
    super.init(frame: frame)
    xibSetup()
    setup()
  }
  
  override func awakeFromNib(){
    super.awakeFromNib()
    setup()
  }
  
  
  func xibSetup() {
    // Override
    // this is where you would load your xib file etc
  }
  
  func setup(){
    // This is where you style everything
  }
  
  
  func layoutConstraints(){
    guard let parent = self.superview else { fatalError("parent has not been added") }
    let container = parent.layoutMarginsGuide
    self.topAnchor.constraint(equalTo: container.topAnchor, constant: 0).isActive = true
    self.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
    self.leftAnchor.constraint(equalTo: container.leftAnchor).isActive = true
    self.rightAnchor.constraint(equalTo: container.rightAnchor).isActive = true
    
  }
  
}
