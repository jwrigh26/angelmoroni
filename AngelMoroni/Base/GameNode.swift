//
//  GameNode.swift
//  HelloWorldSwift
//
//  Created by Wright, Justin M on 8/30/15.
//  Copyright © 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

class GameNode: SKNode, NodeProtocol, SpriteProtocol, CleanupProtocol, Traceable {
  
  var screenSize: CGSize = GameManager.sharedInstance.screenSize
  var dirtyNode: SKSpriteNode!
  var maxPointsPerSecond:CGPoint
  var nodeBounds: CGRect!
  var nodePosition: CGPoint!
  
  fileprivate(set) var pointsPerSecond = CGPoint.zero
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override init() {
    maxPointsPerSecond = CGPoint(x: screenSize.height / 2, y: 0)
    pointsPerSecond = CGPoint(x: 100 , y: 0)
    dirtyNode = GameSprite()
    nodeBounds = CGRect.zero
    nodePosition = CGPoint.zero
    super.init()
  }
  
  var updateTime: CFTimeInterval = 0
  func update(_ dt: CFTimeInterval) {
    
    if updateTime > 1000 {
      updateTime = 0
    }
    
    updateTime += dt*1000
  }
  
  func addParent(_ parent: SKNode){
    print("Being called on Main thread \(Thread.isMainThread)")
    parent.addChild(self)
  }
  
  func cleanup() {}
  
  func boundingBox() -> CGRect {
    return CGRect.zero
  }
  
  func traceHitBox() {
    let shape = makeTraceShape(boundingBox())
    shape.position = CGPoint(x: 0, y: 0)
    self.addChild(shape)
  }
  
  
  func didEvaluateActions() {}
  
}


//extension CGFloat {
//  /// Rounds the double to decimal places value
//  func roundToPlaces(_ places:Int) -> CGFloat {
//    let divisor = pow(10.0, CGFloat(places))
//    var n = self
//    return round(n * divisor) / divisor
//  }
//}

