//
//  GameSprite.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 7/4/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit
typealias TextureHandler = () -> SKTexture
typealias stateSKAction = (action: SKAction?, key: String)

class GameSprite: SKSpriteNode, SpriteProtocol, CleanupProtocol {
  
  static let gameSheet = GameManager.sharedInstance.gameSheet

  var screenSize: CGSize = GameManager.sharedInstance.screenSize
  
  var characterHealth: Int = 0
  
  var dirtyNode: SKSpriteNode!
  
  
  var isPlaying: Bool {
    let state = GameManager.sharedInstance.gameState
    return state == .playing || state == .startingGame
  }
  
  fileprivate var _characterState:CharacterState = .none
  
  var characterState:CharacterState {
    get { return _characterState }
    set {
      
      if _characterState != newValue && isPlaying {
        _characterState = newValue
        changeCharacterState(newValue)
      }
    }
  }
  
  func addToParent(_ parent: SKNode){
    print("Being called on Main thread \(Thread.isMainThread)")
    parent.addChild(self)
  }
  
  func update(_ dt: CFTimeInterval){}
  
  func overrideCharacterState(_ state: CharacterState){
    _characterState = state
    changeCharacterState(state)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(texture: SKTexture?, color: UIColor, size: CGSize) {
    super.init(texture: texture, color: color, size: size)
    
  }
  
  init(texture: SKTexture, position: CGPoint){
    texture.filteringMode = SKTextureFilteringMode.nearest
    super.init(texture: texture, color: SKColor.white, size: texture.size())
    self.setScale(Device.Scale)
    self.position = position
  }
  
  // This method is called from the computative variable "characterState".
  // It calls the getCharacterStateSKAction and then runs the action
  func changeCharacterState(_ newState: CharacterState) {
    self.removeAllActions()
    let tuple = getCharacterStateSKAction(newState)
    if let action = tuple.0 {
      GlobalMainQueue.async{
        self.run(action, withKey: tuple.1)
      }
    }
  }
  
  // This is meant to be overriden and basically is the workhorse for
  // updating UI changes and applying actions to a sprite
  // stateSKAction: (SKAction, String) *String is the key for the action
  func getCharacterStateSKAction(_ state: CharacterState) -> stateSKAction {
    //fatalError("Need to override runCharacterState action")
    return (nil, "")
  }
  
  func didEvaluateActions(){}
  
  func cleanup() {}
  
  func boundingBox() -> CGRect { return self.frame } // Used in place of frame. May be modified for tweaking.
}
