//
//  Constants.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

// These are the cases where the loop is running
// .Start, .WorldNoAnimate, .World, .Level, .Playing

enum GameState: Int{
  
  case none
  case start // Start of the app
  case startingGame
  case worldNoAnimate
  case world
  case level
  case levelComplete
  case playing
  case angelFallen
  case paused
  case resume
  case retry
  case gameOver
  case levelSummary
  case returnToMenu
  case nextLevel
}

// These ones we update on
//case .Start, .WorldNoAnimate, .World, .Level, .Playing, .AngelFallen, .LevelComplete:


enum GameColor: Int {
  case black = 0x111111
  case darkGray = 0x343434
  case medGray = 0x7b8b88
  case lightGray = 0xa7bcb7
  case yellow = 0xffde00
  case lightYellow = 0xf5f531
  case orange = 0xff9f1a
  case bannerRed = 0x731f1f
  case darkBannerRed = 0x2d0c0c
  case medBannerRed = 0x571717
  case lightBannerRed = 0x8f2727
  case cloudWhite = 0xf8f8f8
  case skyBlue = 0x3cbcfd
  case cloudGray = 0xeeeeee
  case cloudLightBlueHighlight = 0x78d1fd
  case cloudLightBlue = 0x3095ca
  case cloudMedBlueHighlight = 0x78aeca
  case cloudMedBlue = 0x448cb2
  case cloudDarkBlueHighlight = 0x26759f
  case cloudDarkBlue = 0x1c5776
  case darkBlue = 0x12374c
  case hartRed = 0xee0e2b
  case brown = 0x43280B
  
  var hex: Int {
    return self.rawValue
  }
  
  var color: UIColor {
    return UIColor(netHex: self.hex)
  }
  
  func color(alpha: Double) -> UIColor {
    return GameColor.color(rgbValue: UInt32(self.rawValue), alpha: alpha)
  }
  
  static func color(rgbValue: UInt32, alpha: Double = 1.0) -> UIColor {
    let red = CGFloat( (rgbValue & 0xFF0000) >> 16) / 256.0
    let green = CGFloat( (rgbValue & 0xFF00) >> 8) / 256.0
    let blue = CGFloat(rgbValue & 0xFF) / 256.0
    return UIColor(red: red, green: green, blue: blue, alpha: CGFloat(alpha))
  }
}

enum Notification {
  // Used in GameViewController base class and posted from NavigationManager
  static let GameState = "GameNotificationGameState"
  static let GameStateKey = "GameNotificationGameStateKey"
  static let UrlKey = "url"
  
  // Used in ScriptureMaster and posted from ScriptureBitNode
  static let ScriptureBitCollected = "GameNotifiationSciptureBitCollected"
  static let ScriptureBitCollectedKey = "GameNotifiationSciptureBitCollectedKey"
  static let EnemyHitDetected = "EnemyHitDetected"
  static let EnemyHitDetectedKey = "EnemyHitDetectedKey"
  static let SummaryStartAnimationComplete = "SummaryStartAnimationComplete"
  static let SummaryStartAnimationCompleteKey = "SummaryStartAnimationCompleteKey"
  
  static let GamePaused = "GamePausedNotification"
  static let AppDidBecomeActive = "AppDidBecomeActiveNotification"
  static let AppResignedActive = "AppResignedActiveNotification"
  static let RemoveRocket = "RemoveRocketNotification"
  static let launchSafari = "GameLaunchSafariViewControllerNotifiation"
  
  static let presetnGKGameCenterViewController = "PresetnGKGameCenterViewController"
  static let presentAuthenticationViewController = "PresentAuthenticationViewController"
  
}

enum ResourceFile: String {
  case NONE = "None"
  case AofF = "ArticlesOfFaith"
  case BoM = "BookOfMormon"
  case OldTest = "OldTestament"
  case NewTest = "NewTestament"
  case Doc = "DoctrineAndConvenants"
}

enum DataError: Error {
  case badEncoding
  case general
  case fetch
  case delete
  case parse
}

enum GameError: Error {
  case typeCasting
  case nullObject
  case arrayMismatch
  case layoutIssue
}


enum Material: String {
  case NONE = "none"
  case STONE_LIGHT = "light"
  case STONE_DARK = "dark"
}

enum CommonStrings {
  static let scriptureBits = "Scripture Bits"
  static let BestBits = "Best Bits"
  static let Bits = "Bits"
  static let DegreesGlory = "Degrees of Glory"
  
}

enum DegreeOfGlory: Int {
  case none = 0
  case star = 1
  case moon = 2
  case sun = 3
}


enum ParseError: Error {
  case general(message: String)
  case missingAttribute(message: String)
}


struct Health {
  static let Max: Int = 3
  static let Min: Int = 0
}

struct ZPos {
  static let Angel: CGFloat = 1
  static let Scripture: CGFloat = 1
  static let Particle: CGFloat = 1
  static let Enemy: CGFloat = 1
  static let HitBox: CGFloat = 99
}


final class GameAnalytics {
  static let appStart = "App Start"
  static let safariView = "SafariVC Launched"
  static let gkView = "GameKitVC Launched"
  static let gkAuth = "GameKit Authentication"
  static let levelComplete = "Level Complete"
  
  static func degree(_ value: Int) -> String {
    switch value {
    case DegreeOfGlory.star.rawValue:
      return "Star"
    case DegreeOfGlory.moon.rawValue:
      return "Moon"
    case DegreeOfGlory.sun.rawValue:
      return "Sun"
    default:
      return "None"
    }
  }
  
}

