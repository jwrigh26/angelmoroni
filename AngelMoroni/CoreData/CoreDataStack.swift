//
//  CoreDataStack.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/31/15.
//  Copyright © 2015 Justin Wright. All rights reserved. AngelMoroniDataModel
//

import UIKit
import CoreData


final class CoreDataStack: NSObject {
  
  static let moduleName = "AngelMoroniDataModel"
  
  var handler: BasicHandler?
  
  init(handler: BasicHandler?) {
    self.handler = handler
    super.init()
    let _ = self.managedObjectContext
  }
  
  func assignCoreDataStackToRoot(_ window: UIWindow?){
    // Pass the Baton!
    if let _ = window?.rootViewController as? CoreDataReady {
      var temp = (window?.rootViewController as! CoreDataReady)
      temp.coreDataStack = self
    }

  }
  
  
  func save() {
    guard managedObjectContext.hasChanges || privateManagedObjectContext.hasChanges else {
      return
    }
    
    //Log.verbose?.message("Going to save now")
    
    managedObjectContext.performAndWait() {
      do {
        try self.managedObjectContext.save()
      } catch {
        fatalError("Error saving main managed object context! \(error)")
      }
    }
    
    privateManagedObjectContext.perform() {
      do {
        try self.privateManagedObjectContext.save()
      } catch {
        fatalError("Error saving private managed object context! \(error)")
      }
    }
    
  }
  
  func saveChildContext(_ child: NSManagedObjectContext){
    guard child.hasChanges else { return }
    do {
      try child.save()
      self.save()
    } catch {
      fatalError("Error saving child context \(error)")
    }
    
    
  }
  
  func spawnChildContext(_ context: NSManagedObjectContext, isPrivate: Bool = false) -> NSManagedObjectContext{
    
    let concurrencyType: NSManagedObjectContextConcurrencyType = isPrivate ? .privateQueueConcurrencyType : .mainQueueConcurrencyType
    let child = NSManagedObjectContext(concurrencyType: concurrencyType)
    child.parent = context
    return child
  }
  
  lazy var managedObjectModel: NSManagedObjectModel = {
    let modelURL = Bundle.main.url(forResource: moduleName, withExtension: "momd")!
    return NSManagedObjectModel(contentsOf: modelURL)!
  }()
  
  lazy var applicationDocumentsDirectory: URL = {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
  }()
  
  lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
    
    let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    let persistentStoreURL = self.applicationDocumentsDirectory.appendingPathComponent("\(moduleName).sqlite")
    
    // This could take a while so we return right away but notify when this is done laters.
    // http://martiancraft.com/blog/2015/03/core-data-stack/
    GlobalBackgroundQueue.async{ [weak self] in
      
      do {
        try coordinator.addPersistentStore(ofType: NSSQLiteStoreType,
          configurationName: nil,
          at: persistentStoreURL,
          options: [NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: false])
        if self?.handler != nil {
          self?.handler?()
        }
      } catch {
        fatalError("Persistent store error! \(error)")
      }
      
    }
    
    return coordinator
  }()
  
  fileprivate lazy var privateManagedObjectContext: NSManagedObjectContext = {
    let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    moc.persistentStoreCoordinator = self.persistentStoreCoordinator
    return moc
  }()
  
  lazy var managedObjectContext: NSManagedObjectContext = {
    let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    
    // Commenting out  #1 and unCommening #2 prevents the leak from happening
    // However when the reverse happens and we create the private context, a memory leak occurs first thing in the app.
    
    // #1
    // managedObjectContext.parentContext = self.privateManagedObjectContext
    
    // And instead replace it with this line
    // #2
    managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
    
    
    return managedObjectContext
  }()
  
}
