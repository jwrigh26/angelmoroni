//
//  DataRouter.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/21/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import CoreData

enum DataRouter {
  
  case worlds(context: NSManagedObjectContext)
  case user(context: NSManagedObjectContext, name: String) // e.g. context, localUser
  
  
  var Request: NSFetchRequest<NSManagedObject> {
    
    let (context, description, predicate): (NSManagedObjectContext, String, NSPredicate?) = {
      switch self {
      case .worlds(let context):
        return (context, "World", nil)
      case .user(let context, let name):
        let predicate = NSPredicate(format: "name == %@", name)
        return (context, "User", predicate)
      }
    }()
    
    let entity = NSEntityDescription.entity(forEntityName: description, in: context)
    let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: description)
    //let fetch = NSFetchRequest()
    //let fetch = NSFetchRequest(entityName: description)
    //let fetc = NSFetchRequest()
    fetch.entity = entity
    if let predicate = predicate {
      fetch.predicate = predicate
    }
    return fetch as! NSFetchRequest<NSManagedObject>
  }
  
}
