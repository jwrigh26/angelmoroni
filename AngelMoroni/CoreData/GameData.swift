//
//  DataManager.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/5/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import CoreData


final class GameData: NSObject {
  
  static let sharedInstance = GameData()
  
  
  var currentUser: User?
  var worlds = [World]()
  
  func seedDataIfNeeded(_ stack: CoreDataStack, handler: @escaping BasicHandler){
    
    let context = stack.managedObjectContext
    context.performAndWait{ [weak self] in
      do {
        try self?.importJSONSeedDataIfNeeded(context)
        try self?.importLocalUserIfNeeded(context)
        print("About to call handler")
        handler()
      } catch {
        print("Error seeding data \(error)")
        context.rollback()
        handler()
      }
    }
  }
  
  func importLocalUserIfNeeded(_ context: NSManagedObjectContext) throws {
    let userName = "localUser"
    let fetch = DataRouter.user(context: context, name: userName)
    do {
      let results = try context.fetch(fetch.Request) as! [User]
      if results.count > 0 {
        currentUser = results.first
      }else{
        let userEntity = NSEntityDescription.entity(forEntityName: "User", in: context)
        currentUser = User(entity: userEntity!, insertInto: context)
        currentUser?.name = userName
      }
    }catch let error as NSError {
      print("ImportLocalUser Err: \(error) and description \(error.description)")
      throw DataError.import
    }
  }
  
  func importJSONSeedDataIfNeeded(_ context:NSManagedObjectContext) throws {
    
    // See if we have worlds and levels
    let fetch =  DataRouter.worlds(context: context)
    
    let results = try! context.count(for: fetch.Request)
    if results == 0 {
      // We have no words
      do {
        let worldEntity = NSEntityDescription.entity(forEntityName: "World", in: context)
        let levelEntity = NSEntityDescription.entity(forEntityName: "Level", in: context)
        let json = try parseJSON()
        
        for (_, wrld):(String, JSON) in json["worlds"] {
          
          // Create values for world variables
          let number = wrld["number"].numberValue.int16Value
          let name = wrld["name"].stringValue
          let total = wrld["totalLevels"].numberValue.int16Value
          let locked = wrld["locked"].boolValue
          let image = wrld["image"].stringValue
          
          
          // Create world
          let world = World(entity: worldEntity!, insertInto: context)
          world.name = name
          world.number = number
          world.totalLevels = total
          world.locked = locked
          world.image = image
          
          
          // Make levels for world and add to set
          var levels = Set<Level>()
          for (_, lvl):(String, JSON) in wrld["levels"] {
            let number = lvl["number"].numberValue.int16Value
            let level = Level(entity: levelEntity!, insertInto: context)
            level.number = number
            level.world = world
            levels.insert(level)
          }
          
          // Finally assign levels to world and make a sorted array for use
          world.level = levels as NSSet?
          world.sortLevels(levels)
          worlds.append(world)
        }
        
        
        
      }catch{
        print("Error has occured \(error)")
        throw DataError.parse
      }
    }else{
      // We need to get the worlds
      do {
        
        // This mess of a code is to avoid a memory leak of force unwrapping an obj-c NSArray
        let i = try context.fetch(fetch.Request)
        if let array = i as? [World] {
          array.forEach{
            $0.sortLevelsWithNSSet($0.level)
            worlds.append($0)
          }
          
        }
        
        // This code is to remind me of how simple it suppose to be
        //worlds = try context.executeFetchRequest(fetch.Request) as! [World]
        //worlds.forEach{ $0.sortLevelsWithNSSet($0.level) }
      } catch{
        print("Error for getting worlds from already saved context occured \(error)")
        throw DataError.parse
      }
    }
  }
  
  
  
  
  fileprivate func parseJSON() throws -> JSON {
    guard let path = Bundle.main.path(forResource: "seed", ofType: "json") else {
      throw ParseError.general(message: "Unable to find path to json file.")
    }
    
    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: NSData.ReadingOptions.mappedIfSafe)
    let json = JSON(data: data, options: JSONSerialization.ReadingOptions.allowFragments)
    return json
  }
  
}
