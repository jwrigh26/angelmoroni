//
//  Level.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/5/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import CoreData


class Level: NSManagedObject, DataProtocol {

  var itemNumber: Int {
    get {
      return Int(self.number)
    }
  }
}
