//
//  LevelInfo+CoreDataProperties.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/5/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension LevelInfo {

    @NSManaged var complete: Bool
    @NSManaged var stars: Int16
    @NSManaged var wings: Bool
    @NSManaged var totalBits: Int16
    @NSManaged var bits: Int16
    @NSManaged var level: NSManagedObject
    @NSManaged var user: NSManagedObject

}
