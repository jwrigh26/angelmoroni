//
//  World+CoreDataProperties.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/5/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension World {

    @NSManaged var image: String
    @NSManaged var locked: Bool
    @NSManaged var name: String
    @NSManaged var number: Int16
    @NSManaged var totalLevels: Int16
    @NSManaged var level: NSSet?

}
