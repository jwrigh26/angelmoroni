//
//  World.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/5/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import CoreData


class World: NSManagedObject, DataProtocol {
  
  var sortedLevels = [Level]()
  
  var itemNumber: Int {
    get {
      return Int(self.number)
    }
  }
  
  func sortLevels(_ levels: Set<Level>){
    let array = levels.sorted{
      $0.number < $1.number
    }
    sortedLevels = array
  }
  
  func sortLevelsWithNSSet(_ levels: NSSet?){
    if let set = levels as? Set<Level> {
      sortLevels(set)
    }
  }
  
}
