//
//  Device.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/14/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import SpriteKit

enum ScreenSizeTrait {
  case compact
  case regular
  case large
  case xLarge
  case xxLarge
}

struct Device {
  static let Scale:CGFloat = Device.isPhone ? 1.6 : 2.8
  typealias deviceType = UIUserInterfaceIdiom
  
  let avgScreenWidth: CGFloat = 540.0
  
  static let isPhone: Bool = (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone) ? true : false
  
  var screenSizeTrait:  ScreenSizeTrait {
    get {
      let size = max(UIScreen.main.bounds.size.width, UIScreen.main.bounds.size.height)
      switch size {
      case 480...568:
        return .compact
      case 569...735:
        return .regular
      case 736...1023:
        return .large
      case 1024...1920:
        return .xLarge
      case 1921..<3000:
        return .xxLarge
        // If we don't know then it's getting regular
      default:
        return .regular
      }
    }
  }
  
  var isLargeScreen: Bool {
    switch screenSizeTrait {
    case .compact, .regular:
      return false
    case .large, .xLarge, .xxLarge:
      return true
    }
  }
  
  var isXLargeScreen: Bool {
    switch screenSizeTrait {
    case .xLarge, .xxLarge:
      return true
    default:
      return false
    }
  }
  
  var isSmallScreen: Bool {
    switch screenSizeTrait {
    case .compact:
      return true
    default:
      return false
    }
  }
  
  // Stupid memory leak occurs when calling isSlowDevice
//  var isSlowDevice: Bool {
//    let type = UIDevice().type
//    
//    switch type {
//    case .iPad2, .iPad3:
//      fallthrough
//    case .iPhone4, .iPhone4S, .iPhone5C, .iPhone5, .iPhone5S:
//      return true
//    default:
//      return false
//    }
//  }
  
  //static let Scale:CGFloat = (UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Phone) ? 2.0 : 3.0
}
