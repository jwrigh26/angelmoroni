//
//  CGFloat+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGFloat {
  
  static func random(min min: CGFloat, max: CGFloat) -> CGFloat {
    assert(min < max)
    return CGFloat.random() * (max - min) + min
  }
  
}