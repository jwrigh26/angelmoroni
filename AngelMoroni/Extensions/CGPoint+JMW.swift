//
//  CGPoint+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import CoreGraphics

extension CGPoint {
  
  func length() -> CGFloat {
    return sqrt(x*x + y*y)
  }
  
  func normalized() -> CGPoint {
    return self / length()
  }
  
  var angle: CGFloat {
    return atan2(y, x)
  }
}