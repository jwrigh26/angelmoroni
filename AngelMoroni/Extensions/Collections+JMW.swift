//
//  Array+DSG.swift
//  DSG.ScoreCardConsumerApp
//
//  Created by Wright, Justin M on 9/25/15.
//  Copyright © 2015 DICK'S. All rights reserved.
//

import UIKit

extension Collection {
  
  
  // (cell.contentView.subviews as [UIView]).forEach { $0.removeFromSuperview() }
  func foreach(_ function: (Self.Iterator.Element) -> ()) {
    for elem in self {
      function(elem)
    }
  }
  
  
}

extension RangeReplaceableCollection where Iterator.Element : Equatable {
  
  // Remove first collection element that is equal to the given `object`:
  mutating func removeObject(_ object : Iterator.Element) {
    if let index = self.index(of: object) {
      self.remove(at: index)
    }
  }
}



extension Collection {
  /// Return a copy of `self` with its elements shuffled
  func shuffle() -> [Iterator.Element] {
    var list = Array(self)
    list.shuffleInPlace()
    return list
  }
}

extension MutableCollection where Index == Int {
  /// Shuffle the elements of `self` in-place.
  mutating func shuffleInPlace() {
    // empty and single-element collections don't shuffle
    if count < 2 { return }
   
    for i in startIndex ..< endIndex - 1 {
      let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
      guard i != j else { continue }
      swap(&self[i], &self[j])
    }
  }
}

//extension MutableCollection where Index == Int {
//  /// Shuffle the elements of `self` in-place.
//  mutating func shuffle() {
//    // empty and single-element collections don't shuffle
//    if count < 2 { return }
//    
//    for i in startIndex ..< endIndex - 1 {
//      let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
//      guard i != j else { continue }
//      swap(&self[i], &self[j])
//    }
//  }
//}

extension Array {
  mutating func removeFirstSafely() -> Element? {
    if !self.isEmpty {
      return self.removeFirst()
    }
    return nil
  }
}
