//
//  Number+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/8/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation


extension Int {
  func format(f: String) -> String {
    return String(format: "%\(f)d", self)
  }
}

extension Double {
  func format(f: String) -> String {
    return String(format: "%\(f)f", self)
  }
}
