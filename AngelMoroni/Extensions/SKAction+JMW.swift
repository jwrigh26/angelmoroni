//
//  SKAction+KMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 8/25/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//  http://stackoverflow.com/questions/21940495/sine-wave-motion-in-spritekit

import SpriteKit


/*
 
 In the code below: amplitude is the height of the oscillation; timePeriod is the time for one complete cycle and the midPoint is the point around which the oscillation occurs. The formula for displacement comes from the equations for Simple Harmonic Motion (http://hyperphysics.phy-astr.gsu.edu/hbase/shm.html#c1).
 
   class GameScene: SKScene {
     override func didMoveToView(view: SKView) {
     let node = SKSpriteNode(color: UIColor.greenColor(), size: CGSize(width: 50, height: 50))
     node.position = CGPoint(x: 25, y: size.height / 2)
     self.addChild(node)
     
     let oscillate = SKAction.oscillation(amplitude: 200, timePeriod: 1, midPoint: node.position)
     node.runAction(SKAction.repeatActionForever(oscillate))
     node.runAction(SKAction.moveByX(size.width, y: 0, duration: 5))
     }
   }
 
 */

extension SKAction {
  static func oscillation(amplitude a: CGFloat, timePeriod t: CGFloat, midPoint: CGPoint) -> SKAction {
    let action = SKAction.customAction(withDuration: Double(t)) { node, currentTime in
      let displacement = a * sin(2 * π * currentTime / t)
      //let displacement = a * sin(2 * π * currentTime)
      node.position.y = midPoint.y + displacement
    }
    
    return action
  }
  
//  static func rotateOnArc<T : ArcRotatable>(node n: inout T, timePeriod t: CGFloat) -> SKAction {
//    let action = SKAction.customAction(withDuration: Double(t)) { node, currentTime in
//      
//      
//      let pos = node.position
//      let y = pos.y - n.prevPosition.y
//      let x = pos.x - n.prevPosition.x
//      
//      //print("x \(x) and y \(y). pos \(pos) prev \(prevPos) and callback \(p)")
//      
//      let angle = atan2(-y, -x)
//      if abs(angle) < 1  {
//        node.zRotation = angle
//      }
//      
//      n.prevPosition = node.position
//    }
//    return action
//  }
}

protocol ArcRotatable: class {
  var prevPosition: CGPoint { get set }
}
