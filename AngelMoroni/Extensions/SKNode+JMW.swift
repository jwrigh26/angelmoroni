//
//  SKNode+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import SpriteKit

extension SKNode {
  class func unarchiveFromFile(_ file : String) -> SKNode? {
    if let path = Bundle.main.path(forResource: file, ofType: "sks") {
      let sceneData = try! Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
      let archiver = NSKeyedUnarchiver(forReadingWith: sceneData)
      
      archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
      let scene = archiver.decodeObject(forKey: NSKeyedArchiveRootObjectKey) as! GameScene
      archiver.finishDecoding()
      return scene
    } else {
      return nil
    }
  }
  
  
}
