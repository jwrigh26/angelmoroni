//
//  SKShapeNode+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


extension SKShapeNode {
  
  static func debugBox(_ rect: CGRect, color: UIColor = SKColor.green) -> SKShapeNode{
    let shape = SKShapeNode(rect: rect)
    shape.drawDebugBox(color)
    return shape
  }
  
  func drawDebugBox(_ color: UIColor = SKColor.green){
    zPosition = ZPos.HitBox
    fillColor = color
    strokeColor = color
    alpha = 0.5
  }
  
  
}
