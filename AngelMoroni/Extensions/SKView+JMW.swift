//
//  SKView+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/28/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import SpriteKit

extension SKView {
  
  func cleanup(){
    scene?.isPaused = true
    scene?.removeAllActions()
    scene?.removeAllChildren()
    scene?.removeFromParent()
    presentScene(nil)
    removeAllSubviews()
    removeFromSuperview()
  }
  
  func pause(){
    scene?.isPaused = true
  }
  
  func resume(){
    scene?.isPaused = false
  }
  
}
