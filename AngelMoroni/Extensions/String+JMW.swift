//
//  String+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/24/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation


extension String {
  
  // Replace the first occurence from String
  
  //    for family: String in UIFont.familyNames()
  //    {
  //      print("\(family)")
  //      for names: String in UIFont.fontNamesForFamilyName(family)
  //      {
  //        print("== \(names)")
  //      }
  //    }
  
  func stringByReplacingFirstOccurance(_ find: String, replace: String) -> String?{
    //    let stringAsNS: NSString = string as NSString
    //    let range:NSRange = stringAsNS.rangeOfString(find)
    //
    //    if NSNotFound != range.location {
    //      return stringAsNS.stringByReplacingCharactersInRange(range, withString: replace)
    //    }
    
    if let range = self.range(of: find) {
      return self.replacingCharacters(in: range, with: replace)
    }
    return nil
  }
  
  
  func rangeFromNSRange(_ nsRange : NSRange) -> Range<String.Index>? {
    let from16 = utf16.startIndex.advanced(by: nsRange.location)
    let to16 = from16.advanced(by: nsRange.length) //, limit: utf16.endIndex
    if let from = String.Index(from16, within: self),
      let to = String.Index(to16, within: self) {
        return from ..< to
    }
    return nil
  }
  
  func NSRangeFromRange(_ range : Range<String.Index>) -> NSRange {
    let utf16view = self.utf16
    let from = String.UTF16View.Index(range.lowerBound, within: utf16view)
    let to = String.UTF16View.Index(range.upperBound, within: utf16view)
    return NSMakeRange(utf16view.startIndex.distance(to: from), from.distance(to: to))
    //return NSMakeRange(utf16view.startIndex.distanceTo(from), from.distanceTo(to))
  }
  
  func trim() -> String
  {
    return self.trimmingCharacters(in: CharacterSet.whitespaces)
  }
  
  
}
