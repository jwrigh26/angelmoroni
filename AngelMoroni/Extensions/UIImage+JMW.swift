//
//  UIImage+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit


extension UIImage {
  
  func rotate(byDegrees degree: Double) -> UIImage {
    let radians = CGFloat(degree*M_PI)/180.0 as CGFloat
    let rotatedSize = self.size
    let scale = UIScreen.main.scale
    UIGraphicsBeginImageContextWithOptions(rotatedSize, false, scale)
    let bitmap = UIGraphicsGetCurrentContext()
    bitmap!.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2);
    bitmap!.rotate(by: radians);
    bitmap!.scaleBy(x: 1.0, y: -1.0);
    
    bitmap?.draw(self.cgImage!, in: CGRect(x: -self.size.width / 2, y: -self.size.height / 2, width: self.size.width, height: self.size.height))
    
    
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    
    return newImage!
  }
  
}

extension UIImageView {
  
  
  func make8BitFriendly(){
    backgroundColor = UIColor.clear
    layer.magnificationFilter = kCAFilterNearest
    contentMode = .scaleAspectFit
  }
}
