//
//  UILabel+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/8/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit


extension UILabel {
  
  
  static func heightForLabel(_ font: UIFont, width: CGFloat, text: String) -> CGFloat {
    
    let rect = NSString(string: text).boundingRect(with: CGSize(width: width, height: CGFloat(MAXFLOAT)), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
    
    return ceil(rect.height)
  }
  
  
  
  
  
}


extension NSMutableAttributedString {
  
  func setColorForStr(_ textToFind: String, color: UIColor) {
    
    let range = self.mutableString.range(of: textToFind, options:NSString.CompareOptions.caseInsensitive);
    if range.location != NSNotFound {
      self.addAttribute(NSForegroundColorAttributeName, value: color, range: range);
    }
    
  }
  
  func getColorRangeForString(_ textToFind: String) -> NSRange? {
    let range = self.mutableString.range(of: textToFind, options:NSString.CompareOptions.caseInsensitive);
    if range.location != NSNotFound {
      return range
    }
    return nil
  }
}


// Figure out the size of height of uilabel
extension String {
  func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
    
    return boundingBox.height
  }
}


extension NSAttributedString {
  func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
    
    return boundingBox.height
  }
  
  func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
    
    return boundingBox.width
  }
}
