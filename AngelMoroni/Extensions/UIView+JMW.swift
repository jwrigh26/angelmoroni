//
//  UIView+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/28/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
  
  func removeAllSubviews(){
    for subview in subviews {
      subview.removeFromSuperview()
    }
  }
  
  func printConstraints(){
    for c in self.constraints {
      print(c)
    }
  }
  
  func snapShotAfterScreenUpdates() -> UIView {
    return snapShot(true)
  }
  
  func snapShot(_ afterScreenUpdates: Bool) -> UIView {
    let snapShot = self.snapshotView(afterScreenUpdates: afterScreenUpdates)
    snapShot?.frame = self.frame
    return snapShot!
  }
  
}
