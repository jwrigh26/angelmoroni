//
//  UIViewController+JMW.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/27/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

var screenSize: CGSize {
  let screenSize = UIScreen.main.bounds.size
  return screenSize
}

var screenBounds: CGRect {
  let bounds = UIScreen.main.bounds
  return bounds
}


extension UIViewController {
  
  func removeAllSubviews(){
    for subview in view.subviews {
      subview.removeAllSubviews()
      subview.removeFromSuperview()
    }
    self.view.removeFromSuperview()
  }
  
}
