//
//  GameColorTable.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/1/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import CleanroomLogger

public struct GameColorTable: ColorTable
{
  /** A light gray `Color` (`#999999`) used as the foreground color
   for the `.Verbose` severity. */
  public static let VerboseColor  = Color(r: 0x99, g: 0xFF, b: 0xBB)
  
  /** A dark gray `Color` (`#666666`) used as the foreground color
   for the `.Debug` severity. */
  public static let DebugColor    = Color(r: 0x99, g: 0xB3, b: 0xFF)
  
  /** A blue `Color` (`#00ffff`) used as the foreground color
   for the `.Info` severity. */
  public static let InfoColor     = Color(r: 0x00, g: 0xFF, b: 0xFF)
  
  /** An orange `Color` (`#DD7722`) used as the foreground color
   for the `.Warning` severity. */
  public static let WarningColor  = Color(r: 0xDD, g: 0x77, b: 0x22)
  
  /** A red `Color` (`#CC0000`) used as the foreground color
   for the `.Error` severity. */
  public static let ErrorColor    = Color(r: 0xCC, g: 0x00, b: 0x00)
  
  /**
   Returns the foreground color to use (if any) for colorizing messages
   at the given `LogSeverity`.
   
   - parameter severity: The `LogSeverity` whose color information is
   being retrieved.
   
   - returns:  The foreground `Color` to use for `severity`, or `nil` if no
   color is specified.
   */
  public func foreground(forSeverity severity: LogSeverity) -> Color? {
    switch severity {
    case .verbose:      return type(of: self).VerboseColor
    case .debug:        return type(of: self).DebugColor
    case .info:         return type(of: self).InfoColor
    case .warning:      return type(of: self).WarningColor
    case .error:        return type(of: self).ErrorColor
    }
  }
}
