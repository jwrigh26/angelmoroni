//
//  GameArchiver.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/2/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//
import Foundation
import SpriteKit
import CleanroomLogger

struct GameArchiver {
  
  let startSceneKey = "AppDelegateStartSceneKey"
  let gameSceneKey = "AppDelegateGameSceneKey"
  let directory = "Private Documents"
  
  var unarchiver: NSKeyedUnarchiver? {
    if let dataPath = privateDocsDir()?.stringByAppendingPathComponent("autosaved-scene"),
      codedData = NSData(contentsOfFile: dataPath) {
      return  NSKeyedUnarchiver(forReadingWithData: codedData)
    }
    return nil
  }

  private var documentsDirectory: String {
    let paths = NSSearchPathForDirectoriesInDomains(.LibraryDirectory, .UserDomainMask, true)
    let documentsDirectory = paths[0].stringByAppendingPathComponent(directory)
    return documentsDirectory
  }
  
  
  func applicationWillResignActive(window: UIWindow?, gameState: GameState){
    if gameState != .Paused && gameState != .GameOver {
      pauseRootScene(window)
    }
  }
  
  func applicationDidBecomeActive(window: UIWindow?, gameState: GameState){
    if gameState != .Paused && gameState != .GameOver {
      resumeRootScene(window)
    }
  }
  
  func pauseRootScene(window: UIWindow?){
    
    if let view = window?.rootViewController?.view as? SKView, documentDirectory = privateDocsDir() {
      view.scene?.paused = true
      let filePath = documentDirectory.stringByAppendingPathComponent("autosaved-scene")
      let data = NSMutableData()
      let archiver = NSKeyedArchiver(forWritingWithMutableData: data)
      
      if let game = view.scene as? GameScene {
        Log.debug?.message("Archiving game scene")
        archiver.encodeObject(game, forKey: gameSceneKey)
      }
      
      if let start = view.scene as? StartScene {
        archiver.encodeObject(start, forKey: startSceneKey)
      }
      
      defer {
        archiver.finishEncoding()
        data.writeToFile(filePath, atomically: true)
      }
    }
  }
  
  
  func resumeRootScene(window: UIWindow?){
    
    guard let
      view = window?.rootViewController?.view as? SKView,
      unarchiver = self.unarchiver
      else { Log.error?.message("cannot resume root scene"); return }
    
    if let scene = unarchiver.decodeObjectForKey(gameSceneKey) as? GameScene {
      presentScene(view, scene: scene)
    }
    
    if let scene = unarchiver.decodeObjectForKey(startSceneKey) as? StartScene {
      presentScene(view, scene: scene)
    }
    
    defer {
      unarchiver.finishDecoding()
    }
  }
  
  
  func presentScene(view: SKView, scene: SKScene){
    
    view.presentScene(scene)
    view.scene?.paused = true
  }
  
  
  private func privateDocsDir() -> String? {
    do{
      try NSFileManager.defaultManager().createDirectoryAtPath(documentsDirectory, withIntermediateDirectories: true, attributes: nil)
    }catch{
      Log.error?.message("Unable to create directory at path \(documentsDirectory)")
      Log.error?.message("Error: \(error)")
      return nil
    }
    
    return documentsDirectory
  }
  
  private func deleteFile() -> Bool {
    let directory = documentsDirectory
    let exists = NSFileManager.defaultManager().fileExistsAtPath(documentsDirectory)
    if exists{
      do {
        try NSFileManager.defaultManager().removeItemAtPath(directory)
        return true
      }catch{
        Log.error?.message("Unable to delete directory at path \(documentsDirectory)")
        Log.error?.message("Error: \(error)")
      }
    }
    return false
  }
  
}