//
//  AchievementsHelper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/1/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import GameKit

class AchievementsHelper {
  
  static let MaxRetries = 3.0
  static let world1completestar = "com.jmw.AngelMoroni.world1completestar"
  static let world1completemoon = "com.jmw.AngelMoroni.world1completemoon"
  static let world1completesun = "com.jmw.AngelMoroni.world1completesun"
  static let celestialperfection = "com.jmw.AngelMoroni.celestialperfection"
  static let keeptrying = "com.jmw.AngelMoroni.keeptrying"
  
  class func celestialPerfectionAchievement() -> GKAchievement {
    let achievement = GKAchievement(identifier: AchievementsHelper.celestialperfection)
    achievement.percentComplete = 100
    achievement.showsCompletionBanner = true
    return achievement
  }
  
  class func keepTryingAchievement(noOfTries: Int) -> GKAchievement {
    
    let percent = Double(noOfTries)/AchievementsHelper.MaxRetries * 100
    let achievement = GKAchievement(identifier: AchievementsHelper.keeptrying)
    
    achievement.percentComplete = percent
    achievement.showsCompletionBanner = true
    
    return achievement
  }
  
  class func world1levelAchievement(complete: Int, total: Int, degree: DegreeOfGlory) -> GKAchievement {
    
    let percent = Double(complete)/Double(total) * 100
    var id = AchievementsHelper.world1completestar
    switch degree {
    case .moon:
      id = AchievementsHelper.world1completemoon
    case .sun:
      id = AchievementsHelper.world1completesun
    default:
      break
    }
    
    let achievement = GKAchievement(identifier: id)
    achievement.percentComplete = percent
    if percent == 100 {
      achievement.showsCompletionBanner = true
    }
    return achievement
  }
  
}

