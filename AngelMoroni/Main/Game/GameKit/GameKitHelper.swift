//
//  GameKitHelper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/1/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import UIKit
import GameKit
import Crashlytics

final class GameKitHelper: NSObject {
  
  static let sharedInstance = GameKitHelper()
  
  var authenticationViewController: UIViewController?
  var gameCenterEnabled = false
  
  
  func authenticateLocalPlayer(){
    GKLocalPlayer.localPlayer().authenticateHandler = {
      vc, error in
      self.gameCenterEnabled = false
      if vc != nil {
        self.authenticationViewController = vc
        
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.presentAuthenticationViewController), object: self, userInfo: nil)
        
        Answers.logCustomEvent(withName: GameAnalytics.gkAuth, customAttributes: nil)
        
      }else if GKLocalPlayer.localPlayer().isAuthenticated{
        self.gameCenterEnabled = true
      }
    }
  }
  
  func reportAchievements(achievements: [GKAchievement], errorHandler: ((Error?)->Void)? = nil){
    guard gameCenterEnabled else { return }
    GKAchievement.report(achievements, withCompletionHandler: errorHandler)
  }
  
  func showGKCenterViewController(viewController: UIViewController){
    guard gameCenterEnabled else { return }
    
    let vc = GKGameCenterViewController()
    vc.gameCenterDelegate = self
    viewController.present(vc, animated: true, completion: nil)
    
    Answers.logCustomEvent(withName: GameAnalytics.gkView, customAttributes: nil)
    
  }
  
}

extension GameKitHelper: GKGameCenterControllerDelegate {
  func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
    gameCenterViewController.dismiss(animated: true, completion: nil)
  }
}
