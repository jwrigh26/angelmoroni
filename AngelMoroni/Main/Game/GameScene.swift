//
//  GameScene.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

/*
 
 Thus on iOS devices you can only ever have these discrete framerates:
 
 60 / 1 = 60 fps (delta time: 0.0166.. seconds)
 60 / 2 = 30 fps (delta time: 0.0333.. seconds)
 60 / 3 = 20 fps (delta time: 0.05 seconds)
 60 / 4 = 15 fps (delta time: 0.0666.. seconds)
 60 / 5 = 12 fps (delta time: 0.0833.. seconds)
 60 / 6 = 10 fps (delta time: 0.1 seconds)
 
 */


import SpriteKit
import RealmSwift
import GameKit
import Crashlytics

final class GameScene: BaseScene {
  
  let gameGain: CGFloat = 2.5
  var backgroundLayer: BackgroundLayer!
  var gameLayer: GameLayer!
  var hudLayer: HUDLayer!
  var particleLayer = ParticleLayer()
  var evalFps: CFTimeInterval = 0.05
  var finishFps: CFTimeInterval = 1.0
  
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(size: CGSize) {
    super.init(size: size)
    name = "GameScene"
    
    
    // This needs to be added regardless
    particleLayer.zPosition = 2.0
    addChild(particleLayer)
    
    addBlackLayer()
    
    backgroundLayer = BackgroundLayer()
    backgroundLayer.zPosition = 1.0
    addChild(backgroundLayer)
    
    gameLayer = GameLayer()
    gameLayer.zPosition = 1.0
    addChild(gameLayer)
    
    hudLayer = HUDLayer()
    hudLayer.zPosition = 1.0
    addChild(hudLayer)
    
  }
  
  override func didMove(to view: SKView) {
    super.didMove(to: view)
    resetTimeValues()
  }
  
  override func willMove(from view: SKView) {
    particleLayer.removeAllActions()
    particleLayer.removeAllChildren()
    backgroundLayer.removeAllActions()
    backgroundLayer.removeAllChildren()
    gameLayer.removeAllActions()
    gameLayer.removeAllChildren()
    hudLayer.removeAllActions()
    hudLayer.removeAllChildren()
    resetTimeValues()
  }
  
  
  func sceneTouched(_ touchLocation: CGPoint){
    gameLayer.angel?.isFlying = true
    if gameState != .levelComplete {
      
    }
    
    //levelComplete()
  }
  
  func sceneTouchEnded(_ touchLocation: CGPoint){
    gameLayer.angel?.isFlying = false
    if gameState != .levelComplete {
      
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let touch = touches.first  {
      let touchLocation = touch.location(in: self)
      sceneTouched(touchLocation)
    }
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    if let touch = touches.first {
      let touchLocation = touch.location(in: self)
      sceneTouchEnded(touchLocation)
    }
  }
  
  
  var lastEvaluateTime: CFTimeInterval = 0
  var lastFinishTime: CFTimeInterval = 0
  var lastUpdateTime: CFTimeInterval = 0
  var dt: CFTimeInterval = 0
  
  
  override func update(_ currentTime: TimeInterval) {
    guard enableUpdateLoop else { return }
    
    if lastUpdateTime > 0 {
      dt = currentTime - lastUpdateTime
    } else {
      dt = 0
    }
    
    if gameState != .levelComplete {
      gameLayer.update(dt)
    }
    
    backgroundLayer.update(dt)
    
    if gameState == .playing {
      lastEvaluateTime += dt
      lastFinishTime += dt
    }
    
    lastUpdateTime = currentTime
  }
  
  override func didEvaluateActions() {
    guard enableUpdateLoop else { return }
    
    if lastEvaluateTime >= evalFps {
      gameLayer.didEvaluateActions()
      lastEvaluateTime = 0
    }
  }
  
  override func didFinishUpdate() {
    guard enableUpdateLoop else { return }
    if lastFinishTime >= finishFps {
      gameLayer.didFinishUpdate()
      lastFinishTime = 0
    }
  }
  
  func screenShakeByAmt(_ amt: CGFloat){
    let amount = CGPoint(x: 0, y: -(amt * gameGain))
    
    let layers: [SKNode] = [gameLayer, backgroundLayer]
    let position = CGPoint(x: 0, y: 0)
    for layer in layers {
      layer.position = position
      layer.removeAction(forKey: "shake")
      let action = SKAction.screenShakeWithNode(layer, amount: amount, oscillations: 10, duration: 2.0)
      layer.run(action, withKey: "shake")
    }
  }
  
  func colorGlitch(_ duration: TimeInterval = 0.1){
    
    self.removeAction(forKey: "colorGlitch")
    backgroundLayer.hideClouds()
    let glitchAction = SKAction.colorGlitchWithScene(self, originalColor: backgroundOriginalColor, duration: duration)
    let restoreAction = SKAction.run{ [weak self] in
      self?.backgroundLayer.showClouds()
    }
    run(SKAction.sequence([glitchAction, restoreAction]), withKey: "colorGlitch")
  }
  
  
  var levelSummary: HUDLevelSummary{
    let hartsMax = hudLayer.hartCount
    let harts = hudLayer.hartsRemaining
    let bits = hudLayer.currentBits
    return HUDLevelSummary(bits: bits!, harts: harts, hartsMax: hartsMax)
  }
  
  func levelComplete(){
    gameLayer?.levelComplete(){ [weak self] in
      GameManager.sharedInstance.levelSummary = self?.levelSummary
      
      guard
      let worldKey = NavigationManager.sharedInstance.currentWorldPath?.world.id,
      let levelKey = NavigationManager.sharedInstance.currentLevelPath?.level.id
        else{ return }
      
      GlobalBackgroundQueue.async {
        self?.updateUser(worldKey: worldKey, levelKey: levelKey)
      }
    }
  }
  
  
  
  func levelStart(){
    gameLayer.levelStart()
    
    
    
  }
  
  override func onPause() {
    self.isPaused = true
    resetTimeValues()
  }
  
  fileprivate func resetTimeValues(){
    lastEvaluateTime = 0
    lastFinishTime = 0
    lastUpdateTime = 0
    dt = 0
    GameManager.sharedInstance.resetGameSpeed()
  }
  
  
  func updateUser(worldKey: String, levelKey: String){
    guard
      let summary = GameManager.sharedInstance.levelSummary else {
        fatalError("No world or level exists")
    }
    
    var levelData: LevelData?
    let realm = Realm.mainInstance()
    
    guard let user = realm.object(ofType: User.self, forPrimaryKey: User.key) else {
      // TODO: Remove realm file
      //http://stackoverflow.com/questions/37396319/how-to-completely-remove-realm-database-from-ios
      fatalError("Realm was corrupted")
    }
    
    if let data = user.levels.filter("levelKey = %@", levelKey).first {
      //print("We found a level data so assign it")
      levelData = data
    } else {
      //print("No level data exist so create a new one")
      levelData = LevelData()
      realm.safeWrite { user.levels.append(levelData!) }
    }
    
    
    guard let data = levelData else { fatalError("Something went horrible writing data") }
    
    let degree = summary.degreeOfGlory.rawValue
    let overwriteBits: Bool = summary.bits > data.bitsCollected
    let overwriteDegree: Bool = degree > data.degreeOfGlory
    let overwriteHarts: Bool = summary.harts > data.harts
  
    
    realm.safeWrite {
      levelData?.complete = true
      levelData?.levelKey = levelKey
      levelData?.worldKey = worldKey
      
      if overwriteHarts {
        levelData?.harts = summary.harts
      }
      
      
      if overwriteBits {
        levelData?.bitsCollected = summary.bits
      }
      
      if overwriteDegree {
        levelData?.degreeOfGlory = degree
      }
      
      realm.add(levelData!, update: true)
    }
    
    //print("Update Achievements")
    
    let staticRealm = Realm.readOnlyInstance()
    let world = staticRealm.object(ofType: World.self, forPrimaryKey: levelData?.worldKey ?? "")
    let complete = user.levels.filter("worldKey = %@", worldKey).count
    //print("Word = \(world?.order) and total = \(world?.levels.count) and complete \(complete) and degree = \(degree)")
    if let w = world?.order, let t = world?.levels.count, let d = DegreeOfGlory(rawValue: degree) {
      reportAchievementsForLevelComplete(world: w, completedLevels: complete, total: t, degree: d)
      
      let index = NavigationManager.sharedInstance.currentLevelPath?.1.row ?? -1
      let lvl = staticRealm.object(ofType: Level.self, forPrimaryKey: levelData?.levelKey ?? "")
      let title = "\(index + 1) - \(lvl?.title)"
      Answers.logCustomEvent(withName: GameAnalytics.levelComplete, customAttributes: ["world" : "\(w + 1)", "level" : title, "degree" : GameAnalytics.degree(degree)])

      
    }
    
    
  }
  
  
  func reportAchievementsForLevelComplete(world: Int, completedLevels levels: Int, total: Int, degree: DegreeOfGlory){
    var achievements = [GKAchievement]()
    
    if world == 0 {
      switch degree {
      case .sun:
        achievements.append(AchievementsHelper.world1levelAchievement(complete: levels, total: total, degree: .sun))
        achievements.append(AchievementsHelper.world1levelAchievement(complete: levels, total: total, degree: .moon))
        achievements.append(AchievementsHelper.world1levelAchievement(complete: levels, total: total, degree: .star))
      case .moon:
        achievements.append(AchievementsHelper.world1levelAchievement(complete: levels, total: total, degree: .moon))
        achievements.append(AchievementsHelper.world1levelAchievement(complete: levels, total: total, degree: .star))
      case .star:
        achievements.append(AchievementsHelper.world1levelAchievement(complete: levels, total: total, degree: .star))
      default:
        break
      }
      
    }
    
    if degree == .sun {
      achievements.append(AchievementsHelper.celestialPerfectionAchievement())
    }
    
    GameKitHelper.sharedInstance.reportAchievements(achievements: achievements)
  }

  
  
}









