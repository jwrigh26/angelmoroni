//
//  GameScene.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

final class BackgroundLayer: SKNode, GameLoopReady {
  
  let starFieldNode = SKNode()
  let starField = StarField()
  let sun: SKSpriteNode
  
  let BackgroundQueue = GameManager.sharedInstance.backgroundQueue
  var cloudSheet: GameSheet!
  var cloudArray = Array<Array<CloudSprite>>()
  var accelerateClouds: Bool = true
  
  func cloud(_ name: String) -> SKTexture {
    return cloudSheet.textureAtlas.textureNamed(name)
  }
  
  override init(){
    sun = SKSpriteNode(texture: GameManager.sharedInstance.gameSheet.Clouds_sun())
    super.init()
    createSun()
    createStarField()
    cloudSheet = GameManager.sharedInstance.gameSheet
    assignCloudsToArray()
    
  }
  
  func update(_ dt: CFTimeInterval){
    BackgroundQueue.sync{ [weak self] in
    guard let cloudArray = self?.cloudArray , !cloudArray.isEmpty else { return }
      for array in cloudArray {
        if !array.isEmpty {
          GlobalMainQueue.async{
            array.forEach{ $0.update(dt, accelerate: self?.accelerateClouds ?? false) }
          }
        }
      }
    }
  }
  
  func didEvaluateActions(){}
  func didFinishUpdate(){}
  
  func createSun(){
    sun.texture?.filteringMode = .nearest
    sun.alpha = 0.1
    let height = sun.frame.width
    let scale = (screenSize.width * 2) / max(1 , height)
    sun.setScale(scale)
    
    let x = screenBounds.maxX
    let y = screenBounds.maxY
    sun.position = CGPoint(x: x, y: y)
    let r = CGFloat(M_PI).degreesToRadians()
    
    let action = SKAction.rotate(byAngle: r, duration: 3.0)
    sun.run(SKAction.repeatForever(action))
    insertChild(sun, at: 0)
  }
  
  func createStarField(){
    starFieldNode.name = "starfieldNode"
    let color = UIColor(netHex: GameColor.cloudLightBlue.hex).cgColor
    let skColor = SKColor(cgColor: color)
    let scale1: CGFloat = Device.isPhone ? 0.09 : 0.14
    let scale2: CGFloat = Device.isPhone ? 0.08 : 0.1
    let emitterNode1 = starField.emitterNode(-48, lifetime: screenSize.width / 23, scale: scale1, birthRate: 2, color: skColor)
    starFieldNode.addChild(emitterNode1)
    
    let emitterNode2 = starField.emitterNode(-32, lifetime: screenSize.width / 10, scale: scale2, birthRate: 1, color: skColor)
    emitterNode2.zPosition = -10
    starFieldNode.addChild(emitterNode2)
    
    addChild(starFieldNode)
    starFieldNode.zPosition = -1
  }

  
  
  // Mark: Private helper methods
  
  fileprivate func assignCloudsToArray(){ //#1 called on init
    if !cloudArray.isEmpty {
      cloudArray.removeAll()
    }
//    cloudArray.append(makeClouds(ParallaxPosition.Back))
    cloudArray.append(makeClouds(ParallaxPosition.middleBack))
    cloudArray.append(makeClouds(ParallaxPosition.middleFront))
    cloudArray.append(makeClouds(ParallaxPosition.front))
  }
  
  
  fileprivate func makeClouds(_ zPos: ParallaxPosition) -> [CloudSprite]{ //#2 called in assign clouds to array
    
    CloudSprite.filePath = "Clouds/cloud_b"
    
    let size = GameManager.sharedInstance.screenSize
    var clouds: [CloudSprite] = []
    let numClouds:Int = max(Int(ceil(size.width / 480.00)+1), 3) //need at least 3 clouds to rotate properly
    
    // #1 Add all clouds to array
    for i in 0..<Int(numClouds) {
      let cloud = createCloud(zPos, scrollOrder: i)
      addChild(cloud)
      clouds.append(cloud)
    }
    
    // #2 Add linked cloud to cloud in array
    for j in 0..<Int(numClouds){
      var index = 0
      if j == 0 {
        index = Int(numClouds)-1 // gets the last cloud for the linked sprite
      }else{
        index = j-1
      }
      let cloud = clouds[j]
      let linkedCloud = clouds[index]
      cloud.linkedSprite = linkedCloud
    }
    
    return clouds
  }
  
  fileprivate func createCloud(_ parallxPosition: ParallaxPosition, scrollOrder: Int) -> CloudSprite { //#3 called in make clouds
    let fileName = CloudSprite.generateFileName(parallxPosition)
    let texture = cloud(fileName)
    return CloudSprite(texture: texture, parallxPosition: parallxPosition, scrollOrder: scrollOrder)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func hideClouds(){
    for array in cloudArray {
      array.forEach{ $0.isHidden = true }
    }
  }
  
  func showClouds(){
    for array in cloudArray {
      array.forEach{ $0.isHidden = false }
    }
  }
}
