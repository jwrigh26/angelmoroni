//
//  BlackLayer.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/5/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

class ColorLayer: SKSpriteNode {
  
  convenience init(color: UIColor){
    self.init(texture: nil, color: color, size: CGSize(width: screenSize.width, height: screenSize.height))
    self.anchorPoint = CGPoint.zero
    self.position = CGPoint.zero
  }
  
  
  convenience init(color: UIColor, size: CGSize){
    self.init(texture: nil, color: color, size: size)
  }
  
  override init(texture: SKTexture?, color: UIColor, size: CGSize) {
    super.init(texture: texture, color: color, size: size)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
