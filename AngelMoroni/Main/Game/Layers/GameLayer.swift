//
//  GameLayer.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/20/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import SpriteKit

final class GameLayer: SKNode, GameLoopReady {
  
  let lvlCompleteOffset:CGFloat = Device.isPhone ? 40 : 80
  var bigText: BMGSwiftyLabel?
  var angel: Angel!
  var screenCoordinator: LevelScreenCoordinator?
  var notificationRemoveRocket: NSNotificationProxy?
  
  let move = SKAction.move(to: CGPoint(x: -screenSize.width, y: 0), duration: 7.0)
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  override init(){
    super.init()
    angel = Angel(texture: GameManager.sharedInstance.gameSheet.AngelMoroni_am_01())
    addChild(angel)
    setupScreenCoordinator()
    RocketController.sharedInstance.parent = self
  }
  
  func update(_ dt: CFTimeInterval){
    angel.update(dt)
    screenCoordinator?.update(dt)
    GameManager.sharedInstance.update(dt)
    RocketController.sharedInstance.update(dt, playerHitBox: angel.boundingBox())
  }
  
  func didEvaluateActions(){
    angel.didEvaluateActions()
    screenCoordinator?.didEvaluateActions()
  }
  
  func didFinishUpdate(){
    screenCoordinator?.didFinishUpdate()
    
  }
  
  deinit{
    
  }
  
  func levelStart(){
    
    let introLight = SKAction.run{ [weak self] in self?.addIntroLight() }
    let delay = SKAction.wait(forDuration: 0.5)
    let move = SKAction.moveTo(x: angel.xPos, duration: 1.0)
    move.timingMode = .easeIn
    let block = SKAction.run{
      GameManager.sharedInstance.gameState = .playing
    }
    angel.run(SKAction.sequence([introLight, delay, move, block]))
    angel.run(AngelAction.repeatWalkForever)
    //addJetpack()
  }
  
  func angelFlyAway(){
    
    let moveToFlyPos = SKAction.moveTo(y: screenBounds.midY - lvlCompleteOffset, duration: 0.4)
    moveToFlyPos.timingMode = .easeIn
    let flyAway = SKAction.moveTo(x: screenBounds.maxX + angel.boundingBox().width, duration: 1.2)
    flyAway.timingMode = .easeOut
    
    let dust = SKAction.run{ [weak self] in self?.addAngelDust() }
    let delay = SKAction.wait(forDuration: 0.05)
    
    angel.run(SKAction.sequence([moveToFlyPos, delay, dust, AngelAction.delay, flyAway]))
    angel.run(AngelAction.flySequence)
  }
  
  func addIntroLight(){
    guard let introLight = Particle(fileNamed: GameManager.sharedInstance.introLight) else { return }
    let x: CGFloat = introLight.particlePositionRange.dx / 2; let y = screenSize.height / 2
    introLight.position = CGPoint(x: x, y: y)
    introLight.name = "introLight"
    introLight.particlePositionRange = CGVector(dx: introLight.particlePositionRange.dx, dy: screenSize.height)
    let lifeSpan: CGFloat = CGFloat(introLight.numParticlesToEmit) / introLight.particleBirthRate +
      introLight.particleLifetime + introLight.particleLifetimeRange / 2
    let time = TimeInterval(lifeSpan)
    
    self.run(SKAction.wait(forDuration: time), completion: { _ in
      introLight.removeFromParent()
    })
    
    introLight.targetNode = (scene as! GameScene).particleLayer
    self.addChild(introLight)
    
  }
  
  func levelComplete(_ complete:@escaping ()->()){
    GlobalBackgroundQueue.async{ [weak self] in
      guard let me = self else { return }
      me.bigText = BMGSwiftyLabel(text: "LEVEL COMPLETE", font: BMGSwiftyFont(name: GameManager.sharedInstance.bigTextFontName, textureAtlas: GameManager.sharedInstance.bigTextAtlas))
      me.bigText!.position = CGPoint(x: screenBounds.midX, y: screenBounds.midY + me.lvlCompleteOffset)
      me.bigText!.alpha = 0.0
      me.bigText!.setScale(5.0)
      me.bigText!.zPosition = 10
      GlobalMainQueue.async{
        
        self?.angelFlyAway()
        
        delay(0.75){
          if let bigText = self?.bigText {
            self?.addChild(bigText)
            let scaleText = SKAction.scale(to: 1.0, duration: 0.5)
            let alphaText = SKAction.fadeIn(withDuration: 0.5)
            let delay = SKAction.wait(forDuration: 1.75)
            let block = SKAction.run{
              NavigationManager.sharedInstance.transitionByGameState(.levelSummary)
            }
            bigText.run(SKAction.sequence([AngelAction.delay, SKAction.group([scaleText, alphaText]), delay, block]))
          }
        }
        complete()
      }
    }
  }
  
  
  func addAngelDust(){
    guard let p = Particle(fileNamed: GameManager.sharedInstance.angelDustParticle) else { return }
    let x: CGFloat = 18
    let y: CGFloat = 12
    p.position = CGPoint(x: x, y: y)
    //p.advanceSimulationTime(NSTimeInterval(p.particleBirthRate))
    p.xAcceleration = -screenSize.width
    p.name = "angeldust"
    p.targetNode = (scene as! GameScene).particleLayer
    angel.addChild(p)
  }
  
  
  fileprivate func setupScreenCoordinator(){
    screenCoordinator = LevelScreenCoordinator.sharedInstance
    screenCoordinator!.parent = self
    screenCoordinator!.playerFrame = angel.frame
  }
}
