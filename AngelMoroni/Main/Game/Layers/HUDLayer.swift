//
//  GameScene.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

final class HUDLayer: SKNode {
  
  let GameQueue = GameManager.sharedInstance.gameQueue

  let hartCount: Int = 3
  
  var pauseBtnPadding: CGFloat {
    let device = Device()
    return device.isXLargeScreen ? 64 : 48
  }
  
  var hartsRemaining: Int = 0
  var harts = [HartSprite]()
  var hartYPos: CGFloat = 0
  
  var scriptureBitLabel: HudYellowLabel!
  var bitsLabel: HudWhiteLabel!
  var totalBitsLabel: HudYellowLabel!
  var currentBits: Int!
  
  var totalBits: Int {
    return GameManager.sharedInstance.scriptureMaster?.totalBits ?? 0
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(){
    super.init()
    
    currentBits = 0
    generateHarts()
    hartsRemaining = hartCount
    addLabels()
  }
  
  
  
  func takeDamage(_ callback: @escaping (_ gameOver: Bool) -> ()){
    GameQueue.async{ [weak self] in
      guard let me = self else { return }
    for i in 0 ..< me.harts.count {
      let hart = me.harts[i]
      if hart.brakeMyHart() {
        me.hartsRemaining -= 1
        break;
      }
    }
    
    let gameOver = me.hartsRemaining <= 0 ? true: false
      GlobalMainQueue.async{
        callback(gameOver)
      }
    }
  }
  
  func generateHarts(){
    
    for index in 1...hartCount {
      
      let texture = GameSprite.gameSheet.HUD_hart()
      let hartSize = texture.size().width * Device.Scale
      let screenSize = GameManager.sharedInstance.screenSize
      let x: CGFloat = floor(screenSize.width - (hartSize*CGFloat(index))) - pauseBtnPadding
      let y: CGFloat = floor(screenSize.height - hartSize)
      let position = CGPoint(x: x, y: y)
      let hart = HartSprite(texture: texture, position: position)
      hartYPos = y
      harts.append(hart)
      addChild(hart)
    }
  }
  
  func addLabels(){
    
    scriptureBitLabel = HudYellowLabel(text: CommonStrings.scriptureBits.uppercased())
    let yPos = hartYPos
    let space: CGFloat = Device.isPhone ? 16 : 24
    var pos = CGPoint(x: scriptureBitLabel!.boundingBox.width/2 + space , y: yPos)
    scriptureBitLabel!.position = pos
    addChild(scriptureBitLabel!)
    
    bitsLabel = HudWhiteLabel(text: bitsToString())
    pos = CGPoint(x: scriptureBitLabel!.maxX + bitsLabel!.boundingBox.width/2 + space/2, y: yPos)
    bitsLabel!.position = pos
    addChild(bitsLabel)
    
    
    let t = totalBitsToString()
    totalBitsLabel = HudYellowLabel(text: "/\(t)".uppercased())
    pos = CGPoint(x: bitsLabel!.maxX + totalBitsLabel!.boundingBox.width/2, y: yPos)
    totalBitsLabel.position = pos
    addChild(totalBitsLabel)
  }
  
  func increaseCurrentBits(){
    GameQueue.async{ [weak self] in
      guard let me = self else { return }
      me.currentBits! += 1
      me.currentBits = min(me.currentBits!, me.totalBits)
      GlobalMainQueue.async{
        me.bitsLabel.text = me.bitsToString()
      }
    
    }
  }
  
  
  fileprivate func bitsToString() -> String{
    if totalBits >= 100 {
      return String(format: "%03d", currentBits)
    }
    return String(format: "%02d", currentBits)
  }
  
  fileprivate func totalBitsToString() -> String {
    if totalBits >= 100 {
      return String(format: "%03d", totalBits)
    }
    return String(format: "%02d", totalBits)
  }
}

extension BMGSwiftyLabel {
  
  public var maxX: CGFloat {
    return boundingBox.width / 2 + position.x
  }
  
}



