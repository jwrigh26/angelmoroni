//
//  GameScene.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//


import SpriteKit



class ParticleLayer: SKNode {
  
  
  let particlePool: ScriptureBitParticlePool
  
  
  override init() {
    particlePool = ScriptureBitParticlePool()
    super.init()
    particlePool.node = self
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
}




