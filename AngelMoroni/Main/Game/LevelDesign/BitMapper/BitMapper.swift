//
//  BitMapper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/7/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



struct BitMapper: Mapper {
  
  fileprivate static var inLevelTotal = 0
  
  fileprivate static var maxInLevelAllowed: Int = {
    let totalBits = GameManager.sharedInstance.scriptureMaster?.totalBits
    return totalBits > 15 ? 6 : 3
  }()
  
  var mapOnlyBits: Bool {
    return onlyBits && BitMapper.inLevelTotal <= BitMapper.maxInLevelAllowed
  }
  
  fileprivate var onlyBits: Bool {
    let totalBits = GameManager.sharedInstance.scriptureMaster?.totalBits
    let onlyBitsMaxChance = totalBits > 15 ? 10 : 5
    let onlyBitsChance = random(min: 1, max: 100)
    let bool = onlyBitsChance <= onlyBitsMaxChance
    
    if bool { BitMapper.inLevelTotal += 1}
    return bool
  }
  
  fileprivate var mapper: BitMapperPositionGenerator!

  
  init(playerFrame: CGRect){
    mapper = BitMapperPositionGenerator(playerFrame: playerFrame)
  }
  
  func mapBits(parent: SKNode, sprites: [SpriteProtocol]?, baseFrame: CGRect) -> [CGRect]? {
    guard let sprites = sprites , sprites.count > 0 else {
      print("No bits to mapout on screen")
      return nil
    }
    
    mapper.lastMappedRect = baseFrame
    return mapPositions(sprites)
  }
  
  func mapPositions(_ sprites: [SpriteProtocol]) -> [CGRect]{
    var frames = [CGRect]()
    for sprite in sprites {
      if let bit = sprite as? ScriptureBitNode {
        let position = mapper.mapPosition(bit.nodeFrame)
        bit.position = position
        frames.append(bit.nodeFrame)
      }
    }
    return frames
  }
  
  
  fileprivate func createScriptureBit(_ parent: SKNode, bit: String) -> ScriptureBitNode{
    let node = ScriptureBitNode(position: CGPoint.zero, text: bit)
    GlobalMainQueue.async{
      parent.addChild(node)
    }
    return node
  }
  
}
