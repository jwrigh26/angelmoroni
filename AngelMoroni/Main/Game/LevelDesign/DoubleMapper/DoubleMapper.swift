//
//  DoubleMapper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/9/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

struct DoubleMapper: Mapper {
  
  fileprivate let xtraSpace: CGFloat = 48 * Device.Scale
  fileprivate let factory = World1SpriteFactory() // TODO: These needs to change per world
  fileprivate var mapper: DoulbeMapperPositionGenerator!
  
  
  init(playerFrame: CGRect){
    mapper = DoulbeMapperPositionGenerator(playerFrame: playerFrame)
  }
  
  func mapDoubleDecker(sprites: [SpriteProtocol], lastFrame: CGRect = CGRect.zero) -> CGRect {
    guard sprites.count == 2 else { fatalError("Sprites must be only 2") }
    let sprite1 = sprites[0]
    let sprite2 = sprites[1]
    
    mapper.lastMappedRect = lastFrame // !Important
    
    do {
      let frames = try getFrames(sprites)
      let positions = mapper.mapPosition(frames)
      
      sprite1.position = positions[0]
      sprite2.position = positions[1]
      
      for sprite in sprites {
        if let platform = sprite as? EnemyPlatformSprite {
          mapper.mapEnemiesToPlatform(platform)
        }
      }
      
      
      let height: CGFloat = sprite1.frame.height
      let lngWidth = max(sprite1.frame.width, sprite2.frame.width)
      
      
      return CGRect(x: positions[0].x + xtraSpace, y: positions[0].y, width: lngWidth + xtraSpace, height: height)
    }catch{
      print("Error occurred: \(error)")
      fatalError()
    }
  }
  
  func mapPositions(_ sprites: [SpriteProtocol]) -> [CGRect] {
    return [CGRect.zero]
  }
  
  fileprivate func getFrames(_ sprites: [SpriteProtocol]) throws -> [CGRect] {
    guard sprites.count == 2 else { throw GameError.arrayMismatch }
    
    var frames = [CGRect]()
    for sprite in sprites {
      if let bit = sprite as? ScriptureBitNode {
        frames.append(bit.nodeFrame)
      }
      if let platform = sprite as? EnemyPlatformSprite {
        frames.append(platform.boundingBox())
      }
    }
    
    if frames.count != 2 {
      throw GameError.arrayMismatch
    }
    
    return frames
  }
  

}
