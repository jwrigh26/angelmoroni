//
//  DoubleMapperPositionGenerator.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/9/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


class DoulbeMapperPositionGenerator: PositionGenerator {
  
  enum area { case top, middle, bottom }
  
  let vSpacer:CGFloat = 54 * Device.Scale
  let hSpacer:CGFloat = 64 * Device.Scale
  let startY: CGFloat = 20
  var bounds: CGRect!
  var maxY: CGFloat!
  var lastMappedRect: CGRect = CGRect.zero
  
  var startHeight: CGFloat = 0
  var diffHeight: CGFloat = 0
  var h1: CGFloat = 0
  var h2: CGFloat = 0
  var x: CGFloat = 0
  
  
  init(playerFrame: CGRect){
    bounds = createBounds(playerFrame)
    maxY = bounds.maxY
  }
  
  func calculateInitialHeight(_ frames: [CGRect])-> CGFloat {
    return frames.reduce(vSpacer) { $0 + $1.height }
  }
  
  
  func mapPosition(_ frames: [CGRect]) -> [CGPoint] {
    guard frames.count >= 2 else { fatalError("Error: \(GameError.arrayMismatch)") }
    
    // First map X position
    x = generateXPos(frames[0])
    
    // Start with the Ys
    startHeight = calculateInitialHeight(frames)
    diffHeight = bounds.height - startHeight
    h1 = frames[0].height
    h2 = frames[1].height
    
    //print("StartHeight \(startHeight) and diff \(diffHeight) and h1 \(h1) and h2 \(h2) and hSpacer \(hSpacer)")
    
    // Log.info?.message("StartHeight \(startHeight) and bounds \(bounds.height) and diff \(diffHeight)")
    // Decide where to stick the difference
    let r = random(min: 1, max: 100)
    var p: [CGPoint]
    switch r {
    case 1...10:
      p = patternTop()
    case 11...20:
      p = patternBottom()
    case 21...30:
      p = patternMiddle()
    case 31...40:
      p = patternEvenDistributeTopMiddle()
    case 41...50:
      p = patternEvenDistributeBottomMiddle()
    default:
      p = patternEvenDistributeTopBottom()
    }
    
   return p
    
  }
  
  func patternTop()-> [CGPoint] {
    let pos1 = CGPoint(x: x, y: startY)
    let spacer = pos1.y + h1 + vSpacer
    let pos2 = CGPoint(x: x, y: spacer)
    return [pos1, pos2]
  }
  
  func patternMiddle()-> [CGPoint] {
    let pos1 = CGPoint(x: x, y: startY)
    let spacer = pos1.y + h1 + vSpacer + diffHeight
    let pos2 = CGPoint(x: x, y: spacer)
    return [pos1, pos2]
  }
  
  func patternBottom()-> [CGPoint] {
    let pos1 = CGPoint(x: x, y: diffHeight)
    let spacer = pos1.y + h1 + vSpacer
    let pos2 = CGPoint(x: x, y: spacer)
    return [pos1, pos2]
  }
  
  func patternEvenDistributeTopMiddle()-> [CGPoint]{
    let d = diffHeight / 2
    let pos1 = CGPoint(x: x, y: d)
    let spacer = pos1.y + h1 + vSpacer + d
    let pos2 = CGPoint(x: x, y: spacer)
    return [pos1, pos2]
  }
  
  func patternEvenDistributeTopBottom()-> [CGPoint]{
    let d = diffHeight / 2
    let pos1 = CGPoint(x: x, y: d)
    let spacer = pos1.y + h1 + vSpacer
    let pos2 = CGPoint(x: x, y: spacer)
    return [pos1, pos2]
  }
  
  func patternEvenDistributeBottomMiddle()-> [CGPoint]{
    let d = diffHeight / 2
    let pos1 = CGPoint(x: x, y: d)
    let spacer = pos1.y + h1 + vSpacer + d
    let pos2 = CGPoint(x: x, y: spacer)
    return [pos1, pos2]
  }
  
  
  func generateXPos(_ rect: CGRect) -> CGFloat {
    
    let min = lastMappedRect.origin.x + lastMappedRect.width + hSpacer
    let padding = random(min: vSpacer, max: vSpacer + hSpacer/2)
    let r = random(min: Int(min), max: Int(min + padding))
    return CGFloat(r)
  }
  
  func createBounds(_ frame: CGRect) -> CGRect{
    let height = ceil(screenSize.height - ((frame.height - vSpacer / 4) + startY))
    return CGRect(x: 0, y: startY, width: screenSize.width, height: height)
  }
  
}
