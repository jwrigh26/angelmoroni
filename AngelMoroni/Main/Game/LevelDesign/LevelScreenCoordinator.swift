//
//  LevelScreenCoordinator.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/10/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}



final class LevelScreenCoordinator: NSObject, GameLoopReady {
  static let sharedInstance = LevelScreenCoordinator()
  
  let GameQueue = GameManager.sharedInstance.gameQueue
  let rocketController = RocketController.sharedInstance
  var index = 1
  
  fileprivate(set) var nodes = [ScreenNode]()
  fileprivate(set) var movingNodes = [ScreenNode]()
  fileprivate var readyToAddNode = false
  fileprivate var screenMapper: LevelScreenMapper?
  
  var playerHitBox: CGRect?
  var playerFrame: CGRect = CGRect.zero
  weak var parent: SKNode?
  
  // Controls how fast to move the nodes
  var pointsPerSecond: CGPoint {
    let gameSpeed: CGFloat = GameManager.sharedInstance.currentGameSpeed
    return CGPoint(x: gameSpeed, y: 0)
  }
  
  func preload(_ factory: SpriteFactory, playerFrame: CGRect, complete: @escaping ()->Void){
    GameQueue.async{ [weak self] in
      
      guard let me = self , GameManager.sharedInstance.scriptureMaster?.totalBits > 0 else {
        print("We have no bits in Scripture Master")
        return
      }
      
      self?.screenMapper = LevelScreenMapper(playerFrame: playerFrame, factory: factory)
      guard let screenMapper = self?.screenMapper else {
        print("Unable to create ScreenMapper")
        return
      }
      
      
      // Set playerFrame
      me.playerFrame = playerFrame
      me.index = 0
      me.readyToAddNode = true
      
      while screenMapper.hasScriptureBits {
        if let node = self?.preloadNextScreen() {
          me.nodes.append(node)
        }
      }
      
      
      me.rocketController.makeRocketPool(factory)
      
      //print("Indexes = \(me.nodes.count) and rocketPool \(me.rocketController.rocketPool.count) and index \(me.index-3)")
      
      GlobalMainQueue.async{
        let node = me.nodes.removeFirst()
        me.movingNodes.append(node)
        me.parent?.addChild(node)
        complete()
      }
    }
  }
  
  func reset(){
    GlobalMainQueue.async{ [weak self] in
      //print("Resetting now!")
      self?.movingNodes.removeAll(keepingCapacity: false)
      self?.parent = nil
      self?.readyToAddNode = false
      self?.nodes.removeAll(keepingCapacity: false)
      RocketController.sharedInstance.resetRocketPool()
    }
  }
  
  
  func update(_ dt: CFTimeInterval){
    for node in movingNodes {
      let x = CGFloat(dt)
      let amountToMove = pointsPerSecond * x
      node.position -= amountToMove
    }
  }
  
  // MARK: Evaluate Actions
  func didEvaluateActions(){
    guard let hitBox = playerHitBox else { return }
    
    for node in movingNodes {
      GameQueue.async{ [weak self] in
        guard let me = self else { return }
        me.evaluateNode(node, hitBox: hitBox)
      }
    }
  }
  
  fileprivate func evaluateNode(_ node: ScreenNode, hitBox: CGRect){
    let x = node.frame.origin.x
    if x <= (screenSize.width/4 + 64) && x >= -(node.nodeFrame.width + 64) {
      node.evaluteActions(hitBox)
    }
  }
  
  // MARK: Finish Update
  
  func didFinishUpdate(){
    if readyToAddNode {
      let node = movingNodes.first
      if checkToAddNode(node) {
        addNodeToMovingNodes()
        return
      }
    }else{
      checkToRemoveNode(movingNodes.first)
    }
  }
  
  fileprivate func checkToAddNode(_ node: ScreenNode?) -> Bool {
    guard let node = node else {
      return false }
    let xDiff  = (screenSize.width - node.nodeFrame.width)
    
    if node.nodeNumber > 1 && node.nodeNumber < index - 3 {
      rocketController.evaluateIfWantToFireRockets(parent, screenNumber: node.nodeNumber)
    }
    
    if node.frame.origin.x <= min(xDiff, 0) { //&&
      //!nodes.isEmpty {
      
      readyToAddNode = false // confusing but basically this has to be true in order to check to add node
      return true
    }
    
    
    
    
    return false
  }
  
  
  fileprivate func addNodeToMovingNodes(){
    guard nodes.indices.contains(0) else { return }
    let node = nodes.removeFirst()
    movingNodes.append(node)
    parent?.addChild(node)
  }
  
  fileprivate func checkToRemoveNode(_ node: ScreenNode?){
    guard let node = node , node.frame.origin.x <= -(node.nodeFrame.width + 32) else { return }
    let _ = movingNodes.removeFirstSafely()
    node.removeAllChildren()
    node.removeFromParent()
    
    readyToAddNode = !nodes.isEmpty
    if movingNodes.isEmpty && nodes.isEmpty {
      GameManager.sharedInstance.gameState = .levelComplete
    }
  }
  
  fileprivate func preloadNextScreen() -> ScreenNode? {
    
    guard let screenMapper = screenMapper , GameManager.sharedInstance.scriptureMaster?.totalBits > 0 else {
      print("We have no bits in Scripture Master")
      return nil
    }
    
    let n = ScreenNode()
    n.prepForGame(index, screenMapper: screenMapper)
    index += 1
    return n
  }
}

