//
//  LevelScreenMapper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/7/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

struct LevelScreenMapper {
  fileprivate let map = ScreenMap()
  
  let device = Device()
  let singleMapper: SingleMapper
  let doubleMapper: DoubleMapper
  let bitMapper: BitMapper
  
  var factory: SpriteFactory!
  
  var scriptureMaster: ScriptureMaster? {
    return GameManager.sharedInstance.scriptureMaster
  }
  
  
  var hasScriptureBits: Bool {
    return !map.scripturePool.isEmpty
  }
  
  init(playerFrame: CGRect, factory: SpriteFactory){
    singleMapper = SingleMapper(playerFrame: playerFrame)
    doubleMapper = DoubleMapper(playerFrame: playerFrame)
    bitMapper = BitMapper(playerFrame: playerFrame)
    self.factory = factory
    
    // Make all sprite pools
    map.scripturePool = makeScriptureBitPool()
  }
  
  var attachDouble: Bool {
    let r = random(min: 1, max: 10)
    return r <= 1
  }
  
  func mapScreen(parent: SKNode) -> CGRect? { //, bits: [String]?
    map.reset()
    while !map.isComplete {
      //      mapSingleSprite(parent)
      //      mapScriptureBits(parent)
      
      // Decide if we are going to make only bits or bits and enemies.
      if bitMapper.mapOnlyBits && !map.mappedOnlyBits  {
        map.mappedOnlyBits = true
        mapScriptureBits(parent)
      }else{
        // Decide if we are going to attach a double or a single
        if attachDouble {
          // We are attaching a double
          mapDoublDecker(parent)
        }else{
          // Just a single
          mapSingleSprite(parent)
        }
      }
    }
    
    return map.lastFrame
  }
  
  
  
  fileprivate func mapSingleSprite(_ parent: SKNode){
    if let sprite = getSingleSprite(parent) {
      if let frame = singleMapper.mapSprite(sprite: sprite, startFrame: map.lastFrame) {
        map.updateScreenMap([frame])
      }
    }
  }
  
  fileprivate func mapDoublDecker(_ parent: SKNode){
    let isSmallScreen = device.isSmallScreen
    let twoEnemies = random(min: 1, max: 3) > 1 ? true : false
    let r = random(min: 1, max: 3)
    let rType: EnemySize = isSmallScreen ? .small : (r > 1) ? .small : .med
    var sprite1: SpriteProtocol?
    var sprite2: SpriteProtocol?
    
    if twoEnemies {
      sprite1 = makeEnemy(.small)
      sprite2 = doublSpriteSecondPicker(rType)
      
    }else {
      // We are going to try and make it with a scripture bit
      if !map.scripturePool.isEmpty {
        sprite1 = map.scripturePool.removeFirstSafely()
      }else{
        sprite1 = makeEnemy(.small)
      }
      sprite2 = doublSpriteSecondPicker(rType)
    }
    if let sprite1 = sprite1, let sprite2 = sprite2 {
      let array = [sprite1, sprite2]
      array.forEach{ $0.addToParent(parent) }
      let sprites: [SpriteProtocol] = [sprite1, sprite2].shuffle()
      let frame = doubleMapper.mapDoubleDecker(sprites: sprites, lastFrame: map.lastFrame)
      map.updateScreenMap([frame])
    }
  }
  
  fileprivate func doublSpriteSecondPicker(_ rType: EnemySize) -> SpriteProtocol?{
    var sprite2: SpriteProtocol?
    if rType == .small {
      sprite2 = makeEnemy(.small)
    }else{
      sprite2 = makeEnemy(.med)
    }
    return sprite2
  }
  
  fileprivate func mapScriptureBits(_ parent: SKNode){
    var bits = [SpriteProtocol]()
    for _ in 1...3 {
      if let bit = map.scripturePool.removeFirstSafely() {
        bits.append(bit)
        bit.addToParent(parent)
      }
    }
    
    guard !bits.isEmpty else { return }
    
    if let bitFrames = bitMapper.mapBits(parent: parent,
                                         sprites: bits,
                                         baseFrame: map.lastFrame){
      map.updateScreenMap(bitFrames)
    }
    
  }
  
  // MARK: Create Methods
  
  fileprivate func getSingleSprite(_ parent: SKNode) -> SpriteProtocol? {
    // Decide if we are adding a scripture bit or enemy
    let r = random(min: 1, max: 10)
    var sprite: SpriteProtocol?
    
    if ScreenMap.enemyMaxReached {
      let bit = map.scripturePool.removeFirstSafely()
      bit?.addToParent(parent)
      ScreenMap.resetEnemyCount()
      return bit
    }
    
    switch r {
    case 1...3:
      let bit = map.scripturePool.removeFirstSafely()
      bit?.addToParent(parent)
      return bit
    case 4...6:
      sprite = makeEnemy(.small)
      ScreenMap.enemyCount += 1
    case 7:
      sprite = makeEnemy(.large)
      ScreenMap.enemyCount += 1
    default:
      sprite = makeEnemy(.med)
      ScreenMap.enemyCount += 1
    }
    
    sprite?.addToParent(parent)
    return sprite
  }
  
  fileprivate func makeEnemy(_ size: EnemySize) -> EnemyPlatformSprite {
    return factory.createEnemyPlatform(size)
  }
  
  fileprivate func makeScriptureBitPool() -> [ScriptureBitNode] {
    var array = [ScriptureBitNode]()
    guard let scriptureMaster = scriptureMaster else { return array }
    while scriptureMaster.hasBits  {
      if let node = factory.createScriptureBit(scriptureMaster.nextBit) {
        array.append(node)
      }
    }
    return array
  }
  
  fileprivate class ScreenMap {
    
    static var enemyCount: Int = 0
    static var enemyMax: Int = 3
    static var enemyMaxReached: Bool {
      return enemyCount >= enemyMax
    }
    
    static var maxScreenWidth: CGFloat = {
      let device = Device()
      let x: CGFloat = device.isSmallScreen ? 3 : 2
      return screenSize.width * x
    }()
    
    static func resetEnemyCount(){
      enemyCount = 0
      enemyMax = random(min: 1, max: 5)
    }
    
    var scripturePool = [ScriptureBitNode]()
    
    
    var mappedOnlyBits: Bool = false
    var frames = [CGRect]()
    var lastFrame: CGRect {
      return frames.last ?? CGRect.zero
    }
    var isComplete: Bool {
      return currentScreenWidth > ScreenMap.maxScreenWidth
    }
    
    var currentScreenWidth: CGFloat = 0
    
    
    func updateScreenMap(_ frames: [CGRect]){
      self.frames.append(contentsOf: frames)
      currentScreenWidth += lastFrame.maxX
    }
    
    func reset(){
      ScreenMap.resetEnemyCount()
      currentScreenWidth = 0
      frames = [CGRect]()
    }
  }
}
