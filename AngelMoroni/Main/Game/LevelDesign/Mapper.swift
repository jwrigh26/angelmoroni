//
//  Mapper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/7/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

protocol Mapper {
  
  
  
  init(playerFrame: CGRect)
  func mapPositions(_ sprites: [SpriteProtocol]) -> [CGRect]
  
}

protocol PositionGenerator: class {
  
  var hSpacer:CGFloat { get }
  var vSpacer:CGFloat { get }
  var lastMappedRect: CGRect { get set }
  
}

extension PositionGenerator {
  
  func safeGuardLastPoisition(){
    let x = lastMappedRect.origin.x
    if x <= 32 {
      let rect = CGRect(x: vSpacer, y: lastMappedRect.origin.y, width: lastMappedRect.width, height: lastMappedRect.height)
      lastMappedRect = rect
    }
  }
  
  func mapEnemiesToPlatform(_ platform: EnemyPlatformSprite){
    if let enemies = platform.linkedEnemies , !enemies.isEmpty {
      self.mapEnemiesToPlatformWorkder(platform.frame, enemies: enemies)
    }
  }
  
  
  func mapEnemiesToPlatformWorkder(_ platform: CGRect, enemies: [EnemySprite]) {
    guard !enemies.isEmpty else {
      print("No Enemies found to Map!")
      return
    }
    
    guard enemies.count > 1 else {
      mapSingleEnemy(platform, enemy: enemies[0])
      return
    }
    
    var mEnemies = enemies.shuffle()
    guard mEnemies.count == 2 else {
      
      preconditionFailure("Can only place max of two enemies") }
    
    let yPos = platform.origin.y + platform.height
    let e1 = mEnemies[0]; let e2 = mEnemies[1]
    e1.adjustPosition(CGPoint(x: platform.origin.x, y: yPos))
    e2.adjustPosition(CGPoint(x: e1.frame.maxX, y: yPos))
    
    let s = platform.width - (e1.frame.width + e2.frame.width)
    let x = floor(s / 3)
    if  x > 3 {
      let _ = e1.position.offset(dx: x, dy: 0)
      let _ = e2.position.offset(dx: x*2, dy: 0)
    }
  }
  
  func mapSingleEnemy(_ platform: CGRect, enemy: EnemySprite){
    let startX = platform.origin.x
    let endX = platform.origin.x + platform.width
    var xPos: CGFloat = startX
    let r = random(min: 1, max: 3)
    switch r {
    case 1:
      xPos = (endX - platform.width / 2) - (enemy.frame.width / 2)
    case 2:
      
      var ok = false
      var attempts = 10
      while(!ok && attempts > 0) {
        let x = random(min: platform.origin.x, max: platform.origin.x + platform.width)
        let r = CGRect(x: x, y: platform.origin.y, width: enemy.frame.width, height: platform.height)
        if platform.contains(r) {
          xPos = x
          ok = true
        }
        attempts -= 1
      }
      
      if attempts == 0 {
        xPos = (endX - platform.width / 2) - (enemy.frame.width / 2)
      }
      
    default:
      break
    }
    
    // TODO TEMP CHANGE
    //    xPos = (endX - platform.width / 2) - (enemy.frame.width / 2)
    
    let yPos = platform.origin.y + platform.height
    enemy.adjustPosition(CGPoint(x: xPos, y: yPos))
  }
  
}

