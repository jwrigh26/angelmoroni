//
//  RocketController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/3/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

enum RocketPattern: Int {
  case single = 1
  case consecutive = 2
  case multiple = 3
  case overwhelm = 4
  
  static func randomPattern() -> RocketPattern {
    let r = random(min: RocketPattern.single.rawValue, max: RocketPattern.overwhelm.rawValue)
    return RocketPattern(rawValue: r)!
  }
}

class RocketController {
  static let sharedInstance = RocketController()
  
  typealias fireCallback = (_ position: CGPoint)-> Void
  
  weak var parent: SKNode?
  
  fileprivate(set) var rocketPool = [Rocket]()
  fileprivate(set) var rockets = [Rocket?]() // Keeps track of rockets in play
  
  let dirtyAlert = EnemyAlert.makeAlert()
  let padding: CGFloat = 8
  
  var firing: Bool = false
  var jetPackId = UUID().uuidString
  var rocketScreens = [Int]() // Keeps track when we show a rocket screen
  var playerPosition = CGPoint.zero
  var rocketPattern: RocketPattern?
  
  
  // Get player position
  // fire rockets
  // Perform hit test
  func update(_ dt: CFTimeInterval,  playerHitBox: CGRect?){
    guard let hitbox = playerHitBox , !rockets.isEmpty else { return }
    playerPosition = CGPoint(x: hitbox.origin.x, y: hitbox.origin.y)
    fireRockets()
    hitTest(hitbox)
  }
  
  
  var randomComparator = 60
  
  func evaluateIfWantToFireRockets(_ parent: SKNode?, screenNumber num:Int){
    
    guard !firing && rockets.isEmpty else {
      return }
    if num % 3 == 0 {
      let r = random(min: 1, max: 100) > randomComparator
//      print("R = \(r) and random \(randomComparator)")
      if r {
//        print("Num = \(num) R = \(r) and firing = \(firing) and empty \(rockets.isEmpty)")
//        print("Fire rockets now! firing \(firing) and rocket count \(rockets.count)")
        prepareRocketsForLaunch(parent)
      }
    }
    
    if num % 2 == 0 {
      randomComparator -= 2
      randomComparator = max(40, randomComparator)
//      print("Random Comparator = \(randomComparator)")
    }
  }
  
  func makeRocketPool(_ factory: SpriteFactory?) {
    var array = [Rocket]()
    guard let factory = factory else { return }
    for _ in 1...20{
      let sprite = factory.createRocket(jetPackId)
      array.append(sprite)
    }
    rocketPool = array
  }
  
  // Called everytime a rocket goes off the screen
  func returnRocket(){
    //print("Returning rocket")
    let notLaunched = rockets.flatMap{ $0?.launched ?? false }.filter{ !$0 }
    
    if notLaunched.isEmpty {
      //print("All rockets have been launched")
      
      firing = false
      removeRockets()
      
    }
  }
  
  // Called when the level resets or back to menu
  func resetRocketPool(){
    
    randomComparator = 60
    firing = false
    removeRockets()
    rocketScreens.removeAll()
    rocketPool.forEach{
      $0.cleanup()
    }
    //print("Rockets in pool are \(rocketPool.count) and rockets \(rockets.isEmpty)")
    //print("reseting rocket pool and r = \(randomComparator)")
  }
  
  func returnRocketsToPool(_ rockets: [Rocket?]){
    rockets.forEach{
      if let rocket = $0 {
        rocket.launched = false
        rocketPool.append(rocket)
      }
    }
  }
  
  // TODO: This is where we will decide what type of rocket display is setup
  // e.g. 1 at a time, multiple at time, etc.
  func prepareRocketsForLaunch(_ parent: SKNode?){
    guard let parent = parent as? GameLayer , rockets.isEmpty else {
      return
    }
    
    var array = [Rocket?]()
    var volley = 1
    let rocketPattern = RocketPattern.randomPattern()
    switch rocketPattern {
    case .single:
      volley = 1
    case .overwhelm:
      volley = random(min: 2, max: 5)
    default:
      volley = random(min: 2, max: 3)
    }
    self.rocketPattern = rocketPattern
    volley = min(volley, rocketPool.count)
    
    // Add rockets to temp array from pool to match value of volley
    while volley > 0 {
      if rocketPool.first?.parent == nil {
        array.append(rocketPool.removeFirst())
      }
      volley -= 1
    }
    
    // Add rockets to parent
    let spriteArray = array.map{ $0 as! SKSpriteNode }
    spriteArray.forEach{ parent.addChild($0) }
    rockets = array
    //print("Preparing Rockets for launch \(rockets.count) and pattern \(rocketPattern)")
  }
  
  
  func fireRockets(){
    guard let pattern = rocketPattern , !firing && !rockets.isEmpty else {
      return }
    firing = true
    
    switch pattern {
    case .consecutive:
      fireConsecutive()
    case .overwhelm:
      fireOverwhelm()
    case .multiple:
      fireMultiple()
    default:
      fireSingleRocket()
    }
  }
  
  func fireSingleRocket(_ replay: Int = 3, fire: fireCallback? = nil){
    // Just testing with one
    guard rockets.indices.contains(0) else { return }
    guard let rocket = rockets[0] else { return }
    alertPlayerAtLastPlayerPosition(replay: replay, wait: 0.5){ rocket.blastOff($0); fire?($0) }
  }
  
  var timer: GCDTimer?
  
  func fireConsecutive(){
    var count = rockets.count - 1 // 1 for zero and 1 for 1st rocket
    fireSingleRocket(1){ [weak self] position in
      guard self?.timer == nil else { return }
      
      self?.timer = GCDTimer(queue: GlobalMainQueue){ [weak self] t in
        guard let rockets = self?.rockets , count > 0 else {
          t?.cancelTimer();
          self?.timer = nil
          return
        }
        
        if let hasIndex = self?.rockets.indices.contains(count){
          if hasIndex {
            if let rocket = rockets[count] {
              self?.alertPlayerAtLastPlayerPosition(replay: 1, wait: 0.5){ rocket.blastOff($0) }
            }
          }
        }
        
        count -= 1
      }
      self?.timer?.startTimer(2.0)
    }
  }
  
  func fireMultiple(){
    let frame = dirtyAlert.frame
    let height = calculateYBounds(screenBounds, rocket: frame)
    let space = height / CGFloat(rockets.count)
    for i in 0..<rockets.count {
      let pos = calculateRocketRandomPosition(space, offset: space * CGFloat(i))
      alertPlayerAtPosition(replay: 0, wait: 1.5, position: pos){ [weak self] in  self?.rockets[i]?.blastOff($0) }
    }
  }
  
  func fireOverwhelm(){
    let frame = dirtyAlert.frame
    let player = playerPosition
    let height = calculateYBounds(screenBounds, rocket: frame)
    let topSpace = height - player.y
    let placeOnTop = topSpace > player.y
    var positions = [CGPoint]()
    var prevPosition: CGPoint = player
    var count = rockets.count
    
    while count > 0 {
      let direction: CGFloat = placeOnTop ? 1 : -1
      let nextY = prevPosition.y + (direction * frame.height)
      
      if nextY < 0 || nextY > height {
        //print("Next Y = \(nextY) and height \(height)")
        count = -1
        break
      }
      
      let position = CGPoint(x: 0, y: nextY)
      positions.append(position)
      prevPosition = position
      count -= 1
    }
    
    if placeOnTop {
      positions.sort{ $0.y < $1.y }
    }else{
      positions.sort{ $0.y > $1.y }
    }
    
    let diff = rockets.count - positions.count
    if diff > 0 { for _ in 1...diff { rockets.removeFirst() } }
  
    if rockets.count == positions.count {
      for i in 0..<rockets.count {
        let d: Double = 0.5 * Double(i)
        delay(d){ [weak self] in
          self?.alertPlayerAtPosition(replay: 0, wait: 1.0, position: positions[i]){ [weak self] in
            self?.rockets[i]?.blastOff($0)
          }
        }
        
      }
    }
    
  }
  
  func alertPlayerAtLastPlayerPosition(replay: Int, wait: TimeInterval, fire:@escaping fireCallback){
    
    let alert = createAlert()
    alert.startAlert()
    
    let fire = SKAction.run{
      alert.endAlert{
        fire(alert.position)
      }
    }
    
    let move = createActionToMoveAlertToLastPlayerPosition(alert, replay: replay, wait: wait)
    let action = (SKAction.afterDelay(wait, performAction: SKAction.sequence([move, fire])))
    alert.run(action)
  }
  
  func alertPlayerAtPosition(replay: Int, wait: TimeInterval, position: CGPoint, fire: @escaping fireCallback){
    
    let alert = createAlert()
    alert.startAlert()
    
    let fire = SKAction.run{
      alert.endAlert{
        fire(alert.position)
      }
    }
    
    let newPosition = CGPoint(x: screenSize.width - (alert.frame.width + padding), y: position.y)
    let move = createActionToMoveAlert(alert, replay: replay, wait: wait, position: newPosition)
    let action = (SKAction.afterDelay(wait, performAction: SKAction.sequence([move, fire])))
    alert.run(action)
  }
  
  
  // Helper method to create an alert and add it to the screen
  fileprivate func createAlert() -> EnemyAlert {
    let alert = EnemyAlert.makeAlert()
    if let gameLayer = self.parent as? GameLayer , alert.parent == nil {
      gameLayer.addChild(alert)
      alert.zPosition = 3
    }
    return alert
  }
  
  // We position the alert on the opposite side of the screen where the play was with a minor delay
  // We then create an animation to follow the player for a # of times
  fileprivate func createActionToMoveAlertToLastPlayerPosition(_ alert: EnemyAlert, replay: Int, wait: TimeInterval) -> SKAction {
    let position = CGPoint(x: screenSize.width - (alert.frame.width + padding), y: playerPosition.y)
    return createActionToMoveAlert(alert, replay: replay, wait: wait, position: position)
  }
  
  fileprivate func createActionToMoveAlert(_ alert: EnemyAlert, replay: Int, wait: TimeInterval, position: CGPoint) -> SKAction {
    alert.position = position
    alert.followCount = replay
    
    let wait = SKAction.wait(forDuration: wait)
    let moveBlock = SKAction.run{ [weak self] in
      guard let pos = self?.playerPosition else { return }
      alert.followPlayer(pos)
    }
    
    let sequence = SKAction.sequence([moveBlock, wait])
    let timer = SKAction.repeat(sequence, count: alert.followCount)
    return timer
  }
  
  fileprivate func hitTest(_ hitbox: CGRect){
    for rocket in rockets {
      rocket?.hitTest(hitbox)
    }
  }
  
  // If rockets is not empty, returns them to the pool and removes them all
  fileprivate func removeRockets(){
    guard !rockets.isEmpty else { return }
    //print("Removing rockets!")
    returnRocketsToPool(rockets)
    rockets.removeAll()
  }
  
  fileprivate func calculateYBounds(_ frame: CGRect, rocket: CGRect) -> CGFloat {
    let height = frame.height - rocket.height*2
    return height
  }
  
  fileprivate func calculateRocketRandomPosition(_ height: CGFloat, offset: CGFloat = 0) -> CGPoint {
    guard offset < height + offset else { fatalError("Offset \(offset) cannot be larger than height \(height)") }
    let y: CGFloat = random(min: offset, max: height + offset)
    return CGPoint(x: 0, y: y)
  }
}
