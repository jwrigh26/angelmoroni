//
//  LevelScreenMapper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/3/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

struct SingleMapper: Mapper {
  
  fileprivate let xtraSpace: CGFloat = 48 * Device.Scale
  fileprivate var mapper: SingleMapperPositionGenerator!
  
  init(playerFrame: CGRect){
    mapper = SingleMapperPositionGenerator(playerFrame: playerFrame)
    
  }
  
  func mapSprite(sprite: SpriteProtocol?, startFrame: CGRect) -> CGRect? {
    
    //print("Last Mapped SpriteFrame \(startFrame)")
    
    guard let sprite = sprite else {
      print("Sprites is empty or nil at mapSprites:")
      return nil
    }
    
    mapper.lastMappedRect = startFrame
    let frames = mapPositions([sprite])
    return frames.first
  }
  
  
  func mapPositions(_ sprites: [SpriteProtocol]) -> [CGRect]{
    var frames = [CGRect]()
    for sprite in sprites {
      if let platform = sprite as?  EnemyPlatformSprite {
        
        let position = mapper.mapPosition(platform.boundingBox())
        platform.position = position
        
        // TODO: TEMP CHANGE
//        platform.position = CGPointMake(64, 100)
        
        
        mapper.mapEnemiesToPlatform(platform)
        let frame = CGRect(x: platform.frame.origin.x + xtraSpace, y: platform.frame.origin.y, width: platform.frame.width + xtraSpace, height: platform.frame.height)
        //frames.append(platform.frame)
        frames.append(frame)
      }
      
      if let bit = sprite as? ScriptureBitNode {
        let position = mapper.mapPosition(bit.nodeFrame)
        
        bit.position = position
        let f = bit.nodeFrame
        let frame = CGRect(x:f.origin.x + xtraSpace, y: f.origin.y, width: f.width + xtraSpace, height: f.height)
        frames.append(frame)
      }
    }
    return frames
  }
}
