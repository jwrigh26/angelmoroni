//
//  SingleMapper.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/7/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

class SingleMapperPositionGenerator: PositionGenerator {
  let hSpacer:CGFloat = 48 * Device.Scale
  let vSpacer:CGFloat = 32 * Device.Scale
  let startY: CGFloat = 8
  var bounds: CGRect!
  var maxY: CGFloat!
  
  var lastMappedRect: CGRect = CGRect.zero
  
  
  init(playerFrame: CGRect){
    bounds = createBounds(playerFrame)
    maxY = bounds.maxY
  }
  
  func mapPosition(_ rect: CGRect) -> CGPoint {
    let x = generateXPos(rect);
    let y = generateYPos(rect)
    let point = CGPoint(x: x, y: y)
    
    safeGuardLastPoisition()
    lastMappedRect = CGRect(x: x, y: y, width: rect.width, height: rect.height)
    return point
  }
  
  func generateYPos(_ rect: CGRect) -> CGFloat {
    let r = random(min: Int(startY), max: Int(maxY - rect.height))
    
    return CGFloat(r)
  }
  
  func generateXPos(_ rect: CGRect) -> CGFloat {
    
    let min = lastMappedRect.origin.x + lastMappedRect.width + hSpacer
    let padding = random(min: vSpacer / 2 , max: hSpacer )
    let r = random(min: Int(min), max: Int(min + padding))
    return CGFloat(r)
  }
  
  func createBounds(_ frame: CGRect) -> CGRect{
    let height = ceil(screenSize.height - ((frame.height - vSpacer / 4) + startY))
    return CGRect(x: 0, y: startY, width: screenSize.width, height: height)
  }
}
