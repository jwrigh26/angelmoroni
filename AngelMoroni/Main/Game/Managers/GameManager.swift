//
//  GameManager.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.


import Foundation
import SpriteKit
import RealmSwift

class GameManager {
  
  static let sharedInstance = GameManager()

  // Game Queues
  lazy var gameQueue: DispatchQueue = {
    return createConCurrentQueue("com.jmw.angelmoroni.gamequeue")
  }()
  
  lazy var angelQueue: DispatchQueue = {
    return createConCurrentQueue("com.jmw.angelmoroni.angelqueue")
  }()
  
  lazy var backgroundQueue: DispatchQueue = {
    return createConCurrentQueue("com.jmw.angelmoroni.backgroundqueue")
  }()
  
  lazy var gameAtlas: SKTextureAtlas = {
    return self.gameSheet.textureAtlas
  }()
  
  lazy var cloudAtlas: SKTextureAtlas = {
    return self.cloudSheet.textureAtlas
  }()
  
  lazy var hudYellowFontName: String = {
    return Device.isPhone ? "hudYellow" : "hudYellow-ipad"
  }()
  
  lazy var hudYellowAtlas: SKTextureAtlas = {
    return SKTextureAtlas(named: self.hudYellowFontName)
  }()
  
  lazy var hudYellowFont: BMGSwiftyFont = {
    return BMGSwiftyFont(name: self.hudYellowFontName, textureAtlas: self.hudYellowAtlas)
  }()
  
  lazy var hudWhiteFontName: String = {
    return Device.isPhone ? "hudWhite" : "hudWhite-ipad"
  }()
  
  lazy var hudWhiteAtlas: SKTextureAtlas = {
    return SKTextureAtlas(named: self.hudWhiteFontName)
  }()
  
  lazy var hudWhiteFont: BMGSwiftyFont = {
    return BMGSwiftyFont(name: self.hudWhiteFontName, textureAtlas: self.hudWhiteAtlas)
  }()
  
  lazy var scriptureBitFontName: String = {
    return Device.isPhone ? "scriptureBit" : "scriptureBit-ipad"
  }()
  
  lazy var scriptureBitAtlas: SKTextureAtlas = {
    return SKTextureAtlas(named: self.scriptureBitFontName)
  }()
  
  lazy var scriptureBitFont: BMGSwiftyFont = {
    return BMGSwiftyFont(name: self.scriptureBitFontName, textureAtlas: self.scriptureBitAtlas)
  }()
  
  lazy var bigTextFontName: String = {
    return Device.isPhone ? "bigText" : "bigText-ipad"
  }()
  
  lazy var bigTextAtlas: SKTextureAtlas = {
    return SKTextureAtlas(named: self.bigTextFontName)
  }()
  
  lazy var scriptureBitParticle: String = {
    return Device.isPhone ? "scriptureBitSpark.sks" : "scriptureBitSpark-ipad.sks"
  }()
  
  lazy var angelDustParticle: String = {
    return Device.isPhone ? "angelDust.sks" : "angelDust-ipad.sks"
  }()
  
  lazy var jetPuffParticle: String = {
    return Device.isPhone ? "jetpuff.sks" : "jetpuff-ipad.sks"
  }()
  
  lazy var jetSmokeParticle: String = {
    return Device.isPhone ? "jetsmoke.sks" : "jetsmoke-ipad.sks"
  }()
  
  lazy var introLight: String = {
    return Device.isPhone ? "introLight.sks" : "introLight-ipad.sks"
  }()
  
  lazy var gameSheet: GameSheet = {
    return GameSheet()
  }()
  
  lazy var cloudSheet: CloudSheet = {
    return CloudSheet()
  }()
  
  lazy var backgroundSpeed: BackgroundSpeed = {
    return BackgroundSpeed()
  }()
  
  lazy var gameSpeed: GameSpeed = {
    return GameSpeed()
  }()
  
  var prevGameSpeedCap: SpeedCap?
  var prevBackgroundSpeedCap: SpeedCap?
  
  var scriptureMaster: ScriptureMaster?
  var levelSummary: HUDLevelSummary?
  
  var skView:SKView?
  
  var gameOver: Bool = false // Game state can change so we keep track if it truly is GameOver
  
  var gameState: GameState = .none {
    didSet {
      if gameState == .gameOver {
        gameOver = true
        skView?.scene?.isPaused = true
      }
      if gameState == .playing {
        skView?.scene?.isPaused = false
      }
      if gameState == .paused {
        skView?.scene?.isPaused = true
      }
      
      if gameState == .levelComplete {
        skView?.isUserInteractionEnabled = false
        skView?.scene?.isPaused = false
        (skView?.scene as? GameScene)?.levelComplete()
      }
      
      if gameState == .levelSummary {
        skView?.scene?.isPaused = false
        skView?.isUserInteractionEnabled = true
      }
      
    }
  }
  
  func resetGameSpeed(){
    backgroundSpeed.resetGameSpeed()
    gameSpeed.resetGameSpeed()
  }
  
  func resetGameSpeedForMenu(){
    resetGameSpeed()
    backgroundSpeed.resetSpeedCapforMenu()
  }
  
  func playerHit(){
    backgroundSpeed.resetGameSpeed()
    gameSpeed.playerHit()
    delay(0.25){ [weak self] in
      self?.gameSpeed.resetGameSpeed()
    }
  }
  
  func update(_ dt: CFTimeInterval) {
    if gameState == .playing {
      backgroundSpeed.update(dt)
      gameSpeed.update(dt)
    }
    
  }
  
  
  
  
  
  
  
}
