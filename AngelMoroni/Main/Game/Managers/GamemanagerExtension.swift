//
//  GamemanagerExtension.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/27/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import SpriteKit

extension GameManager {
  
  var screenSize: CGSize {
    get{ return skView?.bounds.size ?? CGSize.zero }
    set{ self.skView?.bounds.size = newValue }
  }
  
  var currentBackgroundSpeed: CGFloat {
    return backgroundSpeed.currentGameSpeed
  }
  
  var currentGameSpeed: CGFloat {
    return gameSpeed.currentGameSpeed
  }
  
  func saveSpeedCap(){
    prevGameSpeedCap = gameSpeed.speedCap
    prevBackgroundSpeedCap = backgroundSpeed.speedCap
  }
  
  func resetSpeedCap(){
    if let speed1 = prevGameSpeedCap, let speed2 = prevBackgroundSpeedCap {
      gameSpeed.speedCap = speed1
      backgroundSpeed.speedCap = speed2
    }
  }
  
  // MARK: Save Game State
  
  func applicationWillResignActive(_ window: UIWindow?){
    saveSpeedCap()
    GlobalMainQueue.async{
      NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.AppResignedActive), object: self, userInfo: nil)
    }
  }
  
  func applicationDidBecomeActive(_ window: UIWindow?){
    resetSpeedCap()
    GlobalMainQueue.async{
      NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.AppDidBecomeActive), object: self, userInfo: nil)
    }
  }
  
  
  
  func pauseGameScene(){
    saveSpeedCap()
    // This is a special notification that 
    // 1. Let's the scene know to reset
    // 2. Notifies sprites to cleanup actions
    // 3. Not the same as AppResignedActive for it's called during Pause Button tap.
    GlobalMainQueue.async{
      NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.GamePaused), object: self, userInfo: nil)
    }
    
  }
  
  func resumeGameScene(){
    resetSpeedCap()
  }
  
  // MARK: ScriptureMaster
  
  func createScriptureMaster(with level: Level, callback: BasicHandler){
    
    if scriptureMaster != nil {
      scriptureMaster?.resetValues()
      scriptureMaster = nil
    }
    scriptureMaster = ScriptureMaster(level: level)
    scriptureMaster?.makeScriptureBits{
      callback()
    }
  }

  
}
