//
//  NavigationManager.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/16/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

// Note: When first devleoping this I thought I might have more use for this class, but
// as devleopment has gone on less and less is put here. May do away with it completely.
// TODO: Refactor if needed once we reach milestone in development.

import UIKit

typealias WorldPath = (world: World, indexPath: IndexPath)
typealias LevelPath = (level: Level, indexPath: IndexPath)

class NavigationManager {
  
  static let sharedInstance = NavigationManager()
  var currentWorldPath: WorldPath?
  var currentLevelPath: LevelPath?
  //var prevMenu: GameState?
  
  func transitionByGameState(_ state: GameState) {
    
//    switch state {
//    case .world, .worldNoAnimate, .start, .level:
//      prevMenu = state
//    default:
//      break
//    }
    
    
    GameManager.sharedInstance.gameState = state
    let userInfo = [Notification.GameStateKey : GameManager.sharedInstance.gameState.rawValue]
    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.GameState), object: self, userInfo: userInfo)
  }
}
