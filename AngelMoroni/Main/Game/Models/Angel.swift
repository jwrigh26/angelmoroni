//
//  Angel.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 7/4/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit


final class Angel: GameSprite, NotificationListener, GameLoopReady {
  
  let AngelQueue = GameManager.sharedInstance.angelQueue
  let GameQueue = GameManager.sharedInstance.gameQueue
  
  var startY: CGFloat = 8
  
  let baseYVelocity: CGFloat = (UIDevice.current.userInterfaceIdiom == .phone) ? 40 : 54
  let hitBump: CGFloat = 1400
  let maxSpeed: CGFloat = 820.0
  let maxBoost: CGFloat = (UIDevice.current.userInterfaceIdiom == .phone) ? 60 : 80
  let minBoost: CGFloat = (UIDevice.current.userInterfaceIdiom == .phone) ? 14 : 14.4 //12.4 13.8
  let gravity: CGFloat = Device.isPhone ? 11 : 11 //9.8
  
  var counterBoosterFired: Bool = false
  var acceleration: CGFloat = 0.0
  var boost: CGFloat = 0.0
  var staticHeight: CGFloat = 0.0
  var hitBox: SKShapeNode?
  
  var isWalking: Bool = false
  var isTakingOff: Bool = false {
    didSet {
      if isTakingOff { characterState = .takeOff }
    }
  }
  var isTouchingDown: Bool = false {
    didSet {
      if isTouchingDown { characterState = .touchdown }
    }
  }
  
  lazy var idleBlock:SKAction =  {
    return SKAction.run{ [weak self] in
      self?.characterState = .idle
    }
  }()
  
  lazy var recoverBlock: SKAction = {
    return SKAction.run{ [weak self] in
      self?.characterState = .recovering
      }
  }()
  
  lazy var touchdownBlock: SKAction = {
    return SKAction.run{ [weak self] in
      self?.characterState = .walking
      self?.isWalking = true
      }
  }()
  
  
  lazy var takeoffAction: SKAction = {
    return SKAction.sequence([AngelAction.takeoff, self.idleBlock])
  }()
  
  lazy var touchdownAction: SKAction = {
    return SKAction.sequence([AngelAction.touchdown, self.touchdownBlock])
  }()
  
  lazy var takingdamageAction: SKAction = {
    return SKAction.sequence([AngelAction.damage, self.recoverBlock])
  }()
  
  lazy var recoveringAction: SKAction = {
    return SKAction.sequence([AngelAction.repeatRecoveringAction,
      AngelAction.halfpoint, AngelAction.wait,
      self.idleBlock])
  }()
  
  lazy var xPos: CGFloat = {
    let w: CGFloat = Device.isPhone ? self.frame.width * 1.2 : self.frame.width * 1.7
    return self.screenSize.width/3 - w
  }()
  
  var isFlying: Bool = false {
    didSet {
      // The first time they tap we want to give an extra push to things get going
      counterBoosterFired = isFlying
    }
  }
  
  var isTakingDamage: Bool {
    return characterState != .takingDamage &&
      characterState != .recovering &&
      characterState != .dead &&
      characterState != .touchdown &&
      characterState != .takeOff ? false : true
  }
  
  
  var notificationHit: NSNotificationProxy?
  var notificationResume: NSNotificationProxy?
  
  override init(texture: SKTexture, position: CGPoint = CGPoint.zero) {
    super.init(texture: texture, position: position)
    
    startUp()
    listenForNotifications()
    //traceHitBox()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func listenForNotifications(){
    guard notificationHit == nil && notificationResume == nil else {
      notificationResume?.stop()
      notificationHit?.stop()
      return
    }
    
    notificationResume = NSNotificationProxy(notificationKey: Notification.GamePaused){ [weak self] sender in
      guard let me = self else { return }
      me.AngelQueue.async{
        self?.resetState()
      }
    }
    
    notificationHit = NSNotificationProxy(notificationKey: Notification.EnemyHitDetected){ [weak self] sender in
        self?.takenDamage()
    }
  }
  
  deinit {
//    notificationHit?.stop()
//    notificationResume?.stop()
  }
  
  override func getCharacterStateSKAction(_ state: CharacterState) -> stateSKAction {
    
    var action: SKAction?
    
    switch(state) {
    case .idle:
      action = AngelAction.idelSequence
    case .walking:
      action = AngelAction.walkSequence
    case .takeOff:
      action = takeoffAction
    case .touchdown:
      action = touchdownAction
    case .flying:
      action = AngelAction.flySequence
    case .takingDamage:
      action = takingdamageAction
    case .recovering:
      action = recoveringAction
    case .dead:
      action = AngelAction.dead
    default:
      break
    }
    return (action, "currentAngelAction")
  }
  
  
  override func update(_ dt: CFTimeInterval) {
    updateFlying(dt)
  }
  
  
  override func didEvaluateActions() {
    GameQueue.async(flags: .barrier, execute: { [weak self] in
      self?.updateCharacterState()
      // Next update playerHitBox for LevelScreenCoordinator
      LevelScreenCoordinator.sharedInstance.playerHitBox = self?.boundingBox()
    })
  }
  
  func didFinishUpdate() {}
  
  func updateCharacterState(){
    
    guard characterState == .flying ||
      characterState == .idle ||
      characterState == .walking else { return }
    
    if isFlying  {
      characterState = .flying
    }else if isWalking {
      characterState = .walking
    }else{
      characterState = .idle
    }
    
  }
  
  func frameAdjustment() -> CGRect {
    let xCropAmount = self.frame.width * 0.8
    let yCropAmount = self.frame.height * 0.5
    let yOffset:CGFloat = ((frame.height - yCropAmount)/4) //scale
    let xOffset:CGFloat = ((frame.width - xCropAmount)/4) //scale
    
    return CGRect(x: xOffset, y: yOffset, width: xCropAmount, height: yCropAmount)
  }
  
  override func boundingBox() -> CGRect {
    let box = frameAdjustment()
    let xPos = frame.origin.x + box.origin.x
    let yPos = frame.origin.y + box.origin.y
    return CGRect(x: xPos, y: yPos, width: box.width, height: box.height)
  }
  
  func traceHitBox() {
    let box = boundingBox()
    let scale = Device.Scale
    let size = CGSize(width: box.width/scale, height: box.height/scale)
    
    
    // TEMP
    let xCropAmount = self.frame.width * 0.8
    let x = ((frame.width - xCropAmount)/4)/scale
    //
    
    let rect = CGRect(x: x, y: box.origin.y / scale, width: size.width, height: size.height)
    
    //print("BoundingBox \(boundingBox()) and frame \(frame)")
    let shape = SKShapeNode.debugBox(rect, color: SKColor.red)
    self.addChild(shape)
  }
  
  fileprivate func takenDamage(){
    guard let parent = self.scene as? GameScene , !isTakingDamage else { return }
    
    
    GameManager.sharedInstance.playerHit()
    characterState = .takingDamage
    parent.isUserInteractionEnabled = false
    
    //Log.info?.message("acceleration is \(acceleration)")
    // Bump up or down
    let d: CGFloat = acceleration > 0 ? 800 : -200
    
    acceleration = 0
    acceleration = d
    //Log.info?.value(d)
    
    initCounterBoost(&acceleration)
    isFlying = false
    
    parent.hudLayer.takeDamage(){ [weak self] gameOver in
      if gameOver {
        self?.characterState = .dead
        GameManager.sharedInstance.gameState = .angelFallen
        parent.colorGlitch()
      }else{
        parent.isUserInteractionEnabled = true
      }
      parent.screenShakeByAmt(3)
    }
  }
  
  // Run when first created
  func startUp(){
    staticHeight = frame.height
    anchorPoint = CGPoint(x: 0, y: 0)
    
    position = CGPoint(x: -frame.width, y: startY)
    boost = minBoost
    characterState = .walking
    characterHealth = Health.Max
    zPosition = ZPos.Angel
    isTouchingDown = true
  }
  
  
  func checkAndClampSpritePosition(){
    let clampHeight = staticHeight + staticHeight/2
    
    if position.y > startY { isWalking = false } // Reset so not walking in air

    if position.y >= startY && position.y <= startY && !isTakingDamage { // Handle take off
      //isTakingOff = acceleration < 0 && !isTakingOff ? true : false
      if acceleration < 0 && !isTakingOff {
        isTakingOff = true
        isTouchingDown = false
      }
    }
    
    if position.y >= startY && position.y <= startY  && !isTakingDamage { // prepare for touch down
      
      if acceleration > 0 && !isTouchingDown {
        isTouchingDown = true
        isTakingOff = false
      }
    }
    
    
    // On The ground
    if self.position.y <= startY && characterState != .dead{
      
      if !isTakingDamage {
        isWalking = true
        isFlying = false
      }
      self.position = CGPoint(x: self.position.x, y: startY)
      acceleration = 0
      
    } else if self.position.y > screenSize.height - clampHeight {
      self.position.y = screenSize.height - clampHeight
      
      
      if !isFlying {
        // At this point acceleration is a negative number,
        // so we want to counter it and give it boost to
        // have the angel start to fall down
        // Don't ask me how I came up with this.
        // It was through a night of just messing around with it
        acceleration /= 2
        //print("Not flying and accel = \(acceleration)")
        initCounterBoost(&acceleration)
        acceleration = -(acceleration/2)
        initCounterBoost(&acceleration)
      }
    }
    
    
    if characterState == .dead && self.position.y < -staticHeight * 4 {
      NavigationManager.sharedInstance.transitionByGameState(.gameOver)
      return
    }
  }
  
  
  // Every time the user taps, we want to fake some kind of physics for an upboost
  // So this gives the angel a little extra boost to push them upwards
  func initCounterBoost(_ acceleration: inout CGFloat){
    counterBoosterFired = false
    acceleration = -acceleration/4
    //print("acceleration \(acceleration)")
  }
  
  // Move the angle on the y access points per second down
  func angelFall (_ dt: CFTimeInterval, velocity: CGPoint){
    let amountToMove = CGPoint(x: velocity.x * CGFloat(dt), y: velocity.y * CGFloat(dt + dt/6))
    let newPosition = CGPoint(x: position.x - amountToMove.x, y: position.y - amountToMove.y)
    GlobalMainQueue.async{ [weak self] in
      self?.position = newPosition
      self?.checkAndClampSpritePosition()
    }
  }
  
  
  func updateFlying(_ dt: CFTimeInterval){
    AngelQueue.sync{ [weak self] in
      self?.updateFlyingworker(dt)
    }
  }
  
  func updateFlyingworker(_ dt: CFTimeInterval){
    if abs(acceleration) < maxSpeed { acceleration += gravity }
    
    if isFlying {
      if counterBoosterFired {
        //print("CounterBoosterFired for updateFlying worker")
        initCounterBoost(&acceleration)
      }
      acceleration -= boost // This is where boost is used. subtract from accel
      if boost < maxBoost {
        boost += (UIDevice.current.userInterfaceIdiom == .phone) ? 0.14 : 0.2
      }
      
      // This is to get the angel off the ground
      if position.y <= startY {
        acceleration = -maxBoost
      }
      
    } else {
      boost = minBoost // Reset it so next time they tap it's not uber strong
    }
    
    // After all that is calculated, let's make sure we didn't exceet maxSpeed
    if abs(acceleration) >= maxSpeed {
      // Here we are concerened only with the angel going up so we check for negative values
      if acceleration < 0 {
        acceleration = -maxSpeed
      }
    }
    
    let pointsPerSecond = baseYVelocity + acceleration
    angelFall(dt, velocity: CGPoint(x: 0, y: (pointsPerSecond)))
  }
  
  fileprivate func resetState(){
    NSObject.cancelPreviousPerformRequests(withTarget: self)
    removeAllActions()
    isFlying = false
    isWalking = false
    
    //print("Need to change character state -> \(characterState)")
    if characterState == .dead {
      return
    }
    
    if characterState == .walking || characterState == .touchdown  {
      isTouchingDown = true
      isTakingOff = false
      isWalking = true
      overrideCharacterState(.walking)
    }
    
    if characterState == .takeOff || position.y >= startY || isTakingDamage {
      isTouchingDown = false
      isTakingOff = true
      isWalking = false
      overrideCharacterState(.idle)
    }
  }
  
}
