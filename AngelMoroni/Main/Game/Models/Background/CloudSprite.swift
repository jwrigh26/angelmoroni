//
//  CloudSprite.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

final class CloudSprite: GameSprite, ParallaxSpriteProtocol {
  
  
  let offset:CGFloat = UIScreen.main.scale * 2
  static var filePath: String = "Clouds/cloud_b"
  var scrollOrder = 0
  var velocity = CGPoint.zero
  var yPos: CGFloat = 0
  var parallxPosition:ParallaxPosition = ParallaxPosition.back
  weak var linkedSprite: SKSpriteNode?
 
  var scrollSpeed: CGFloat {
    return calculateScrollSpeed()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  init(texture:SKTexture, parallxPosition:ParallaxPosition, scrollOrder:Int){
    super.init(texture: texture, position: CGPoint.zero)
    self.parallxPosition = parallxPosition
    self.scrollOrder = scrollOrder
    anchorPoint = CGPoint.zero
    setScrollPosition()
  }
  

  
  func calculateScrollSpeed() -> CGFloat {
    let currentSpeed:CGFloat = GameManager.sharedInstance.currentBackgroundSpeed
    var d:CGFloat = 1
    switch parallxPosition {
    case .back:
      d = 4
    case .middleBack:
      d = 3
    case .middleFront:
      d = 2
    default:
      break
    }
    return ceil(currentSpeed / d)
  }
  
  
 static func generateFileName(_ parallxPosition: ParallaxPosition) -> String {
    let value = parallxPosition.rawValue
    let max = (parallxPosition == .middleFront) || (parallxPosition == .front) ? 6 : 3
    let number = random(min: 1, max: max)
    let fileName = "\(CloudSprite.filePath)\(value)_0\(number)"
    return fileName
  }
  
  func update(_ dt: CFTimeInterval, accelerate: Bool = true){
    
    move(dt, velocity: CGPoint(x: scrollSpeed, y: 0))
    
    // Only resets if linked sprite is less than the negative value of it's size
    resetScrollPosition(){ [weak self] in
      if let parent = self?.parent as? BackgroundLayer, let pos = self?.parallxPosition {
        let fileName = CloudSprite.generateFileName(pos)
        let texture = parent.cloud(fileName)
        self?.texture = texture
      }
    }
  }

}
