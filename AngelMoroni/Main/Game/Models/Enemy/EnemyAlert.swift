//
//  EnemyAlert.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/2/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

/*
 let blink = SKAction.sequence(
 [
 SKAction.fadeAlphaTo(0.0, duration: blinkDuration),
 SKAction.waitForDuration(blinkDuration),
 SKAction.fadeAlphaTo(1.0, duration: blinkDuration)
 ]
 )
 
 
 if count % 2 == 0 {
 blinkCount += 1
 blinkDuration -= 0.1
 }
 blinkCount = min(blinkCount, 6)
 blinkDuration = max(blinkDuration, 0.1)
 count -= 1
 
 runAction(SKAction.repeatAction(blink, count: blinkCount), withKey: "blink")
 */

import SpriteKit

final class EnemyAlert: GameSprite {
  
  let fadeIn = SKAction.fadeIn(withDuration: 0.25)
  let fadeOut = SKAction.fadeOut(withDuration: 0.25)
  
  static func makeAlert() -> EnemyAlert {
    let texture = GameSprite.gameSheet.Enemies_Warning()
    let bounds = UIScreen.main.bounds
    let alert = EnemyAlert(texture: texture, position: CGPoint(x: bounds.width + 1000, y: 0))
    return alert
  }
  
  var blinkDuration = 0.1
  var followCount = 3
  
  override init(texture: SKTexture, position: CGPoint = CGPoint.zero) {
    super.init(texture: texture, position: position)
    anchorPoint = CGPoint(x: 0, y: 0)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func followPlayer(_ playerPosition: CGPoint){
    let moveEffect = SKTMoveEffect(node: self, duration: 0.35, startPosition: position, endPosition: CGPoint(x: position.x, y: playerPosition.y))
    moveEffect.timingFunction = SKTTimingFunctionSmoothstep
    let move = SKAction.actionWithEffect(moveEffect)
    run(move)
  }
  
  func endAlert(_ ended: @escaping ()-> Void){
    removeAllActions()
    let block = SKAction.run{ [weak self] in
      ended()
      self?.removeAllActions()
      self?.removeFromParent()
    }
    run(SKAction.sequence([fadeIn, SKAction.wait(forDuration: 0.15), fadeOut, block]))
  }
  
  func startAlert(){
    alpha = 0.0
    let blink = SKAction.sequence(
      [
        SKAction.fadeAlpha(to: 0.0, duration: blinkDuration),
        SKAction.wait(forDuration: blinkDuration),
        SKAction.fadeAlpha(to: 1.0, duration: blinkDuration)
      ]
    )
    let blinkRepeat = SKAction.repeatForever(blink)
    run(SKAction.sequence([fadeIn, SKAction.wait(forDuration: 0.35), blinkRepeat]))
  }
  
}
