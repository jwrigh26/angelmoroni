//
//  EnemyPlatformSprite.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/29/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

enum Platform: Int {
  case small = 2, medium = 3, large = 4, xLarge = 5
  
  var enemies: Int {
    var number = 0
    switch self {
    case .small, .medium:
      number = random(min: 1, max: 2)
    case .large, .xLarge:
      number = random(min: 2, max: 3)
    }
    return number
  }
}

protocol PlatformLinkable: class {
  var linkingUUID: String? { get set }
}

class PlatformLinkableEnemySprite: EnemySprite, PlatformLinkable, NotificationListener {
  
  var linkingUUID: String?
  var notificationResume: NSNotificationProxy?
  
  override func adjustPosition(_ position: CGPoint) {
    let p = CGPoint(x: position.x + frame.width / 2 + posOffset.x, y: position.y + posOffset.y)
    self.position = p
  }
  
  
  override init(texture: SKTexture, position: CGPoint) {
    super.init(texture: texture, position: position)
    self.anchorPoint = CGPoint(x: 0.5, y: 0)
    flipSprite()
    listenForNotifications()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func listenForNotifications(){
    guard notificationResume == nil else {
      notificationResume?.stop()
      return
    }
    
    notificationResume = NSNotificationProxy(notificationKey: Notification.GamePaused){ [weak self] sender in
      GameManager.sharedInstance.gameQueue.async{ [weak self] in
        self?.onPaused()
      }
    }
  }
  
  deinit {
//    notificationResume?.stop()
  }
  
  override func getCharacterStateSKAction(_ state: CharacterState) -> stateSKAction {
    return super.getCharacterStateSKAction(state)
  }
  
  func onPaused() {
    removeAllActions()
    overrideCharacterState(.idle)
  }
  
  func flipSprite(){
    let r = random(min: 1, max: 3)
    let d: CGFloat = r == 3 ? -1 : 1
    guard abs(d) == 1 else { preconditionFailure("Must be 1 or -1") }
    xScale = xScale * d
  }
  
}


class EnemyPlatformSprite: EnemySprite {
  
  var uuid = UUID().uuidString
  var enemySize: EnemySize = .small
  var linkedEnemies: [EnemySprite]?
  var platform: Platform
  
  override func addToParent(_ parent: SKNode){
    parent.addChild(self)
    if let array = linkedEnemies , !array.isEmpty {
      for enemy in array { parent.addChild(enemy) }
    }
  }
  
  
  override var hitBox: CGRect {
    return frame // Override because boundingbox takes in consideration for encapuslated enemies
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override init(texture: SKTexture, position: CGPoint) {
    platform = .medium
    linkedEnemies = [EnemySprite]()
    super.init(texture: texture, position: position)
  }
  
  lazy var adjustedHeight: CGFloat = {
    guard let enemies = self.linkedEnemies , !enemies.isEmpty else {
      return self.frame.height
    }
    
    let heights = enemies.map{ $0.frame }.sorted{ $0.height > $1.height }
    let height = self.frame.height + heights[0].height
    return height
  }()
  
  override func boundingBox() -> CGRect {
    guard let enemies = linkedEnemies , !enemies.isEmpty else { return frame }
    return CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.width, height: adjustedHeight)
  }
  
  func calculateNumberOfEnemiesAllowed(_ size: EnemySize) -> [EnemySize] {
    
    var enemies = [EnemySize]()
    // TODO: TEMP CHANGE
    //    enemies.append(.Small)
    
    // Small Options
    if platform == .small && size == .small {
      enemies.append(.small)
    }
    else if platform == .small && size == .med {
      enemies.append(.med)
    }else if platform == .small && size == .large {
      enemies.append(.med)
    }
      
      // Medium Options
    else if platform == .medium && size == .small {
      enemies.append(.small)
    }
    else if platform == .medium && size == .med {
      enemies.append(.med)
    }
    else if platform == .medium && size == .large {
      enemies.append(.large)
    }
      
      // Large Options
    else if platform == .large && size == .small {
      let r = random(min: 1, max: 10)
      enemies = r < 5 ? [.small, .small] : [.small]
    }
    else if platform == .large && size == .med {
      let r = random(min: 1, max: 10)
      enemies = r < 5 ? [.med, .small] : [.med]
    }
    else if platform == .large && size == .large {
      let r = random(min: 1, max: 10)
      enemies = r < 5 ? [.large] : [.large, .small]
    }
      
      // XLarge Options
    else if platform == .xLarge && size == .small {
      let r = random(min: 1, max: 10)
      enemies = r < 5 ? [.small, .small] : [.small]
    }
    else if platform == .xLarge && size == .med {
      let r = random(min: 1, max: 10)
      enemies = r < 5 ? [.med, .small] : [.med]
    }
    else if platform == .xLarge && size == .large {
      let r = random(min: 1, max: 10)
      switch r {
      case 1...3:
        enemies = [.large]
      case 4...5:
        enemies = [.large, .med]
      default:
        enemies = [.large, .small]
      }
      
      enemies = r < 5 ? [.large] : [.large, .small]
      
    }
    
    return enemies
  }
  
  let randomEnemySize = { (min: EnemySize, max: EnemySize) -> EnemySize in
    let r = random(min: min.rawValue, max: max.rawValue)
    return EnemySize(rawValue: r)!
  }
  
  func sweepForHit(_ playerHitBox: CGRect, box: CGRect) -> Bool {
    // We checked adjusted Bounding box and it detected a hit
    // So now we go through and confirm a hit
    // This setup works for the platform because hitbox is just the platform in the node space without adjusted Height
    let newFrame = CGRect(x: box.origin.x , y: box.origin.y, width: hitBox.width, height: hitBox.height)
    
    //    return sweepLinkedEnemeisForHit(playerHitBox, box: box)
    
    if playerHitBox.intersects(newFrame){
      //      Log.verbose?.message("Box -> \(box)")
      //      Log.verbose?.message("Platform -> \(hitBox)")
      //      Log.verbose?.message("Player Hitbox -> \(playerHitBox)")
      return onHit()
    }else{
      return sweepLinkedEnemeisForHit(playerHitBox, box: box)
    }
  }
  
  func sweepLinkedEnemeisForHit(_ playerHitBox: CGRect, box: CGRect) -> Bool {
    // Loop through all enemies to see if it hit them
    guard let enemies = linkedEnemies else { return false }
    
    let platform = hitBox
    for (_, rect) in enemies.enumerated() {
      let x: CGFloat = rect.hitBox.origin.x - platform.origin.x + box.origin.x + rect.posOffset.x
      let y: CGFloat = box.origin.y + platform.height + rect.posOffset.y
      let newFrame = CGRect(x: x, y: y, width: rect.hitBox.width, height: rect.hitBox.height)
      
      if playerHitBox.intersects(newFrame){
        //        Log.debug?.message("Enemy -> \(newFrame)")
        //        Log.verbose?.message("Box -> \(box)")
        //        Log.verbose?.message("Platform -> \(hitBox)")
        //        Log.verbose?.message("Player Hitbox -> \(playerHitBox)")
        
        return onHit()
      }
    }
    return false
  }
  
  func repopulateLinkedEnemies(_ parent: SKNode){
    
    var i = 0
    for child in parent.children {
      if let linker = child as? PlatformLinkable {
        if linker.linkingUUID == uuid {
          linkedEnemies?.append(linker as! EnemySprite)
        }
      }
      i += 1
    }
  }
}
