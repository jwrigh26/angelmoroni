//
//  MobSprite.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/15/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

protocol Enemy: class, Hitable, Traceable {
  var posOffset: CGPoint { get set }
  func adjustPosition(_ position: CGPoint)
}


class EnemySprite: GameSprite, Enemy {
  
  var posOffset: CGPoint = CGPoint.zero
  
  var hitBox: CGRect {
    return boundingBox()
  }
  
  var hit:Bool = false {
    didSet {
//      Log.debug?.message("----> HIT <-----")
      postHitNotification()
      hit = false
    }
  }
  
  func adjustPosition(_ position: CGPoint) {
    let p = CGPoint(x: position.x + posOffset.x, y: position.y + posOffset.y)
    self.position = p
  }
  
  func onHit()-> Bool {
    if hit { return false }
    hit = true;
    return hit
  }
  
  func traceHitBox() {
    let rect = makeTraceShape(boundingBox())
    let x = floor(rect.frame.maxX)
    let y = floor(rect.frame.maxY)
    rect.position = CGPoint(x: x , y: y)
    addChild(rect)
  }
  
  override init(texture: SKTexture, position: CGPoint) {
    super.init(texture: texture, position: position)
    anchorPoint = CGPoint(x: 0, y: 0)
    characterState = .random
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func cleanup() {
    removeAllActions()
    removeAllChildren()
    removeFromParent()
  }
  
  override func update(_ dt: CFTimeInterval){}
  
  override func getCharacterStateSKAction(_ state: CharacterState) -> stateSKAction {
    var action: SKAction?
    switch(state) {
    case .random:
      action = randomAction
    default:
      action = makeIdleAction()
    }
    return (action, "currentEnemy\(self)Action")
  }
  

  func makeIdleAction() -> SKAction? {
    guard let action = idleAction else { characterState = .none; return nil }
    let wait = SKAction.wait(forDuration: TimeInterval(random(min: 0.1, max: 0.3)))
    let idleLoop = SKAction.sequence([wait, action])
    
    let block = SKAction.run{ [weak self] in
      self?.characterState = .random
    }
    let seq = SKAction.sequence([
      idleLoop, block
      ])
    return seq
  }
  
  var idleAction: SKAction? {
    return nil
  }
  
  
  var randomAction: SKAction? {
    guard let array = randomActionArray else { characterState = .idle; return nil }
    
    
    let r = random(min: 1, max: array.count)
    let action = array[r-1]
    
    let block = SKAction.run{ [weak self] in
      self?.characterState = .idle
    }
    
    return SKAction.sequence([
      action, block
      ])
  }
  
  var randomActionArray: [SKAction]? {
    return nil
  }
  
  
  
  fileprivate func postHitNotification(){
    GlobalMainQueue.async{
      NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.EnemyHitDetected), object: self, userInfo: nil)
    }
  }
}
