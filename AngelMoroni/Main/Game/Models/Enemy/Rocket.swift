//
//  Rocket.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/2/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


protocol Rocket: class, ArcRotatable {
  var position: CGPoint { get set }
  var frame: CGRect { get }
  var characterState:CharacterState { get set }
  var prevPosition: CGPoint { get set }
  var zPosition: CGFloat { get set }
  var parent: SKNode? { get }
  var launched: Bool { get set }
  var alert: EnemyAlert { get set }
  
  func blastOff(_ position: CGPoint)
  func hitTest(_ playerHitBox: CGRect)
  func removeFromParent()
  func removeAllActions()
  func stopParticles()
  func runParticles()
  func runAction(_ action: SKAction)
  func cleanup()
}

extension Rocket where Self: SKSpriteNode {
  
  func postRemoveRocket(){
    //print("Post Remove Rocket")
    RocketController.sharedInstance.returnRocket()
  }
}
