//
//  LevelSummary.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/30/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation

class HUDLevelSummary {
  let hartsMax: Int
  var harts: Int
  var bits: Int
  
  var totalBits: Int {
    return GameManager.sharedInstance.scriptureMaster?.totalBits ?? 0
  }
  
  var degreeOfGlory: DegreeOfGlory {
    return calculateDegreeOfGlory()
  }
  
  init(bits: Int, harts: Int, hartsMax: Int){
    self.bits = bits
    self.harts = harts
    self.hartsMax = hartsMax
  }
  
  fileprivate func percent(_ p: Double) -> Int {
    return Int(Double(totalBits) * p)
  }
  
  fileprivate func calculateDegreeOfGlory() -> DegreeOfGlory {
  
    var points = 0
    //let p = Int(Double(totalBits) * 0.8)
    
    //bits = Int(Double(totalBits) * 0.3)
    //harts = 2
    
    //print("bits = \(bits) and total \(totalBits)")
    
    
    switch bits {
    case totalBits:
      points += 7
    case percent(0.9)..<totalBits:
      points += 6
    case percent(0.8)..<percent(0.9):
      points += 5
    case percent(0.7)..<percent(0.8):
      points += 4
    case percent(0.6)..<percent(0.7):
      points += 3
    case percent(0.5)..<percent(0.6):
      points += 2
    default:
      points += 1
    }
    
    
    //print("Points for bits \(points)")
    
    if harts >= hartsMax {
      points += 3
    }else if harts == hartsMax - 1 {
      points += 2
    }else{
      points += 1
    }
    
    // Lets add a penlaty for full harts but less than half of bits
    if harts >= hartsMax && bits <= totalBits/2 {
      points = 5 // meaning you get no reward for collecting bits
    }
    
    if bits == 0 {
      return .none
    }
    
    //print("Total Points \(points)")
    
    
    // TODO: Come back and make graphic for perfect run!
    // If a user gets 5 points show a graphic at the end that says perfect run
    switch points {
    case 10..<99:
      //print("!!--- SUN ---!!")
      return .sun
    case 8..<10:
      //print("!!--- MOON ---!!")
      return .moon
    case 4..<8:
      //print("!!--- STAR ---!!")
      return .star
    default:
      return .none
    }
    
    
  }
  
}
