//
//  HartSprite.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/27/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

final class HartSprite: GameSprite {
  
  var hit: Bool = false
  func swapTexure(){
    hit = !hit
    let texture = hit ? GameSprite.gameSheet.HUD_hart_gone() : GameSprite.gameSheet.HUD_hart()
    self.texture = texture
  }
  
  func brakeMyHart()-> Bool {
    if !hit {
      swapTexure()
      return true
    }
    return false
  }
  
  override init(texture: SKTexture, position: CGPoint = CGPoint.zero) {
    super.init(texture: texture, position: position)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
