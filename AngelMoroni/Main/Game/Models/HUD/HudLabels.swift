//
//  HudLabels.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/30/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


final class HudYellowLabel: BMGSwiftyLabel {
  
  override var font: BMGSwiftyFont! {
    get {
      return GameManager.sharedInstance.hudYellowFont
    }
    set {
      // do nothing
    }
  }
  
  
  internal init(text: String) {
    super.init()
    self.text = text
  }
  
  internal required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
}

final class HudWhiteLabel: BMGSwiftyLabel {
  
  override var font: BMGSwiftyFont! {
    get {
      return GameManager.sharedInstance.hudWhiteFont
    }
    set {
      // do nothing
    }
  }
  
  
  internal init(text: String) {
    super.init()
    self.text = text
  }
  
  internal required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
}
