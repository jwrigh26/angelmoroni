//
//  Types.swift
//  AngelMoroni
//
//  Created by Justin Wright on 2/14/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import SpriteKit

typealias BasicHandler = () -> Void // For this example, not sure where to put this..so yeah throw it in the AppDelegate.


enum CharacterState: Int {
  case none
  case spawning
  case idle
  case walking
  case takingDamage
  case takeOff
  case touchdown
  case flying
  case recovering
  case dead
  case random
}

enum BitType {
  case start
  case middle
  case end
  
  var texture: SKTexture {
    let sheet = GameManager.sharedInstance.gameSheet
    switch self {
    case .start:
      return sheet.Scriptures_scripture_bit_start()
    case .middle:
      return sheet.Scriptures_scripture_bit_middle()
    case .end:
      return sheet.Scriptures_scripture_bit_end()
    }
  }
}


enum EnemyAction {
  static let delay = SKAction.wait(forDuration: TimeInterval(random(min: 0.25, max: 0.5)))
}


enum SlurpeeImpAction {
  static let stand = SKAction.setTexture(GameSprite.gameSheet.Enemies_SlurpeeImp_slurpee_imp_01())
  static let idle = SKAction.sequence([SlurpeeImpAction.stand, EnemyAction.delay])
  static let blink = SKAction.animate(with: GameSprite.gameSheet.SlurpeeImpBlink(), timePerFrame: 0.3, resize: true, restore: false)
  static let slurp = SKAction.animate(with: GameSprite.gameSheet.SlurpeeImpSlurp(), timePerFrame: 0.16, resize: true, restore: false)
  static let doublGulp = SKAction.animate(with: GameSprite.gameSheet.SlurpeeImpDoubleSlurp(), timePerFrame: 0.16, resize: true, restore: false)
}

enum OniAction {
  static let stand = SKAction.setTexture(GameSprite.gameSheet.Enemies_Oni_oni_01())
  static let idle = SKAction.sequence([OniAction.stand, EnemyAction.delay])
  static let delay = SKAction.wait(forDuration: TimeInterval(0.25))
  static let blink = SKAction.animate(with: GameSprite.gameSheet.OniBlink(), timePerFrame: 0.3, resize: true, restore: false)
  static let grunt = SKAction.animate(with: GameSprite.gameSheet.OniBreath(), timePerFrame: 0.5, resize: true, restore: false)
  
  static let breathing = SKAction.sequence([
    OniAction.grunt, OniAction.delay, OniAction.grunt, OniAction.blink, OniAction.grunt
    ])
}


enum ImpAction {
  static let stand = SKAction.setTexture(GameSprite.gameSheet.Enemies_Imp_medimp_01())
  static let idle = SKAction.sequence([ImpAction.stand, EnemyAction.delay])
  static let blink = SKAction.animate(with: GameSprite.gameSheet.ImpBlink(), timePerFrame: 0.3, resize: true, restore: false)
  static let breathing = SKAction.animate(with: GameSprite.gameSheet.ImpBreath(), timePerFrame: 0.5, resize: true, restore: false)
}

enum SkeletonAction {
  static let stand = SKAction.setTexture(GameSprite.gameSheet.Enemies_skeleton_skeleton_01())
  static let idle = SKAction.sequence([SkeletonAction.stand, EnemyAction.delay])
  
  static let look1 = SKAction.animate(with: GameSprite.gameSheet.SkeletonLook1(), timePerFrame: 0.4, resize: true, restore: false)
  static let look2 = SKAction.animate(with: GameSprite.gameSheet.SkeletonLook2(), timePerFrame: 0.4, resize: true, restore: false)
  static let breathing = SKAction.animate(with: GameSprite.gameSheet.SkeletonBreath(), timePerFrame: 0.5, resize: true, restore: false)
}

enum LittleImpAction {
  static let stand = SKAction.setTexture(GameSprite.gameSheet.Enemies_LittleImp_littlimp_01())
  static let idle = SKAction.sequence([LittleImpAction.stand, EnemyAction.delay])
  
  static let breathing = SKAction.animate(with: GameSprite.gameSheet.LittleImpBreath(), timePerFrame: 0.5, resize: true, restore: false)
  static let growl = SKAction.animate(with: GameSprite.gameSheet.LittleImpGrowl(), timePerFrame: 0.16, resize: true, restore: false)
  static let jump = SKAction.animate(with: GameSprite.gameSheet.LittleImpJump(), timePerFrame: 0.16, resize: true, restore: false)
}

enum GoblinAction {
  static let stand = SKAction.setTexture(GameSprite.gameSheet.Enemies_Goblin_goblin_01())
  static let idle = SKAction.sequence([GoblinAction.stand, EnemyAction.delay])
  
  static let headTurn = SKAction.animate(with: GameSprite.gameSheet.GoblinHeadTurn(), timePerFrame: 1.0, resize: true, restore: false)
  static let blink = SKAction.animate(with: GameSprite.gameSheet.GoblinBlink(), timePerFrame: 0.3, resize: true, restore: false)
  static let breathing = SKAction.animate(with: GameSprite.gameSheet.GoblinBreath(), timePerFrame: 0.5, resize: true, restore: false)
  
}


enum AngelAction {
  static let wait = SKAction.wait(forDuration: 0.06)
  static let idel = SKAction.setTexture(GameSprite.gameSheet.AngelMoroni_am_01(), resize: true)
  static let delay = SKAction.wait(forDuration: 0.14)
  static let idelSequence = SKAction.sequence([delay, idel])
  
  // Walk
  static let walk = SKAction.animate(with: GameSprite.gameSheet.angelWalk(), timePerFrame: 0.16, resize: true, restore: false)
  static let repeatWalkForever = SKAction.repeatForever(walk)
  static let walkSequence = SKAction.sequence([delay, repeatWalkForever])
  
  // Takeoff
  static let takeoff = SKAction.animate(with: GameSprite.gameSheet.angelTakeoff(), timePerFrame: 0.06, resize: true, restore: false)
  
  // TouchDown
  static let touchdown = SKAction.animate(with: GameSprite.gameSheet.angelTouchdown(), timePerFrame: 0.06, resize: true, restore: false)
  
  
  // Fly
  static let fly = SKAction.animate(with: GameSprite.gameSheet.angelFly(), timePerFrame: 0.06, resize: true, restore: false)
  static let repeatFlyForever = SKAction.repeatForever(fly)
  static let flySequence = SKAction.sequence([delay, repeatFlyForever])
  
  // Dead
  static let dead = SKAction.setTexture(GameSprite.gameSheet.AngelMoroni_am_17(), resize: true)
  
  // Recovering
  static let recovering = SKAction.animate(with: GameSprite.gameSheet.angelRecovering(), timePerFrame: 0.06, resize: true, restore: false)
  static let repeatRecoveringAction = SKAction.repeat(recovering, count: 3)
  static let halfpoint = SKAction.setTexture(GameSprite.gameSheet.AngelMoroni_am_13())
  
  // Taking Damage
  static let damage = SKAction.animate(with: GameSprite.gameSheet.angelTakeDamage(), timePerFrame: 0.06, resize: true, restore: false)
  
  
}
