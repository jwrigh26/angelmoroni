//
//  ScreenNode.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/10/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


final class ScreenNode: GameNode {
  
  var nodeNumber = 0
  
  func prepForGame(_ id: Int, screenMapper: LevelScreenMapper){
    position = CGPoint(x: screenSize.width, y: 0)
    if let frame = screenMapper.mapScreen(parent: self) {
      calculateNodeBounds(frame)
      nodeNumber = id
    }
  }
  
  override init(){
    super.init()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func calculateNodeBounds(_ frame: CGRect){
    // Get fartheset x position
    let x:CGFloat = frame.origin.x
    let size:CGFloat = frame.width
    let caf = self.calculateAccumulatedFrame()
    //print(caf)
    nodeBounds = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: x + size, height: caf.height)
  }
  
  func evaluteActions(_ playerHitBox: CGRect){
    for child in children {
      hitTest(playerHitBox, child: child)
    }
  }
  
  fileprivate func hitTest(_ playerHitBox: CGRect, child: SKNode){
     if let sprite = child as? SpriteProtocol , sprite is ScriptureBitNode ||  sprite is EnemyPlatformSprite {
        
        let point = convertChildPosition(child)
        
        let box = convertChildBoundingBox(sprite, point: point)
        
        if playerHitBox.intersects(box){
          //print("CGRectIntersects and box \(box)")
          
          if scriptureBitHit(sprite, box: box) { return }
          if enemyHit(sprite, playerHitBox: playerHitBox, box: box) { return }
        }
    }
  }
  
  fileprivate func scriptureBitHit(_ sprite: SpriteProtocol, box: CGRect) -> Bool {
    guard let gameScene = scene as? GameScene, let bit = sprite as? ScriptureBitNode , !bit.hit
      else { return  false}
    
    
    GlobalMainQueue.async{
      //print("Show Particle")
      gameScene.particleLayer.particlePool.showParticle(box)
    }
    return bit.onHit()
    //return false
  }
  
  fileprivate func enemyHit(_ sprite: SpriteProtocol, playerHitBox: CGRect, box: CGRect) -> Bool {
    guard let enemy = sprite as? EnemyPlatformSprite else { return  false}
    return enemy.sweepForHit(playerHitBox, box: box)
  }
  
    
  
  
  override func removeAllChildren() {
    for child in children {
      if let platform = child as? EnemyPlatformSprite, let linkedEnemies = platform.linkedEnemies {
        for enemy in linkedEnemies {
          enemy.cleanup()
        }
        // Return to pool
        //print("Enemy needs to be returned is size \(platform.enemySize) and has enemies #\(platform.linkedEnemies?.count)")
        //screenMapper.returnEnemy(platform)
      }
      
      if let scripture = child as? ScriptureBitNode {
        scripture.cleanup()
      }
    }
    super.removeAllChildren()
  }
  
}
