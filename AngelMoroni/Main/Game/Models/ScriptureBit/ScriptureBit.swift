//
//  ScriptureBit.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 7/12/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

final class ScriptureBit: GameSprite {
  
  required init?(coder aDecoder: NSCoder){
    super.init(coder: aDecoder)
  }
  
  init(scriptureBitType: BitType){
    super.init(texture: scriptureBitType.texture, position: CGPoint.zero)
    anchorPoint = CGPoint(x: 0, y: 0)
    //traceHitBox()
  }
  
}


final class ScriptureBitLabel: BMGSwiftyLabel {
  
  override var font: BMGSwiftyFont! {
    get {
      return GameManager.sharedInstance.scriptureBitFont
    }
    set {
      // do nothing
    }
  }
  
  
  internal init(text: String) {
    super.init()
    self.text = text
  }
  
  internal required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  
}
