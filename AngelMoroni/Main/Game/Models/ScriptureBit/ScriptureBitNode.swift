//
//  ScriptureBit.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 7/12/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

final class ScriptureBitNode: GameNode, Hitable {
  
  var scriptureBitLabel: ScriptureBitLabel!
  
  var text: String?
  var bits:[ScriptureBit] = [] // array of bits that this node is made of
  var hit:Bool = false {
    didSet {
      let key = hit ? Notification.ScriptureBitCollectedKey : "notcolllected"
      if let text = self.text, let parent = self.scene as? GameScene {
        let  userInfo = [key : text]
        
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.ScriptureBitCollected), object: self, userInfo: userInfo)
        
        if hit {
          parent.hudLayer.increaseCurrentBits()
        }
      }
    }
  }
  
  var hitBox: CGRect {
    return frameAdjustment()
  }
  
  let padding = 1.0 * Device.Scale
  
  init(position: CGPoint, text: String){
    super.init()
    dirtyNode = ScriptureBit(scriptureBitType: BitType.middle)
    spawnBit(text)
    self.position = position
    self.zPosition = ZPos.Scripture
    //traceHitBox()
   
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  deinit {
    //print("ScriptureBitNode was deinit")
  }
  
  func calculateWidth(_ width: CGFloat) -> Int {
    let block = dirtyNode.size.width
    if width < block {
      return 2
    }
    
    let blocks = Int(ceil(width/block))
    return blocks
  }
  
  func spawnBit(_ text: String){
    
    // PreSetup
    self.text = text
    scriptureBitLabel = ScriptureBitLabel(text: text.uppercased())
    let bitNumber = calculateWidth(scriptureBitLabel.boundingBox.width)
    
    // 1# Create start
    let start = ScriptureBit(scriptureBitType: BitType.start)
    start.position = CGPoint(x: 1 * Device.Scale, y: 0)
    addChild(start)
    bits.append(start)
    
    // 2# Create middle
    var offset: CGFloat = 0
    var xPos: CGFloat = 0
    var width: CGFloat = 0
    for i in 0..<bitNumber {
      let bit = ScriptureBit(scriptureBitType: BitType.middle)
      offset = (1 * Device.Scale) * CGFloat(i)
      xPos = ( bit.size.width * CGFloat(i) ) + start.size.width - offset
      // setting for end
      width = bit.size.width
      bit.position = CGPoint(x: ceil(xPos), y: 0)
      addChild(bit)
      bits.append(bit)
    }
    
    // 3# Create end
    let end = ScriptureBit(scriptureBitType: BitType.end)
    offset = (1 * Device.Scale) * CGFloat(bitNumber)
    xPos = ( width * CGFloat(bitNumber) ) + start.size.width - offset
    end.position = CGPoint(x: ceil(xPos), y: 0)
    addChild(end)
    bits.append(end)
    
    // 4# calculate self frame for node
    nodeBounds = self.calculateAccumulatedFrame()
    
    // 5# add scripture
    //let pos = CGPoint(x: nodeBounds.width/2 + (padding*2), y: nodeBounds.height/2 - (padding/2))
    let pos = CGPoint(x: nodeBounds.width/2 + (padding*2), y: nodeBounds.height/2)
    scriptureBitLabel.position = pos
    scriptureBitLabel.zPosition = 1
    addChild(scriptureBitLabel)
  }
  
  
  override func cleanup(){
    bits.forEach{ $0.removeFromParent() }
    bits.removeAll(keepingCapacity: false)
    NSObject.cancelPreviousPerformRequests(withTarget: self)
    removeAllActions()
    removeAllChildren()
    removeFromParent()
  }
    
  func frameAdjustment()-> CGRect {
    
    let xPos = nodeBounds.width/2 + padding
    let yPos = dirtyNode.size.height/2
    return CGRect(x: xPos, y: yPos, width: nodeBounds.width - padding*4, height: nodeBounds.height - padding*12)
  }
  
  override func boundingBox()-> CGRect {
    let box = frameAdjustment()
    let xPos = self.frame.origin.x + padding
    let yPos = self.frame.origin.y //+ box.origin.y/2
    return CGRect(x: xPos, y: yPos, width: box.width, height: box.height)
  }
  
  override func traceHitBox() {
    let box = frameAdjustment()
    let size = CGSize(width: box.width, height: box.height)
    let rect = SKShapeNode(rectOf: size)
    rect.position = CGPoint(x: box.origin.x, y: box.origin.y)
    rect.zPosition = ZPos.HitBox
    rect.fillColor = SKColor.green
    rect.strokeColor = SKColor.green
    rect.alpha = 0.8
    self.addChild(rect)
  }
  
  func onHit()-> Bool{
    if hit { return false }
    hit = true;
    isHidden = true
//    cleanup()
    return true
  }
}
