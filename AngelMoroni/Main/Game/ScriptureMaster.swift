//
//  ScriptureMaster.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/31/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift

class ScriptureMaster: NotificationListener {
  
  fileprivate var level: Level!
  fileprivate var assembledString: String?
  fileprivate(set) var scriptureArray:[String]!
  fileprivate(set) var bits:[String]!
  fileprivate let brake = "<br/>"
  var notification: NSNotificationProxy?
  var totalBits: Int = 0 // Total count minus brakes in scripture
  var collectedBits: Int = 0
  var scripture: String!
  var scriptureTitle: String!
  var returningBit: Bool = false
  
  func resetValues(){
    bits.removeAll(keepingCapacity: false)
    scriptureArray.removeAll(keepingCapacity: false)
    scripture = nil
    scriptureTitle = nil
    notification?.stop()
  }
  
  init(level: Level){
    self.level = level
    listenForNotifications()
  }
  
  deinit {
//    notification?.stop()
  }
  
  func listenForNotifications(){
    guard notification == nil else { return }
    notification = NSNotificationProxy(notificationKey: Notification.ScriptureBitCollected){
      [weak self] sender in
      guard let info = (sender as NSNotification).userInfo as? [String : String],
        let bit = info[Notification.ScriptureBitCollectedKey] else {
          print("No bit is returned but let's check if it's end of game")
          // TODO: Check to see if it should be end of game
          return
      }
      
      self?.returnBit(bit)
    }
  }
  
  func compareAndModifyBits(_ index:Int, bit: String, returnedBit: String)-> Bool{
    
    if bit == returnedBit {
      scriptureArray[index] = "%\(bit)%"
      collectedBits += 1
      collectedBits = min(collectedBits, totalBits)
      return true
    }
    return false
  }
  
  func showCompletedScripture(){
    let joinedString = scriptureArray.joined(separator: " ")
    print(joinedString)
  }
  
  var completedScripture: String {
    return scriptureArray.joined(separator: " ")
  }
  
  var hasBits: Bool {
    return bits.count > 0
  }
  
  // Get the next bit to show on the screen
  var nextBit:String? {
    
    get {
      guard !returningBit else {
        print("Returning nil because in the middle of returning Bit")
        return nil }
      var result: String?
      
      if bits.count > 0{
        if !bits.isEmpty{
          result = bits.remove(at: 0)
          // Check if it's a br
          if result == brake {
            result = nil
          }
        }
      }
      return result
    }
  }
  
  func returnBit(_ returnedBit: String){
    returningBit = true
    defer { returningBit = false }
    for (index, bit) in scriptureArray.enumerated() {
      
      // need to escape any parenethesis first
      let word = bit.replacingOccurrences(of: "(", with: "\\(").replacingOccurrences(of: ")", with: "\\)")
      
      // Now check if word contains "%"
      // If it does not then we can compare it with the returned bit
      if let regExp = RegExp(pattern: "^\\%([^\\%]*)\\%$", options: .caseInsensitive) , !regExp.isMatching(word){
        
        if compareAndModifyBits(index, bit:bit, returnedBit: returnedBit) {
          break
        }
      }
      
    } // close for loop
    
    //if bits.isEmpty {
      //print("Bits is empty")
      //showCompletedScripture()
      // TODO: Notify to end game
    //}
  }
  
  func makeScriptureBits(callback: BasicHandler){
    // Level should always start at zero so subract
    do {
      try parseScripture()
      callback()
    }catch{
      fatalError("Error parsing scripture for ScriptureMaster. \(error)")
    }
  }
  
  func parseScripture() throws{
    
    // Get the scriptureBit from the level
    guard let scriptureBitString = level.scripture?.bitText,
      let scripture = level.scripture?.text
      else { throw DataError.parse }
    
    self.scriptureTitle = level.title
    self.scripture = scripture
    
    bits = scriptureBitString.components(separatedBy: "%")
    scriptureArray = bits
    
    // filter out </br> for total bits
    let array = bits.filter( { [weak self] in $0 != self?.brake ?? "" })
    totalBits = array.count
    
    //print("Scripture Title \(scriptureTitle)")
    //print("Scripture \(scripture)")
    //print("total bits \(totalBits) and array count \(bits.count)")
  }
  
  
  /*
  func parseJSON(){
    guard let path = Bundle.main.path(forResource: file.rawValue, ofType: "json") else {
      throw ParseError.general(message: "Unable to find path to json file.")
    }
    
    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: NSData.ReadingOptions.mappedIfSafe)
    let json = JSON(data: data, options: JSONSerialization.ReadingOptions.allowFragments)
    
    // Precondition if level is more than the count
    let count = json["scriptures"].count
    precondition( level >= 0 && level < count, "Index: \(level) is out of bounds! and count is \(count)")
    
    // Get the scriptureBit from the level
    let scriptureBitString = json["scriptures"][level]["bits"].stringValue
    
    scripture = json["scriptures"][level]["scripture"].stringValue
    scriptureTitle = json["scriptures"][level]["title"].stringValue
    
    //scriptureArray = scripture.componentsSeparatedByString(" ")
    bits = scriptureBitString.components(separatedBy: "%")
    scriptureArray = bits
    
    // filter out </br> for total bits
    let array = bits.filter( { [weak self] in $0 != self?.brake ?? "" })
    totalBits = array.count
    
    //totalBits = bits.count
    
    print("total bits \(totalBits) and array count \(bits.count)")
  }*/
  
}



