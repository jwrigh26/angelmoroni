//
//  SegueScene.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/27/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

class SegueScene: SKScene, SceneProtocol {
  
  override init(size: CGSize) {
    super.init(size: size)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func didMove(to view: SKView){
    self.isUserInteractionEnabled = false // allows for touch
    backgroundColor = UIColor(netHex: GameColor.black.rawValue)
    segueToGame()
  }
  
  fileprivate func segueToGame(){
    NavigationManager.sharedInstance.transitionByGameState(.startingGame)
  }
  
}
