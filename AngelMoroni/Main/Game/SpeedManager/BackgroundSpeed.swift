//
//  CloudSpeed.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/13/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

enum BackgroundSpeedCap: CGFloat, SpeedCap {
  case stop = 0.0
  case menu = 5.0
  case min = 2.75
  case slow = 2.5
  case slowMed =  2.0
  case med = 1.75
  case fast = 1.5
  case max = 1.0
  
  func getValue()-> CGFloat {
    return self.rawValue
  }
  
  mutating func reset(){
    self = .min
  }
  
  var isMaxedOut: Bool {
    return self == .max
  }
  
  mutating func increase(){
    switch self {
    case .min:
      self = .slow
    case .slow:
      self = .slowMed
    case .slowMed:
      self = .med
    case .med:
      self = .fast
    case .fast:
      self = .max
    case .max:
      self = .max
    case .stop, .menu:
      break
    }
  }
}


final class BackgroundSpeed: SpeedManager {
  
  var gameSpeedRate: CGFloat = 0
  var lastUpdateTime: CFTimeInterval = 0
  var speedCap: SpeedCap
  var adjustingSpeed: CGFloat = 0
  var speedLimit: CGFloat = 0
  
  
  
  init(){
    speedCap = BackgroundSpeedCap.min
  }

  var timeJump: CFTimeInterval = 1 // The time at which the speed cap will increase
  var timeAdjusting: Bool = false {
    willSet {
      if timeAdjusting == false && newValue == true {
        adjustingSpeed = currentGameSpeed
      }
    }
  }
  
  func update(_ dt: CFTimeInterval) {
    updateWorker(dt)
  }
  
  func resetSpeedCapforMenu(){
    speedCap = BackgroundSpeedCap.menu
  }

  
}
