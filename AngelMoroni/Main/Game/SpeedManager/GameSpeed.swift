//
//  GameSpeed.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/13/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

enum GameSpeedCap: CGFloat, SpeedCap {
  static let device = Device()
  case stop = 0.0
  case min = 0.55 //0.4
  case slow = 0.65 //0.5
  case med = 0.7 //0.6
  case max = 0.8 //0.7
  case maxExceeded = 0.9
  
  
  func getValue()-> CGFloat {
    if GameSpeedCap.device.isSmallScreen {
      return self.rawValue + 0.1
    }
    
    if GameSpeedCap.device.isXLargeScreen {
      return self.rawValue - 0.1
    }
    
    return self.rawValue
  }
  
  mutating func reset(){
    self = .min
  }
  
  var isMaxedOut: Bool {
    return self == .maxExceeded
  }
  
  mutating func increase(){
    switch self {
    case .min:
      self = .slow
    case .slow:
      self = .med
    case .med:
      self = .max
    case .max:
      self = .maxExceeded
    case .maxExceeded:
      self = .maxExceeded
    case .stop:
      break
    }
    //print("Increase Speed \(self) and speed = \(GameManager.sharedInstance.gameSpeed.currentGameSpeed)")
  }
}



final class GameSpeed: SpeedManager {
  
  let device = Device()
  var gameSpeedRate: CGFloat = 0
  var lastUpdateTime: CFTimeInterval = 0
  var speedCap: SpeedCap
  var adjustingSpeed: CGFloat = 0
  var speedLimit: CGFloat = 0
  
  func computeRawSpeed() -> CGFloat {
    let speedCap = self.speedCap.getValue()
    //let speed = (screenSize.height) * speedCap // This is what is different
    let speed = estimatedSpeed * speedCap
    
    return speed
  }
  
//  var countSpeed:Int = 0
  var currentGameSpeed: CGFloat {
    
    
    let speed = computeRawSpeed()
    
    
    
    if timeAdjusting && adjustingSpeed < min(speed, maxGameSpeed) {
      adjustingSpeed += gameSpeedRate * 10
      
      return adjustingSpeed
    }else{
      timeAdjusting = false
      adjustingSpeed = 0
    }
    
    // This is in place because there is a rounding issue
    if speed >= maxGameSpeed {
      speedLimit = speed
      //print("Returning speedLimit \(speedLimit)")
      return speedLimit
    }
    
//    if countSpeed % 3 == 0 {
//      print(speed)
//    }
    
//    countSpeed += 1
    
    return speed
  }
  
  var timeJump: CFTimeInterval = 1 // The time at which the speed cap will increase
  var timeAdjusting: Bool = false {
    willSet {
      if timeAdjusting == false && newValue == true {
        adjustingSpeed = currentGameSpeed
      }
    }
  }
  
  init(){
    speedCap = GameSpeedCap.min
  }
  
  func playerHit(){
    speedCap = GameSpeedCap.stop
    resetValues()
    
  }
  

  func update(_ dt: CFTimeInterval) {
    updateWorker(dt)
  }
  
  
}
