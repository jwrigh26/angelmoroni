//
//  SpeedManager.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/13/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit



protocol SpeedCap {
  var isMaxedOut: Bool { get }
  func getValue()-> CGFloat
  mutating func reset()
  mutating func increase()
}



protocol SpeedManager:class {
  
  var estimatedSpeed: CGFloat { get }
  var gameSpeedRate: CGFloat { get set }
  var speedCap: SpeedCap { get set }
  var lastUpdateTime: CFTimeInterval { get set }
  var adjustingSpeed: CGFloat { get set }
  var maxGameSpeed: CGFloat { get }
  var speedLimit: CGFloat { get set }// This is used once maxGameSpeed is reached
  var currentGameSpeed: CGFloat { get }
  var timeJump: CFTimeInterval { get set }
  var timeAdjusting: Bool { get set }
  
  func resetValues()
  func resetGameSpeed()
  func reClockTimeJump()
  func updateWorker(_ dt: CFTimeInterval)
  func update(_ dt: CFTimeInterval)
  func computeRawSpeed() -> CGFloat
  
}


extension SpeedManager {
  
  var maxGameSpeed: CGFloat {
    let max:CGFloat = Device.isPhone ? 480 : 512
    //return max
    //return min(screenSize.width/2, max)
    return min(estimatedSpeed, max)
  } // no reason but 512 seemed most enjoyable for large screens

  
  var startTimeJump: CFTimeInterval {
    return 1
  }
  
  var estimatedSpeed: CGFloat {
    return Device.isPhone ? screenSize.width * 0.64 : screenSize.width * 0.7
  }
  
  func computeRawSpeed() -> CGFloat {
    let speedCap = self.speedCap.getValue()
    let speed = maxGameSpeed / speedCap
    return speed
  }
  
  var currentGameSpeed: CGFloat {
    
    
    let speed = computeRawSpeed()
    
    
    
    if timeAdjusting && adjustingSpeed < min(speed, maxGameSpeed) {
      adjustingSpeed += gameSpeedRate * 10
      
      return adjustingSpeed
    }else{
      timeAdjusting = false
      adjustingSpeed = 0
    }
    
    // This is in place because there is a rounding issue
    if speed >= maxGameSpeed {
      speedLimit = speed
      //print("Returning speedLimit \(speedLimit)")
      return speedLimit
    }
    
    return speed
  }
  
  func resetValues(){
    gameSpeedRate = 0
    lastUpdateTime = 0
    adjustingSpeed = 0
    timeJump = 1
    timeAdjusting = false
  }
  
  func resetGameSpeed(){
    speedCap.reset()
    resetValues()
  }
  
  func reClockTimeJump(){
    timeJump += timeJump
    timeJump = min(timeJump, 2)
  }
  
  func updateWorker(_ dt: CFTimeInterval){
    guard !speedCap.isMaxedOut else { return }
    
    gameSpeedRate = CGFloat (round(10000*dt)/10000)
    
    if lastUpdateTime >= timeJump && !timeAdjusting {
      reClockTimeJump()
      timeAdjusting = true
      speedCap.increase()
      lastUpdateTime = 0
      
    }else if !timeAdjusting  && !speedCap.isMaxedOut {
      lastUpdateTime += dt
      //print("lastUpdateTime \(lastUpdateTime) and dt \(dt)")
    }
  }
  
}
