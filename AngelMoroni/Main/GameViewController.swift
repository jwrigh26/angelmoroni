//
//  GameViewController.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import UIKit
import SpriteKit
import RealmSwift


final class GameViewController: MenuViewController {
  
  var token: NotificationToken?
  var snapShot: UIView?
  var blackCurtain: BlackCurtain?
  var screenCoordinator: LevelScreenCoordinator!
  var playerFrame: CGRect {
    let scale = Device.Scale
    return CGRect(x: 0, y: 0, width: 56 * scale, height: 32 * scale)
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NavigationManager.sharedInstance.transitionByGameState(.start)
    view.alpha = 0.0
    UIView.animate(withDuration: 0.25){ [weak self] in
      self?.view.alpha = 1.0
    }
    
    listenForChanges()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  
  
  
  // This is where everything gets started.
  // AppDelegate inits CoreData, Base class gets notified, calls fetchData
  // We then assign Menu data, create the scene, and go to World menu
  
  
  
  
  func loadData() {
    preloadResources{ [weak self] in
      // NavigationManager.sharedInstance.transitionByGameState(.worldNoAnimate)
      // NavigationManager.sharedInstance.transitionByGameState(.LevelSummary)
      GlobalBackgroundQueue.async { [weak self] in
        //print("Sleeping")
        Thread.sleep(forTimeInterval: 1.0)
        //print("Loading Data")
        MenuGameDataFeed.sharedInstance.preloadData()
        //MenuGameDataFeed.sharedInstance.debug()
        
        GlobalMainQueue.async {
          self?.readyToPlay()
        }
      }
    }
  }
  
  func listenForChanges(){
    let realm = Realm.mainInstance()
    token = realm.addNotificationBlock{ _, _ in
    GlobalBackgroundQueue.async {
        MenuGameDataFeed.sharedInstance.preloadData()
      }
    }
  }
  
  override func preloadResources(_ handler: @escaping CallBackHandler) {
    SKTextureAtlas.preloadTextureAtlases([
      GameManager.sharedInstance.bigTextAtlas,
      GameManager.sharedInstance.gameAtlas,
      GameManager.sharedInstance.scriptureBitAtlas,
      GameManager.sharedInstance.hudWhiteAtlas,
      GameManager.sharedInstance.hudYellowAtlas]){
        GlobalMainQueue.async{
          handler()
        }
    }
  }
  
  
  func animateBlackCurtain(){
    addBlackCurtain()
    delay(1.0){ [weak self] in
      self?.removeBlackCurtain()
    }
  }
  
  override func transitionByState(_ state: GameState) {
    
    removeSnapShot() // don't want any snapshots hanging around if we go to menu or retry, etc
    
    switch state {
    case .start:
      transitionToStart()
      
    case .worldNoAnimate:
      transitionMenu(worldMenu, animate: false)
      
    case .world:
      transitionMenu(worldMenu)
      
    case .level:
      transitionMenu(levelMenu)
      
    case .startingGame:
      startingGame()
      
    case .playing:
      break
      
    case .paused:
      pauseGame()
      
    case .resume:
      resumeGame()
      
    case .retry, .nextLevel:
      retryCurrentLevel()
      
    case .returnToMenu:
      returnToMenu()
      
    case .gameOver:
      gameOver()
      
      
    case .levelSummary:
      transitionToLevelSummary()
      
    default:
      break
    }
  }
  
  override func gameActive() {
    
    if GameManager.sharedInstance.gameState != .paused {
      delay(0.01){ [weak self] in
        self?.removeSnapShot()
        self?.skView.scene?.isPaused = false
      }
    }else{
      skView.scene?.isPaused = false
    }
  }
  
  override func gameNotActive() {
    
    // This sets the scene to pause
    // It notifies the Angel to reset actions
    // It notifies sprites as well
    // It resets dt times for updates etc
    GameManager.sharedInstance.pauseGameScene()
    
    // If we are playing the game, then show the modal and set to pause
    // else just do a background so we avoid on resume glitches
    if GameManager.sharedInstance.gameState == .playing  {
      NavigationManager.sharedInstance.transitionByGameState(.paused)
      gameState = .paused
    }else if GameManager.sharedInstance.gameState != .paused {
      //print("Not in game mode so just add Snapshot \(gameState)")
      addSnapShot()
    }
    
  }
  
  fileprivate func startingGame(){
    guard let level = NavigationManager.sharedInstance.currentLevelPath?.level else { fatalError("No Level available to start the game.")}
    
    prefetchModalData()
    
    GameManager.sharedInstance.createScriptureMaster(with: level){ [weak self] _ in
      GlobalMainQueue.async{
        self?.transitionToGame{
          self?.startingGameWorker()
        }
      }
    }
    showGameStartScreen()
  }
  
  fileprivate func startingGameWorker(){
    
    let factory: SpriteFactory = World1SpriteFactory()
    screenCoordinator = LevelScreenCoordinator.sharedInstance
    screenCoordinator.preload(factory, playerFrame: playerFrame){ [weak self] in
      
      GlobalMainQueue.async{ [weak self] in
        
        delay(1.0) {
          self?.hideGameStartScreen()
          (self?.skView.scene as? GameScene)?.levelStart()
          self?.addPauseButton()
        }
        
      }
    }
  }
  
  fileprivate func pauseGame(){
    prePauseGame()
    showPauseScreen()
  }
  
  fileprivate func gameOver(){
    prePauseGame()
    showGameOverScreen()
  }
  
  fileprivate func prePauseGame(){
    addSnapShot()
    GameManager.sharedInstance.pauseGameScene()
  }
  
  fileprivate func resumeGame(){
    hidePauseScreen()
    gameState = .playing
    GameManager.sharedInstance.resumeGameScene()
  }
  
  func addSnapShot(){
    if snapShot != nil { removeSnapShot() }
    snapShot = self.skView.snapShotAfterScreenUpdates()
    self.view.addSubview(snapShot!)
  }
  
  func removeSnapShot(){
    snapShot?.removeFromSuperview()
    snapShot = nil
  }
  
  fileprivate func addBlackCurtain(){
    if blackCurtain != nil { removeBlackCurtain() }
    blackCurtain = BlackCurtain(layer: self.view.layer, opacity: 1.0)
    self.view.layer.addSublayer(blackCurtain!)
  }
  
  fileprivate func removeBlackCurtain(){
    blackCurtain?.removeFromSuperlayer()
    blackCurtain = nil
  }
  
  deinit {
    token?.stop()
  }
  
}
