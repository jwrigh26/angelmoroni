//
//  LevelSummaryScene.swift
//  AngelMoroni
//
//  Created by Justin Wright on 5/15/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


class LevelSummaryScene: SKScene, SceneProtocol, SummaryViewDelegate {
  
  
  var scrollPercentage: CGFloat = 0
  
  var levelSummary: HUDLevelSummary? {
    return GameManager.sharedInstance.levelSummary
  }
  
  var scriptureMaster: ScriptureMaster? {
    return GameManager.sharedInstance.scriptureMaster
  }
  var splashLayer: SummarySplashLayer!
  var hudHeight: CGFloat = 0
  
  
  deinit {}
  
  override init(size: CGSize) {
    super.init(size: size)
    
    
    // Print out what we have
    // Total Bits
    // Collected Bits
    // Harts
//    let totalBits = scriptureMaster?.totalBits
//    let collectedBits = scriptureMaster?.collectedBits
    
    
    //print("Total Bits \(totalBits)")
    //print("CollectedBits Bits \(collectedBits)")
    //print("Scripture")
//    if let scripture = scriptureMaster?.completedScripture {
//      print(scripture)
//    }
    
    
//    if let summary = levelSummary {
//      print("Total harts \(summary.hartsMax) and harts \(summary.harts)")
//      print("Bits collected \(summary.bits)")
//    }
    
    
    if splashLayer != nil {
      //print("Splash Layer not nil so must remove")
      splashLayer.removeFromParent()
      splashLayer = nil
    }
    
    splashLayer = SummarySplashLayer()
    addChild(splashLayer)
    splashLayer.animateBitLabels()
    splashLayer.animateDegrees()
    hudHeight = splashLayer.hudHeight
    
  }
  
  
  
  override func update(_ currentTime: TimeInterval) {}
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func didMove(to view: SKView){
    backgroundColor = UIColor(netHex: GameColor.black.rawValue)
    
  }
  
  func syncWithScroll(_ amount: CGFloat){
    splashLayer.alpha = min(1, (1 - scrollPercentage*2))
    splashLayer.position = CGPoint(x: splashLayer.position.x, y: amount)
  }
  
  func didScroll(_ scrollView: UIScrollView){
    
    let top = scrollView.contentInset.top
    let height = max(0, (scrollView.contentOffset.y + top))
    scrollPercentage = min(1, height / top)
    syncWithScroll(min(height, top))
  }
  
  
  
  
//  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//    print("touch began")
  
  
    //    if splashLayer != nil {
    //      splashLayer.removeFromParent()
    //      splashLayer = nil
    //    }
    //
    //    splashLayer = SummarySplashLayer()
    //    addChild(splashLayer)
    //    splashLayer.animateBitLabels()
    //    splashLayer.animateDegrees()
//  }
  
}
