//
//  SummaryBadge.swift
//  AngelMoroni
//
//  Created by Justin Wright on 6/22/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


protocol SummaryBadge {
  var image: SummaryBadgeSprite { get }
  var shadow: SummaryBadgeSprite { get }
  var sequenceNumber: Int { get }
  
  init(size: SummaryBadgeSprite.BadgeSize)
}

  
  
//func ==<T:SummaryBadge>(lhs: T, rhs: T) -> Bool {
//  return lhs.sequenceNumber == rhs.sequenceNumber
//}
//
//func < <T: SummaryBadge>(lhs: T, rhs: T) -> Bool {
//  return lhs.sequenceNumber < rhs.sequenceNumber
//}

struct SunSummaryBadge: SummaryBadge {
  let image: SummaryBadgeSprite
  let shadow: SummaryBadgeSprite
  let sequenceNumber = 3
  
  init(size: SummaryBadgeSprite.BadgeSize){
    image = SummaryBadgeSprite(texture: GameManager.sharedInstance.gameSheet.Summary_sun(), position: CGPoint.zero, size: size)
    shadow = SummaryBadgeSprite(texture: GameManager.sharedInstance.gameSheet.Summary_sun_black(), position: CGPoint.zero, size: size)
  }
}

struct MoonSummaryBadge: SummaryBadge {
  let image: SummaryBadgeSprite
  let shadow: SummaryBadgeSprite
  let sequenceNumber = 2
  
  init(size: SummaryBadgeSprite.BadgeSize){
    image = SummaryBadgeSprite(texture: GameManager.sharedInstance.gameSheet.Summary_moon(), position: CGPoint.zero, size: size)
    shadow = SummaryBadgeSprite(texture: GameManager.sharedInstance.gameSheet.Summary_moon_black(), position: CGPoint.zero, size: size)
  }
  
}

struct StarSummaryBadge: SummaryBadge {
  let image: SummaryBadgeSprite
  let shadow: SummaryBadgeSprite
  let sequenceNumber = 1
  
  init(size: SummaryBadgeSprite.BadgeSize){
    image = SummaryBadgeSprite(texture: GameManager.sharedInstance.gameSheet.Summary_star(), position: CGPoint.zero, size: size)
    shadow = SummaryBadgeSprite(texture: GameManager.sharedInstance.gameSheet.Summary_star_black(), position: CGPoint.zero, size: size)
  }
  
}

class SummaryBadgeSprite: GameSprite {
  enum BadgeSize {
    
    static let SmallScale: CGFloat = Device.Scale
    static let LargeScale:CGFloat = Device.isPhone ? 4.0 : 4.0
    
    case small, large
    
    var scale: CGFloat {
      switch self {
      case .small:
        return BadgeSize.SmallScale
      case .large:
        return BadgeSize.LargeScale
      }
    }
    
  }
  
  convenience init(texture: SKTexture, position: CGPoint, size: SummaryBadgeSprite.BadgeSize){
    self.init(texture: texture, position: position)
    self.setScale(size.scale)
  }
  
  override init(texture: SKTexture, position: CGPoint){
    texture.filteringMode = SKTextureFilteringMode.nearest
    super.init(texture: texture, color: SKColor.white, size: texture.size())
    self.position = position
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}
