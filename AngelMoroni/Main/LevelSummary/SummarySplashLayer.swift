//
//  SummarySplashLayer.swift
//  AngelMoroni
//
//  Created by Justin Wright on 6/22/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

final class SummarySplashLayer: SKNode {
  
  enum Badge: String { case Sun, Star, Moon }
  enum Alignment { case left, right, center }
  
  var numberOfObtainedDegrees = GameManager.sharedInstance.levelSummary?.degreeOfGlory.rawValue ?? 0
  
  let centerSummaryBadges: [Badge : SummaryBadge] = [
    Badge.Sun : SunSummaryBadge(size: .large),
    Badge.Moon : MoonSummaryBadge(size: .large),
    Badge.Star : StarSummaryBadge(size: .large)
  ]
  
  let leftSummaryBadges: [Badge : SummaryBadge] = [
    Badge.Sun : SunSummaryBadge(size: .small),
    Badge.Moon : MoonSummaryBadge(size: .small),
    Badge.Star : StarSummaryBadge(size: .small)
  ]
  
  let yellowLblNode = GameLabelNode(fntColor: UIColor(netHex: GameColor.lightYellow.hex))
  let whiteLblNode = GameLabelNode(fntColor: UIColor(netHex: GameColor.cloudWhite.hex))
  let grayLblNode = GameLabelNode(fntColor: UIColor(netHex: GameColor.lightGray.hex))
  
  let scaleDown = SKAction.scale(to: 0.0, duration: 0)
  let scaleTo = SKAction.scale(to: SummaryBadgeSprite.BadgeSize.LargeScale, duration: 0.25)
  
  var summaryBadgeLabel: SKLabelNode!
  var bitsLabel: SKLabelNode!
  var centerBadgeContainer = SKNode()
  var centerBitsContainer = SKNode()
  var rightBadgeContainer = SKNode()
  var leftBitsContainer = SKNode()
  var totalBits: Int = GameManager.sharedInstance.scriptureMaster?.totalBits ?? 00
  var currentBits: Int = 0
  var collectedBits: Int = GameManager.sharedInstance.scriptureMaster?.collectedBits ?? 00
  var animating: Bool = false
  
  var yPosForBitsContainer: CGFloat = 0
  
  var hudHeight: CGFloat {
    let l = leftSummaryBadges[.Sun]?.image.frame.height ?? 60 // quick dirty for height ref
    return l + 8
  }
  
  
  
  deinit {}
  
  override init() {
    super.init()
    
    currentBits = 0
    
    createSummaryBadgeTitle()
    addCenterBadge(.Sun, alignment: .right)
    addCenterBadge(.Star, alignment: .left)
    addCenterBadge(.Moon, alignment: .center)
    addChild(centerBadgeContainer)
    
    setupRightBadges()
    addChild(rightBadgeContainer)
    
    createCenterBitHeader()
    createLeftBitHeader()
  }
  
  required init?(coder aDecoder: NSCoder){
    super.init(coder: aDecoder)
    
  }
  
  func createCenterBitHeader(){
    let t = makeBitHeader(16, xOffset: 16, container: centerBitsContainer, bits: currentBits)
    bitsLabel = t.labels[1]
    centerBitsContainer.position = CGPoint(x: screenBounds.midX - t.0, y: yPosForBitsContainer)
    addChild(centerBitsContainer)
    centerBitsContainer.zPosition = 99
  }
  
  func createLeftBitHeader(){
    
    let t = makeBitHeader(12, xOffset: 8, container: leftBitsContainer, bits: collectedBits)
    let lbl = t.labels[0]
    let x:CGFloat = lbl.frame.width/2 + 40
    let y = screenSize.height - hudHeight
    
    leftBitsContainer.position = CGPoint(x: x, y: y)
    addChild(leftBitsContainer)
    leftBitsContainer.zPosition = 99
    leftBitsContainer.isHidden = true
  }
  
  func makeBitHeader(_ size: CGFloat, xOffset: CGFloat, container: SKNode, bits: Int) -> (offset: CGFloat, labels: [SKLabelNode]){
    let lbl1 = yellowLblNode.createLabel(CommonStrings.scriptureBits.uppercased(), size: size)
    let lbl2 = whiteLblNode.createLabel(bitsToString(bits), size: size)
    let lbl3 = yellowLblNode.createLabel("/", size: size)
    let lbl4 = yellowLblNode.createLabel(bitsToString(totalBits), size: size)
    
    container.addChild(lbl1)
    container.addChild(lbl2)
    container.addChild(lbl3)
    container.addChild(lbl4)
    
    let spacer: CGFloat = Device.isPhone ? 8 : 16
    var xpos = lbl1.frame.width/2 + lbl2.frame.width / 2 + xOffset
    lbl2.position = CGPoint(x: xpos + spacer, y: lbl1.position.y - 1)
    
    xpos = lbl2.position.x + lbl2.frame.width/2 + lbl3.frame.width / 2
    lbl3.position = CGPoint(x: xpos, y: lbl1.position.y - 1)
    
    xpos = lbl3.position.x + lbl3.frame.width/2 + lbl4.frame.width / 2
    lbl4.position = CGPoint(x: xpos, y: lbl1.position.y - 1)
    
    let offset = (lbl2.frame.width / 2) + (lbl4.frame.width / 2) + (lbl3.frame.width / 2) + 8
    return (offset, [lbl1,lbl2, lbl3, lbl4])
  }
  
  func addCenterBadge(_ badge: Badge, alignment: Alignment){
    guard let image = centerSummaryBadges[badge]?.image,
      let shadow = centerSummaryBadges[badge]?.shadow else { return }
    
    let space1: CGFloat = Device.isPhone ? 24 : 48
    let y = summaryBadgeLabel.frame.minY - (image.frame.height/2 + space1)
    let xOffset = image.frame.width + image.frame.width/2
    
    let x: CGFloat = {
      switch alignment {
      case .left:
        return screenBounds.midX - xOffset
      case .right:
        return screenBounds.midX + xOffset
      case .center:
        return screenBounds.midX
      }
    }()
    
    image.isHidden = true
    image.position = CGPoint(x: x, y: y)
    image.run(scaleDown)
    
    shadow.position = image.position
    centerBadgeContainer.addChild(shadow)
    centerBadgeContainer.addChild(image)
    
    let space2: CGFloat = Device.isPhone ? 0 : 32
    yPosForBitsContainer = image.frame.origin.y - image.frame.height / 2 - space2
  }
  
  func setupRightBadges(){
    let array = makeBadgeArray(leftSummaryBadges)
    let layoutOrder = [Alignment.left, Alignment.center, Alignment.right]
    var n = numberOfObtainedDegrees
    for i in 0..<array.count {
      let hidden = n > 0 ? false : true
      addLeftBadge(array[i], alignment: layoutOrder[i], hidden: hidden)
      n -= 1
    }
    rightBadgeContainer.isHidden = true
  }
  
  func addLeftBadge(_ badge: SummaryBadge, alignment: Alignment, hidden: Bool){
    
    let image = badge.image
    let shadow = badge.shadow
    
    let y = screenSize.height - hudHeight
    let xOffset = image.frame.width + image.frame.width/2
    
    let x: CGFloat = {
      switch alignment {
      case .left:
        return screenBounds.maxX - (xOffset + 16 + (image.frame.width * 2))
      case .right:
        return screenBounds.maxX - xOffset
      case .center:
        return screenBounds.maxX - (xOffset + 8 + image.frame.width)
      }
    }()
    
    image.isHidden = hidden
    image.position = CGPoint(x: x, y: y)
    shadow.position = image.position
    rightBadgeContainer.addChild(shadow)
    rightBadgeContainer.addChild(image)
  }
  
  
  func createSummaryBadgeTitle(){
    let size: CGFloat = 14
    let y = screenSize.height * 0.7
    summaryBadgeLabel = grayLblNode.createLabel(CommonStrings.DegreesGlory, size: size)
    summaryBadgeLabel.position = CGPoint(x: screenBounds.midX, y: y)
    addChild(summaryBadgeLabel)
  }
  
  // MARK: Animations
  
  func animateBitLabels(){
    
    let wait = SKAction.wait(forDuration: 0.5)
    let fadeout = SKAction.fadeOut(withDuration: 0.25)
    let remove = SKAction.removeFromParent()
    let block1 = SKAction.run{ [weak self] in
      self?.summaryBadgeLabel.run(fadeout)
      self?.centerBadgeContainer.run(fadeout)
      self?.centerBitsContainer.run(fadeout)
      self?.notifyAnimationComplete()
    }
    let block2 = SKAction.run{ [weak self] in
      self?.fadeInTopHUD()
      self?.summaryBadgeLabel.run(remove)
      self?.centerBadgeContainer.run(remove)
      self?.centerBitsContainer.run(remove)
    }
    
    let seq = SKAction.sequence([wait, updateAction, SKAction.wait(forDuration: 1.0), block1, wait, block2])
    centerBitsContainer.run(seq, withKey: "bitupdate")
    
  }
  
  func animateDegrees(){
    
    let array = makeBadgeArray(centerSummaryBadges)
    for i in 0..<numberOfObtainedDegrees {
      let image = array[i].image
      image.alpha = 0.0
      image.isHidden = false
      let wait = SKAction.wait(forDuration: 0.5 * Double(i))
      let a = SKAction.fadeIn(withDuration: 0.0)
      image.run(SKAction.sequence([wait, a, scaleTo]))
    }
    
  }
  
  var updateAction: SKAction {
    let bitsWait = SKAction.wait(forDuration: 0.05)
    let bitsPerform = SKAction.run({ [weak self] in
      self?.updateBitsWoker()
      })
    let updateBits = SKAction.sequence([bitsWait, bitsPerform])
    return SKAction.repeat(updateBits, count: collectedBits)
  }
  
  func fadeInTopHUD(){
    leftBitsContainer.isHidden = false
    leftBitsContainer.alpha = 0.0
    rightBadgeContainer.isHidden = false
    rightBadgeContainer.alpha = 0.0
    let wait = SKAction.wait(forDuration: 0.15)
    let fade = SKAction.fadeIn(withDuration: 0.25)
    leftBitsContainer.run(SKAction.sequence([wait, fade]))
    rightBadgeContainer.run(SKAction.sequence([wait, fade]))
  }
  
  fileprivate func updateBitsWoker(){
    currentBits += 1
    currentBits = min(currentBits, collectedBits)
    bitsLabel?.text = bitsToString(currentBits)
    animating = false
  }
  
  fileprivate func bitsToString(_ bits: Int) -> String{
    if bits >= 100 {
      return String(format: "%03d", bits)
    }
    return String(format: "%02d", bits)
  }
  
  fileprivate func makeBadgeArray(_ badges: [Badge: SummaryBadge]) -> [SummaryBadge]{
    return badges.map{ (key, value) in value }.sorted {
      return $0.sequenceNumber < $1.sequenceNumber
    }
  }
  
  fileprivate func notifyAnimationComplete(){
    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.SummaryStartAnimationComplete), object: self, userInfo: nil)
  }
  
}


