//
//  ScriptureLabel.swift
//  AngelMoroni
//
//  Created by Justin Wright on 7/2/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class ScriptureLabel: UILabel {
  
  var attrTextColor: UIColor = UIColor(netHex: GameColor.medGray.rawValue)
  var attrTextHighlightColor: UIColor = UIColor(netHex: GameColor.yellow.rawValue)
  var attrLineBreakMode: NSLineBreakMode = .byWordWrapping
  var attrLineSpacing: CGFloat = 1.0
  var attrAlignment: NSTextAlignment = .left
  var isUppercase: Bool = false
  
  var normalAttributes: [String : AnyObject] {
    let dic: [String : AnyObject] = [NSParagraphStyleAttributeName : paragraphStyle,
                                     NSFontAttributeName : attrFont,
                                     NSForegroundColorAttributeName : attrTextColor]
    return dic
  }
  
  var highlightedAttributes: [String : AnyObject] {
    let dic: [String : AnyObject] = [NSParagraphStyleAttributeName : paragraphStyle,
                                     NSFontAttributeName : attrFont,
                                     NSForegroundColorAttributeName : attrTextHighlightColor]
    return dic
  }
  
  lazy var attrFont: UIFont = {
    return  Device.isPhone ? GameFont.pixelIntv.font(size: 19) : GameFont.pixelIntv.font(size: 30)
  }()
  
  lazy var paragraphStyle: NSMutableParagraphStyle = {
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineBreakMode = self.attrLineBreakMode
    paragraphStyle.lineSpacing = self.attrLineSpacing
    return paragraphStyle
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  func styleText(_ text: String){
    let t = isUppercase ? text.uppercased() : text
    self.attributedText = attrText(t)
  }
  
  fileprivate func setup(){
    numberOfLines = 0
    adjustsFontSizeToFitWidth = false
  }
  
  
  func attrText(_ text: String)-> NSMutableAttributedString {
    let range = NSMakeRange(0, text.characters.count)
    let attrString = NSMutableAttributedString(string: text)
    attrString.addAttributes(normalAttributes, range: range)
    return attrString
  }
  
  
  
  func highlightScriptureBits(_ text: String){
    GlobalUserInteractiveQueue.async{ [weak self] in
      self?.peformReplacementsForRange(text)
    }
  }
  
  
  fileprivate func applyStyleToRange(_ text: String, range: NSRange){
    //let pattern = "(\\%\\w+(\\s\\w+)*\\%)"
    var pattern = "(\\%([^\\%])*\\%)"
    var regex = RegExp(pattern: pattern, options: .caseInsensitive)
    var matches = regex?.findRangeOfAllMatches(text)
    
    var attrString = NSMutableAttributedString(string: text)
    attrString = attrText(text)
    
    matches?.forEach{
      if let range = $0 {
        
        attrString.addAttributes(highlightedAttributes, range: range)
        
        if NSMaxRange(range) + 1 < attrString.length {
          let r = NSMakeRange(NSMaxRange(range) + 1, 1)
          attrString.addAttributes(normalAttributes, range: r)
        }
      }
    }
    
    pattern = "([\\%])"
    regex = RegExp(pattern: pattern, options: .caseInsensitive)
    matches = regex?.findRangeOfAllMatches(text)
    var offset = 0
    let count = attrString.length
    matches?.forEach{
      if let range = $0 {
        if NSMaxRange(range) <= count {
          let l = max(0, range.location - offset)
          //print("L \(l) and range \(range)")
          let r = NSMakeRange(l, 1)
          attrString.deleteCharacters(in: r)
          offset += 1
        }
      }
    }
    
    
    GlobalMainQueue.async{
      self.attributedText = attrString
    }
  }
  
  fileprivate func peformReplacementsForRange(_ text: String){
    let nsString = text as NSString
    let range = NSMakeRange(0, text.characters.count)
    let startOfRange = nsString.lineRange(for: NSMakeRange(range.location, 0))
    let endOfRanage = nsString.lineRange(for: NSMakeRange(NSMaxRange(range), 0))
    
    var extendedRange = NSUnionRange(range, startOfRange)
    extendedRange = NSUnionRange(extendedRange, endOfRanage)
    applyStyleToRange(text, range: extendedRange)
  }
  
}
