//
//  LSContentView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 6/25/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import RealmSwift

final class SummaryView: BaseView {
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var contentView: UIView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var scriptureLabel: ScriptureLabel!
  @IBOutlet weak var buttonTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var nextButton: ModalMenuButton!
  
  lazy var titleFont: UIFont = {
    return Device.isPhone ? GameFont.pixelIntv.font(size: 26) : GameFont.pixelIntv.font(size: 44)
  }()
  
  let navManager = NavigationManager.sharedInstance
  var notification: NSNotificationProxy?
  var top: CGFloat = 0
  var rawText: String?
  weak var delegate: SummaryViewDelegate?
  
  convenience init(parent view: UIView){
    self.init(frame: CGRect.zero)
    self.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(self)
    
    layoutConstraints()
    notification = NSNotificationProxy(notificationKey: Notification.SummaryStartAnimationComplete){ [weak self] _ in
      self?.animateView()
    }

  }
  
  deinit {}
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    //print("Summary View is init now!")
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func xibSetup() {
    loadViewFromNibAndPrepViewLayout("SummaryView")
    
  }
  
  
  override func setup() {
    super.setup() // called in awakeFromNib
    
    contentView.backgroundColor = UIColor.clear
    //scrollView.backgroundColor = UIColor.purple
    
    //scrollView.userInteractionEnabled = false
    scrollView.delegate = self
    scrollView.showsVerticalScrollIndicator = false
    scrollView.showsHorizontalScrollIndicator = false
    
    let white = UIColor(netHex: GameColor.lightGray.rawValue)
    
    titleLabel.textColor = white
    titleLabel.font = titleFont
    titleLabel.numberOfLines = 2
    
    scriptureLabel.text = nil
    scriptureLabel.numberOfLines = 0
    scriptureLabel.lineBreakMode = .byTruncatingTail
    
    
    guard let level = NavigationManager.sharedInstance.currentLevelPath?.level,
      let rawText = GameManager.sharedInstance.scriptureMaster?.completedScripture else {
        return
    }
    
    titleLabel.text = level.title
    scriptureLabel.highlightScriptureBits(rawText)
    self.rawText = rawText
    
    
    // Prevent a user from moving on if last level in world 1.
    if let level = NavigationManager.sharedInstance.currentLevelPath?.level, let levels = level.world.first?.levels {
      if level.order == levels.last?.order {
        nextButton.isEnabled = false
        nextButton.alpha = 0.4
      }
    }
    
  }
  
  
  @IBAction func onMenu(_ sender: AnyObject){
    UIView.animate(withDuration: 0.0, animations: {
      self.scrollView.alpha = 0.0
    }, completion: { _ in
      //delay(0.10){ [weak self] in
        self.navManager.transitionByGameState(.returnToMenu)
        self.removeFromSuperview()
      //}
    })
    
  }
  
  @IBAction func onRetry(_ sender: AnyObject){
    navManager.transitionByGameState(.retry)
    removeFromSuperview()
  }
  
  @IBAction func onNext(_ sender: AnyObject){
    // TODO: Make this dynamic for all words
    guard let level = NavigationManager.sharedInstance.currentLevelPath?.level else { return }
    
    let m = level.world.first?.levels.count ?? 0
    let index = min(level.order + 1, (m - 1))
    
    //print("Next Level = \(index) and count = \(m)")
    
    //let realm = Realm.readOnlyInstance()
    guard let nextLevel = level.world.first?.levels[index] else { return }
    
    
    // Order
    NavigationManager.sharedInstance.currentLevelPath = (nextLevel, IndexPath(row: index, section: 0  ))
    NavigationManager.sharedInstance.transitionByGameState(.nextLevel)
    
    removeFromSuperview()
  }
  
  
  
  func adjustScrollView(_ top: CGFloat){
    scrollView.contentInset.top = screenSize.height
    self.top = top * 2.4
    adjustButtonLayout(top)
  }
  
  func animateView(){
    //print("Main thread \(NSThread.isMainThread()) \(self.superview)")
    UIView.animate(withDuration: 0.75, delay: 0.25,
                               usingSpringWithDamping: 0.6, initialSpringVelocity: 2.0,
                               options: .curveEaseIn, animations: { [weak self] in
      self?.scrollView.contentInset.top = self?.top ?? 0
      }, completion: { [weak self] _ in
        self?.scrollView.isUserInteractionEnabled = true
    })
  }
  
  func adjustButtonLayout(_ top: CGFloat){
    guard let title = titleLabel.text, let text = self.rawText else { return }
    
    let screenHeight = screenSize.height
    let width = screenSize.width - 72
    let titleHeight = UILabel.heightForLabel(titleFont, width: width, text: title)
    let scriptureHeight = text.heightWithConstrainedWidth(width: width, font: scriptureLabel.attrFont)
    let buttonHeight = self.nextButton.intrinsicContentSize.height
    let extraPadding: CGFloat = Device.isPhone ? 16 : 64
    let btmPadding: CGFloat = Device.isPhone ? 48 : 64
    let padding = extraPadding + btmPadding + buttonTopConstraint.constant + 24
    
    let height:CGFloat = top + titleHeight + scriptureHeight + buttonHeight + padding
    
    //print("text")
    //print(text)
    
    
    
    //print("Height = \(height) and screen Height = \(screenHeight) and button Height \(buttonHeight)")
    //print("Title \(titleHeight) and scripture \(scriptureHeight) and constant \(buttonTopConstraint.constant)")
    
    
    
    if height < screenHeight {
      let h = screenHeight - height
      buttonTopConstraint.constant = h
      //print("Layout out constant to be \(h)")
    }
    self.layoutIfNeeded()
  }
  
}

extension SummaryView: UIScrollViewDelegate {
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    delegate?.didScroll(scrollView)
  }
  
}

protocol SummaryViewDelegate: class {
  func didScroll(_ scrollView: UIScrollView)
}
