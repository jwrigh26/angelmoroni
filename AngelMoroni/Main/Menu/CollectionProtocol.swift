//
//  CollectionLayoutProtocol.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/15/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit


// This needs to go away
protocol DataProtocol {
  var itemNumber: Int { get }
}

protocol CollectionLayout: class {
  
  var frameSize: CGSize? { get }
  var frameSizeMessage: String { get }
  
  func adjustSpacingForBounds(_ frame: CGSize, item: CGSize)
  func defineItemSize(_ frame: CGSize)-> CGSize
  func scrollInsetOffset()
}

extension CollectionLayout {
  
  var frameSizeMessage: String {
    return "Please set frameSize before calling"
  }
  
  var collectionViewMessage: String {
    return "Please set the collection view. It won't work without one"
  }
  
  
  
  func scrollInsetOffset(){
    guard
      let flowLayout = self as? UICollectionViewFlowLayout,
      let collectionView = flowLayout.collectionView else {
        assertionFailure(frameSizeMessage)
        return
    }
    
    var insets = collectionView.contentInset
    insets.left = 0
    insets.right = 0
    collectionView.contentInset = insets
    collectionView.decelerationRate = UIScrollViewDecelerationRateNormal // makes a more native feel that is closer to the UIScrollView
    
  }
  
  
}


protocol CollectionView: class {
  //var layout: CollectionLayout { get }
  var identifier: String { get }
  
  func setupLayout()
  func layoutConstraints()
  
  
}

extension CollectionView {
  
  func scrollTo(_ indexPath: IndexPath){
    if let view = self as? UICollectionView {
      view.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: false)
    }
  }
  
  func layoutConstraints(){
    guard let view = self as? UICollectionView else { return }
    
    view.translatesAutoresizingMaskIntoConstraints = false
    
    let views = ["view": view]
    var allContraints = [NSLayoutConstraint]()
    let viewVerticalContraints = NSLayoutConstraint.constraints(
      withVisualFormat: "V:|[view]|",
      options: [],
      metrics: nil,
      views: views)
    allContraints += viewVerticalContraints
    
    let viewHorizontalConstraints = NSLayoutConstraint.constraints(
      withVisualFormat: "H:|[view]|",
      options: [],
      metrics: nil,
      views: views)
    allContraints += viewHorizontalConstraints
    
    NSLayoutConstraint.activate(allContraints)
  }
}
