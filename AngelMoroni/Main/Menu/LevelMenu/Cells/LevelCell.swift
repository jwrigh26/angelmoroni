//
//  LevelCell.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/14/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

class LevelCell: GameCell {

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  
  
  
  override func styleLabels(){
    let titleFontSize: CGFloat = Device.isPhone ? 18 : 32
    let titleFont = GameFont.pixelIntv.font(size: titleFontSize)
    
    titleLabel.font = titleFont
    titleLabel.textColor = GameColor.lightGray.color
    
    let bitLabelFontSize: CGFloat = Device.isPhone ? 12 : 21
    let bitLabelFont = GameFont.pixelIntv.font(size: bitLabelFontSize)
    let bitFontSize: CGFloat = Device.isPhone ? 15 : 24
    let bitFont = GameFont.pixelIntv.font(size: bitFontSize)
    
    scriptureBitHeaderLabel.font = bitLabelFont
    scriptureBitHeaderLabel.textColor = GameColor.lightGray.color
    scriptureBitHeaderLabel.text = CommonStrings.Bits.uppercased()
    
    scriptureBitLabel.font = bitFont
    scriptureBitLabel.textColor = GameColor.yellow.color
    scriptureBitLabel.text = ""
  }
  
  
}
