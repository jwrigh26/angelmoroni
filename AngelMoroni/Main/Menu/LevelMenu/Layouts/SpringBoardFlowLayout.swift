//
//  SpringBoardFlowLayout.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/15/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//
//  For a horizontally scrolling grid, this value represents the minimum spacing between items in the same column. // row
//  minimumInteritemSpacing = 10
//
//
//  For a horizontally scrolling grid, this value represents the minimum spacing between successive columns. // column
//  minimumLineSpacing = 10

import UIKit


class SpringBoardFlowLayout: UICollectionViewFlowLayout, CollectionLayout {
  
  let levelColumnCount = 2 //5
  
  var verticalSizeClass: UIUserInterfaceSizeClass? {
    return collectionView?.traitCollection.verticalSizeClass
  }
  
  // This adjusts how much space to take up at the bottom of the screen for levels
  var contentHeightPercent: CGFloat {
    if let trait = verticalSizeClass , trait == .compact {
      //print("It's compact")
      return 0.72
    }
    return 0.8
  }
  
  var itemSizePercent: CGFloat {
    if let trait = verticalSizeClass , trait == .compact {
      return 0.25
    }
    return 0.20
  }
  
  var frameSize: CGSize?
  var nbColumns = -1
  var nbLines = -1
  var contentHeight: CGFloat = 0
  var spacing:CGFloat = 0
  

  func defineItemSize(_ frame: CGSize)-> CGSize {
    let value = GameManager.sharedInstance.screenSize.height
    let w: CGFloat = value * (itemSizePercent * 3)
    let hx: CGFloat = verticalSizeClass == .compact ? 1.2 : 1.5
    let h: CGFloat = value * (itemSizePercent * hx)
    let itemSize = CGSize(width: w, height: h)
    return itemSize
  }
  
  
  func adjustSpacingForBounds(_ frame: CGSize, item: CGSize) {
    spacing = calculateSpacing(frame, item: item)
    minimumLineSpacing = spacing
    minimumInteritemSpacing = 10
    let topInset = verticalSizeClass == .compact ? spacing/2.0 : spacing * 2.0
    //print("Top Inset = \(topInset) and verticalSize class = \(verticalSizeClass?.rawValue) and regular \(UIUserInterfaceSizeClass.regular.rawValue)")
    let insets = UIEdgeInsetsMake(topInset, spacing / 2.0, 0, spacing / 2.0) // T L B R
    sectionInset = insets
  }
  
  fileprivate func calculateSpacing(_ frame: CGSize, item: CGSize) -> CGFloat {
    
    let columnCount = levelColumnCount
    
    // Take the frame width and subtract the total width of items and divde by the number of items
    let column: CGFloat = (( frame.width - ( CGFloat(columnCount) * item.width ) ) / CGFloat(columnCount))
    //print("Spacing \(column) and frame width \(frame.width) and column count \(columnCount) and item \(item.width)")
    return column
  }
  
  override func prepare() {
    super.prepare()
    guard let frameSize = frameSize else {
      assertionFailure(frameSizeMessage)
      return
    }
    collectionView?.showsHorizontalScrollIndicator = false
    collectionView?.showsVerticalScrollIndicator = false
    collectionView?.isPagingEnabled = true
    scrollDirection = .horizontal
    itemSize = defineItemSize(frameSize)
    adjustSpacingForBounds(frameSize, item: itemSize)
    contentHeight = frameSize.height * contentHeightPercent
    adjustContraints(frameSize)
   
  }
  
  override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
    return true
  }
  
  
  override var collectionViewContentSize : CGSize {
    
    let size = super.collectionViewContentSize
    let collectionViewWidth = collectionView?.frame.size.width ?? 0
    let n = round (100.0 * Double(size.width / collectionViewWidth)) / 100.0 // Need to get rid of any thing less than the hundredth place
    let nbOfScreens:CGFloat = ceil(CGFloat(n))
    
    
    let newSize = CGSize( width: (nbOfScreens) * collectionViewWidth, height: size.height) //size.height
    return newSize
  }
  
  
  var printed = false
  
  func calculateGridLayout(_ collectionView: UICollectionView) -> (columns: Int, rows: Int){
    
    let frame = collectionView.frame.size
    let itemWidth = itemSize.width + minimumLineSpacing
    let itemHeight = itemSize.height + minimumInteritemSpacing
    let c:Int = Int(frame.width / itemWidth)
    let r:Int = Int(frame.height / itemHeight)
    
    
    let cols = self.nbColumns != -1 ? self.nbColumns : c
    let rows = self.nbLines != -1 ? self.nbLines : r

    return (cols, rows)
  }
  
  override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
    guard let collectionView = self.collectionView else {
      assertionFailure(collectionViewMessage)
      return nil
    }
    
    let gridLayout = calculateGridLayout(collectionView)
    let nbColumns = gridLayout.0; let nbLines = gridLayout.1
    
    let idxPage = Int((indexPath as NSIndexPath).row / (nbColumns * nbLines) )
    //print("IdxPage = \(idxPage) reald indexPath = \(indexPath.row) and / by \((nbColumns * nbLines))")
    
    let orderLayout = (indexPath as NSIndexPath).row - (idxPage * nbColumns * nbLines)
    //print("OrderLayout \(orderLayout) and nbColumns \(nbColumns) and nbLines \(nbLines)")
    
    let xD = Int(orderLayout / nbColumns) // Decides which row
    let yD = orderLayout % nbColumns // Decides which column
    let D = xD + yD * nbLines + idxPage * nbColumns * nbLines // Decides new row
    //print("XD \(xD) and YD \(yD) and D \(D)")
    
    let fakeIndexPath = IndexPath(item: D, section: (indexPath as NSIndexPath).section)
    //print("FakeINdexPath \(fakeIndexPath)")
    let attributes = super.layoutAttributesForItem(at: fakeIndexPath)
    
    return attributes
  }
  
  
  
  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    
    //print("RECT = \(rect)")
    
    let newX:CGFloat = min(0, rect.origin.x - rect.size.width / 2)
    let newWidth: CGFloat = rect.size.width * 2 + (rect.origin.x - newX)
    
    
    
    let newRect = CGRect(x: newX, y: rect.origin.y, width: newWidth, height: rect.size.height) //rect.size.height
    
    
    var returnArray = [UICollectionViewLayoutAttributes]()
    
    // Get all the atributes for the element in the specified frame
    if let allAttributesInRect = super.layoutAttributesForElements(in: newRect) {
      for attr in allAttributesInRect {
        if let newAttr = self.layoutAttributesForItem(at: attr.indexPath)?.copy() as? UICollectionViewLayoutAttributes {
          newAttr.indexPath = (attr.indexPath as NSIndexPath).copy() as! IndexPath
          
          returnArray.append(newAttr)
        }
      }
      return returnArray
    }
    
    return super.layoutAttributesForElements(in: rect)
  }
  
  
  fileprivate func adjustContraints(_ frameSize: CGSize){
    
    let constant = frameSize.height - contentHeight
      for c in collectionView!.superview!.constraints {
        if let _ = c.secondItem as? UICollectionView , c.secondAttribute == .bottom {
          c.constant = constant
          break
        }
        
      }
    
      collectionView!.setNeedsDisplay()
  }
  
  
  
}


