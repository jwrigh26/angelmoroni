//
//  LevelCollectionView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class LevelCollectionView: UICollectionView, CollectionView {
  
  let identifier = "LevelCell"
  
  func setupLayout(){
    guard let layout = self.collectionViewLayout as? SpringBoardFlowLayout else { return }
    layout.frameSize = GameManager.sharedInstance.screenSize
    
    register(UINib(nibName: "LevelCell", bundle: nil), forCellWithReuseIdentifier: identifier)
    
    
  }
  
}
