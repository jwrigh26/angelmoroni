//
//  StartGameViewController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit
import SpriteKit
import RealmSwift
import Crashlytics

final class LevelMenu: NSObject, Menu, UICollectionViewDataSource, UICollectionViewDelegate {
  
  
  var levels: List<Level>!
  var collectionView: CollectionView!
  
  fileprivate var _collectionView: LevelCollectionView? {
    if let collectionView = self.collectionView as? LevelCollectionView {
      return collectionView
    }
    return nil
  }
  
  override init() {
    super.init()
    
    if let world = NavigationManager.sharedInstance.currentWorldPath?.world {
      levels = world.levels
    }
    let layout = SpringBoardFlowLayout()
    collectionView = LevelCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    _collectionView?.setupLayout()
    _collectionView?.dataSource = self
    _collectionView?.delegate = self
  }
  
  func collectionViewLoaded() {
    scrollToLevel()
  }
  
  func onBack(){
    NavigationManager.sharedInstance.transitionByGameState(.world)
  }
  
  
  func scrollToLevel(){
    guard let level = NavigationManager.sharedInstance.currentLevelPath,
      let layout = _collectionView?.collectionViewLayout as? SpringBoardFlowLayout else { return }
    
    let grid = layout.calculateGridLayout(_collectionView!)
    let item = CGFloat((level.1 as IndexPath).row + 1)
    let nbItemsPerScreen = CGFloat(grid.0 * grid.1)
    let screen = floor(item / nbItemsPerScreen)
    let size = GameManager.sharedInstance.screenSize
    let offset = CGPoint(x: size.width * screen, y: 0)
    
    // This was cool but not needed
    //let nbScreens = ceil(CGFloat(menuDataSource.data.count) / CGFloat(grid.0 * grid.1))
    
    _collectionView?.setContentOffset(offset, animated: false)
    NavigationManager.sharedInstance.currentLevelPath = nil
  }
  
  // MARK: Collection View Delegate
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    let level = levels[indexPath.row]
    NavigationManager.sharedInstance.currentLevelPath = (level, indexPath)
    NavigationManager.sharedInstance.transitionByGameState(.startingGame)
    
    
    let w = NavigationManager.sharedInstance.currentWorldPath?.1.row ?? -1
    let l = indexPath.row + 1
    Answers.logLevelStart("World: \(w) Level: \(l)", customAttributes: nil)
    
  }
  
  // MARK: Collection View Datasource
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return levels.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let identifier = self.collectionView.identifier
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    let level = levels[indexPath.row]
    let key = level.world.first?.id ?? ""
    
    
    if let cell = cell as? LevelCell,
      let lvls = MenuGameDataFeed.sharedInstance.levels[key],
      let lvl = lvls[level.id]{
      cell.title = "\(level.title)"
      cell.scripture(bits: lvl.collectedBits, total: lvl.totalBits)
      cell.degreeOfGlory = lvl.degreeOfGlory
      
      //print("Title \(level.title) and Degree = \(lvl.degreeOfGlory)")
    }
    return cell
  }
  
}
