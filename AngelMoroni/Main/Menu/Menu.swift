//
//  Menu.swift
//  AngelMoroni
//
//  Created by Justin Wright on 2/7/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit


protocol Menu: class {
  
  var collectionView: CollectionView! { get set}
  var gameState: GameState { get set }
  
  init()
  
  func addCollectionViewToParent(_ parent: UIView)
  func cleanup()
  func layoutSubViewForMenu()
  func collectionViewLoaded()
  func hideMenu()
  func onBack()
  
}

extension Menu {
  
  var gameState: GameState {
    get {
      return GameManager.sharedInstance.gameState
    }
    set {
      GameManager.sharedInstance.gameState = newValue
    }
  }
  
  func addCollectionViewToParent(_ parent: UIView){
    guard let collectionView = collectionView as? UICollectionView else { return }
    
    collectionView.backgroundColor = UIColor.clear
    parent.insertSubview(collectionView, at: 0)
    
    self.collectionView.layoutConstraints()
    
    
  }
  
  func hideMenu(){
    guard let collectionView = collectionView as? UICollectionView else { return }
    collectionView.isHidden = true
  }
  
  func showMenu(){
    guard let collectionView = collectionView as? UICollectionView else { return }
    collectionView.isHidden = false
  }
  
  func layoutSubViewForMenu(){}
  
  func collectionViewLoaded(){}
  
  func cleanup(){
    if let collectionView = self.collectionView as? UICollectionView {
      collectionView.removeFromSuperview()
      self.collectionView = nil
    }
  }
}
