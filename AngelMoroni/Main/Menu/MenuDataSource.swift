//
//  StartGameDelegate.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/16/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

typealias ConfigureCellBlock = (_ cell: GameCell, _ item: DataProtocol?) -> ()


class MenuDataSource: NSObject, UICollectionViewDataSource {
  
  let identifier: String
  var data = [DataProtocol]()
  var configureCellBlock: ConfigureCellBlock?
  
  init(collectionView: CollectionView, configureCellBlock: @escaping ConfigureCellBlock){
    self.identifier = collectionView.identifier
    self.configureCellBlock = configureCellBlock
    super.init()
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return data.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
      let item = data[(indexPath as NSIndexPath).row]
      
      if let cell = cell as? GameCell , configureCellBlock != nil {
        configureCellBlock!(cell, item)
      }
    
      return cell
  }
  
  
}


