//
//  MenuDelegate.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

typealias MenuIndexConfigureBlock = (_ indexPath: IndexPath) -> ()

class MenuDelegate: NSObject, UICollectionViewDelegateFlowLayout {
  
  var menuIndexConfigureBlock: MenuIndexConfigureBlock?
  
  init(block: @escaping MenuIndexConfigureBlock){
    self.menuIndexConfigureBlock = block
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    if menuIndexConfigureBlock != nil {
      return menuIndexConfigureBlock!(indexPath)
    }
  }
  
}
