//
//  MenuGameDataFeed.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/15/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift

class MenuGameDataFeed {
  
  static let sharedInstance = MenuGameDataFeed()
  
  
  
  // ID of world, summary
  var worlds:[String: DataSummary] = [String: DataSummary]()
  
  // ID of world, [ ID of level , summary ]
  var levels:[String : [String: DataSummary]] = [String: [String: DataSummary]]()
  
  func preloadData(){
    let staticRealm = Realm.readOnlyInstance()
    let realm = Realm.mainInstance()
    
    let worlds = staticRealm.objects(World.self)
    let user = realm.object(ofType: User.self, forPrimaryKey: "1")
    
    for world in worlds {

      // Create world summary and assign back to world dictionary
      self.worlds[world.id] = assign(world: world, for: user)
      
      var dic:[String: DataSummary] = [String: DataSummary]()
      for level in world.levels {
        dic[level.id] = assign(level: level, for: user)
      }
      
      self.levels[world.id] = dic
    }
    
  }
  
  
  
  
  
  func debug(){
    
    for (key, value) in worlds {
      print("Key \(key) and bits \(value.collectedBits)/\(value.totalBits) and Degree = \(value.degreeOfGlory)")
      
      print("\nLevels for World")
      if let dic = levels[key]{
        for (key, value) in dic {
          print("Key = \(key) and bits \(value.collectedBits)/\(value.totalBits) and Degree = \(value.degreeOfGlory)")
        }
      }
      print("\n-------------------\n")
    }
    
  }
  
  fileprivate func assign(world: World, for user: User?) -> DataSummary {
    
    // Make the DataSumamry for worlds first
    let totalBits:Int = world.levels.map{ $0.scripture!.totalBits }.reduce(0, +)
    var collectedBits:Int = 0
    var degreeOfGlory: DegreeOfGlory = .none
    
    // Check if we have a level from the user and update the summary values
    if let levels = user?.levels {
      let lvls = levels.filter("worldKey = '\(world.id)'")
      collectedBits = lvls.map{ $0.bitsCollected}.reduce(0, +)
      let degrees: [Int] = Array(lvls.map{ $0.degreeOfGlory})
      degreeOfGlory = assignWorldDegree(degrees: degrees, numberOfLevels: world.levels.count)
    }
    
    let summary = DataSummary(bits: collectedBits, total: totalBits, degree: degreeOfGlory)
    return summary
  }
  
  fileprivate func assign(level: Level, for user: User?) -> DataSummary {
    let total: Int = level.scripture?.totalBits ?? 0
    var collected: Int = 0
    var degree: DegreeOfGlory = .none
    if let lvl = user?.levels.filter("levelKey = '\(level.id)'").first, let d = DegreeOfGlory(rawValue: lvl.degreeOfGlory) {
      collected = lvl.bitsCollected
      degree = d
    }
    let summary = DataSummary(bits: collected, total: total, degree: degree)
    //print("Level Summary bits \(collected) and total \(total) and degree \(degree)")
    return summary
  }
  
  
  fileprivate func assignWorldDegree(degrees:[Int], numberOfLevels levels: Int) -> DegreeOfGlory {
    guard levels != 0 else { return .none }
    
    //print ("Degrees colleced = \(degrees.count) and lvls = \(levels)")
    
    //let amount = degrees.count
    //print("Amount collected = \(amount)")
    
    let sunTotal = levels * DegreeOfGlory.sun.rawValue
    let moonTotal = levels * DegreeOfGlory.moon.rawValue
    let starTotal = levels * DegreeOfGlory.star.rawValue
    
    //print("Amount = \(amount) sun = \(sunTotal) moon = \(moonTotal) star = \(starTotal)")
    
    let stars = degrees.count
    let moons = degrees.filter{ return $0 == DegreeOfGlory.moon.rawValue || $0 == DegreeOfGlory.sun.rawValue }.count
    let suns = degrees.filter{ return $0 == DegreeOfGlory.sun.rawValue }.count
    
    //print("Stars = \(stars) moons \(moons) and sun \(suns)")
    
    if suns + moons + stars >= sunTotal {
      //print("returning sun")
      return .sun
    }
    
    if moons + stars >= moonTotal {
      //print("returning moon")
      return .moon
    }
    
    if stars >= starTotal {
      //print("returning star")
      return .star
    }
    
    return .none
    
  }
  
  
  
  func queryFun(){
    
    let staticRealm = Realm.readOnlyInstance()
    _ = staticRealm.objects(World.self).map{ $0.id }
    
    let realm = Realm.mainInstance()
    let user = realm.object(ofType: User.self, forPrimaryKey: "1")
    
    guard (user?.levels) != nil else {
      print("No Levels for User")
      return
    }
    
    //print("User has data for \(levels.count) levels")
    
   // for id in worlds {
      
      // Let's get total bits for all levels in a world
     // let world = staticRealm.object(ofType: World.self, forPrimaryKey: id)
      
      //let totalBits:Int = world!.levels.map{ $0.scripture!.totalBits }.reduce(0, +)
      
      //let lvls = levels.filter("worldKey = '\(id)'")
      
      //let collectedBits:Int = lvls.map{ $0.bitsCollected }.reduce(0, +)
      
//      /print("World \(id) and total collected bits so far \(collectedBits) out of \(totalBits)\n")
//      for lvl in lvls {
//        print("Level harts = \(lvl.harts) and bitsCollected \(lvl.bitsCollected) and degree \(lvl.degreeOfGlory)")
//        print("")
//      }
//      
//      print("-------")
   // }
    
  }
}

struct DataSummary {
  
  let totalBits: Int
  let collectedBits: Int
  let degreeOfGlory: DegreeOfGlory
  
  init(bits: Int, total: Int, degree: DegreeOfGlory){
    totalBits = total
    collectedBits = bits
    degreeOfGlory = degree
  }
}
