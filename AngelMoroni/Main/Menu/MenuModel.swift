//
//  StartGameModel.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/27/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

typealias dataComplete = ()-> Void

enum DataType {
  case undefined
  case world
  case level
}

class MenuModel: NSObject {
  
  var data = [DataProtocol]() {
    didSet {
      dataSource.data = data
    }
  }
  var dataSource: MenuDataSource!
  var dataType: DataType = .undefined
  
  override init(){
    super.init()
  }
  
  convenience init(dataSource: MenuDataSource, dataType: DataType){
    self.init()
    self.dataSource = dataSource
    self.dataType = dataType
  }
  
}
