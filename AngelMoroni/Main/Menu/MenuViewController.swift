//
//  MenuViewController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import SpriteKit
import RealmSwift

class MenuViewController: BaseGameViewController  {
  
  let duration: Double = 0.10
  var delayTime: Double = 0.0
  var backButton:MenuActionButton?
  var pauseButton: PauseButton!
  var currentMenu: Menu?
  var pauseModal: PauseModal?
  var gameOverModal: GameOverModal?
  var gameStartView: GameStartView?
  weak var summaryView: SummaryView?
  weak var startView: StartView?
  var menuCurtain: BlackCurtain?
  
  var worldMenu: Menu {
    let menu = WorldMenu()
    return menu
  }
  
  var levelMenu: Menu {
    let menu = LevelMenu()
    return menu
  }
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    currentMenu?.layoutSubViewForMenu()
  }
  
  func readyToPlay(){
    if let startView = self.startView {
      startView.readyToPlay()
    }
  }
  
  
  func removeMenuIfNecessary(_ animate: Bool = true, complete: @escaping ()->()){
    guard let view = self.currentMenu?.collectionView as? UICollectionView
      , currentMenu != nil else { complete(); return }
    
    let d = animate ? self.duration : 0.0
    
    UIView.animate(withDuration: d, animations: { _ in
      view.alpha = 0.0
      }, completion: { [weak self] _ in
        self?.currentMenu?.cleanup()
        self?.currentMenu = nil // reset current
        complete()
    })
  }
  
  func removeStart(){
    if let startView = self.startView {
      UIView.animate(withDuration: self.duration, animations: { _ in
        startView.alpha = 0.0
        }, completion: { [weak self] _ in
          self?.startView?.removeFromSuperview()
      })
    }
  }
  
  func retryCurrentLevel(){
    skView.scene?.isPaused = false
    LevelScreenCoordinator.sharedInstance.reset()
    removePauseButton()
    hidePauseScreen()
    hideGameOverScreen()
    createSegueScene()
  }
  
  
  // MARK: - Actions
  func onBackButton(){
    switch gameState {
    case .world, .level, .worldNoAnimate:
      currentMenu?.onBack()
    default:
      break
    }
  }
  
  func onPauseButton(){
    switch GameManager.sharedInstance.gameState {
    case .playing:
      NavigationManager.sharedInstance.transitionByGameState(.paused)
      break
    default:
      break
    }
  }
  
  // This is the method for going back to the menu from game scene
  func returnToMenu(){
    self.createMenuScene()
    LevelScreenCoordinator.sharedInstance.reset()
    NavigationManager.sharedInstance.transitionByGameState(.level)
  }
  
  func showGameStartScreen(){
    if gameStartView != nil { hideGameStartScreen() }
    gameStartView = GameStartView(parent: self.view)
    if let level = NavigationManager.sharedInstance.currentLevelPath?.level {
      gameStartView?.title = level.title
    }else{
      gameStartView?.title = ""
    }
  }
  
  func hideGameStartScreen(){
    gameStartView?.removeFromSuperview()
    gameStartView = nil
  }
  
  func showPauseScreen() {
    addMenuCurtain()
    if pauseModal != nil { hidePauseScreen() }
    pauseModal = PauseModal(parent: self.view, bits: bitsCollected, degree: degreeOfGlory)
  }
  
  func hidePauseScreen(){
    removeMenuCurtain()
    pauseModal?.removeFromSuperview()
    pauseModal = nil
    
  }
  
  func showGameOverScreen(){
    addMenuCurtain()
    if gameOverModal != nil { hideGameOverScreen() }
    gameOverModal = GameOverModal(parent: self.view)
  }
  
  func hideGameOverScreen(){
    removeMenuCurtain()
    gameOverModal?.removeFromSuperview()
    gameOverModal = nil
  }
  
  func showSummaryView(_ scene: LevelSummaryScene){
    if summaryView == nil {
      summaryView = SummaryView(parent: self.view)
      summaryView?.delegate = scene
      summaryView?.adjustScrollView(scene.hudHeight)
    }
  }
  
  func showStartView(_ scene: StartScene, skipLoading: Bool = false){
    if startView == nil {
      startView = StartView(parent: self.view)
      
      // Coming back from a world, we don't want the flashy loading animation
      if skipLoading {
        startView?.alpha = 0.0
        startView?.disableFlashingButtonAnimation()
        UIView.animate(withDuration: self.duration){
          self.startView?.alpha = 1.0
        }
      }
    }
  }
  
  // MARK: - All transition methods
  
  func transitionToGame(_ callback: ()->()){
    skView.scene?.isPaused = false
    currentMenu?.cleanup()
    currentMenu = nil
    createGameScene()
    removeBackButton()
    callback()
  }
  
  func transitionToLevelSummary(){
    skView.scene?.isPaused = false
    removePauseButton()
    let scene = createLevelSummaryScene()
    showSummaryView(scene)
  }
  
  func transitionMenu(_ menu: Menu, animate: Bool = true){
    removeStart()
    removeMenuIfNecessary(animate){ [weak self] in
      self?.currentMenu = nil
      self?.currentMenu = menu
      self?.addMenu(animate)
      self?.collectionViewLoaded()
      self?.fadeInCollectionView(animate)
      self?.removePauseButton()
      self?.addBackButton()
      self?.hidePauseScreen()
      self?.hideGameOverScreen()
    }
  }
  
  func transitionToStart(skipLoading: Bool = false){
    guard currentMenu == nil else {
      transitionToStartFromMenu()
      return
    }
    
    skView.scene?.isPaused = false
    removePauseButton()
    removeBackButton()
    
    var startScene = skView.scene
    
    if startScene == nil {
      startScene = createStartScene()
    }
    
    
    if let scene = startScene as? StartScene {
      scene.moveCloudsUp()
      showStartView(scene, skipLoading: skipLoading)
    }
  }
  
  func transitionToStartFromMenu(){
    removeMenuIfNecessary(true){ [weak self] in
      self?.transitionToStart(skipLoading: true)
    }
  }
  
  
  // MARK: - Private helper methods
  
  fileprivate func addMenuCurtain(){
    if menuCurtain != nil { removeMenuCurtain() }
    menuCurtain = BlackCurtain(layer: self.view.layer, opacity: 0.64)
    self.view.layer.addSublayer(menuCurtain!)
  }
  
  fileprivate func removeMenuCurtain(){
    menuCurtain?.removeFromSuperlayer()
    menuCurtain = nil
  }
  
  fileprivate func addBackButton(){
    // Create Back Button
    if backButton != nil { removeBackButton() }
    
    backButton = MenuActionButton(imageView: UIImageView(image: #imageLiteral(resourceName: "back"))) //BackButton(screenSize, padding: 32.0)
    backButton!.addTarget(self, action: #selector(MenuViewController.onBackButton), for: .touchUpInside)
    self.view.addSubview(backButton!)
    let leadSpace:CGFloat = Device.isPhone ? 32 : 48
    
    
    let btmSpace = (screenSize.height - (screenSize.height / 1.8)) / 4
    
    backButton!.translatesAutoresizingMaskIntoConstraints = false
    backButton!.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadSpace).isActive = true
    backButton!.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -(btmSpace - (backButton!.frame.height / 2) )).isActive = true
  }
  
  fileprivate func removeBackButton(){
    backButton?.removeFromSuperview()
    backButton = nil
  }
  
  func addPauseButton(){
    if pauseButton != nil { removePauseButton() }
    pauseButton = PauseButton()
    self.view.addSubview(pauseButton)
    let y: CGFloat = device.isXLargeScreen ? 8 : 5
    pauseButton.frame.origin.x += screenSize.width - (pauseButton.bounds.width + 8)
    pauseButton.frame.origin.y = y // We want 16 but there is 9 pixels of spacing in the image to account for
    pauseButton.addTarget(self, action: #selector(MenuViewController.onPauseButton), for: .touchUpInside)
    
  }
  
  fileprivate func removePauseButton(){
    pauseButton?.removeFromSuperview()
    pauseButton = nil
  }
  
  fileprivate func addMenu(_ animate: Bool){
    guard let currentMenu = self.currentMenu else { return }
    currentMenu.addCollectionViewToParent(skView)
    
    // Prevent glitch
    if let cv = currentMenu.collectionView as? UICollectionView , animate {
      cv.alpha = 0.0
    }
    
  }
  
  fileprivate func collectionViewLoaded(){
    self.view.layoutIfNeeded() // hog the main queue
    GlobalMainQueue.async{ [weak self] in
      // Make this wait for the main queue
      self?.currentMenu?.collectionViewLoaded()
    }
  }
  
  fileprivate func fadeInCollectionView(_ animate: Bool) {
    guard let cv = self.currentMenu?.collectionView as? UICollectionView , animate else { return }
    cv.alpha = 0.0
    UIView.animate(withDuration: duration, delay: delayTime, options: .curveLinear,
                   animations: { _ in cv.alpha = 1.0 },
                   completion: { [weak self] _ in self?.delayTime = 0 })
    
    
  }
  
  // Mark: Create Methods
  
  func createStartScene() -> StartScene? {
    guard skView.scene == nil else {
      print("It already exists")
      return nil
    }
    
    let scene = StartScene(size: skView.bounds.size)
    if let scene = configureScene(scene) as? StartScene {
      skView.presentScene(scene)
    }
    return scene
  }
  
  
  func createMenuScene(){
    let scene = StartScene(size: skView.bounds.size)
    scene.moveCloudsUp()
    if let scene = configureScene(scene) as? StartScene {
      skView.presentScene(scene)
    }
  }
  
  func createGameScene(){
    let scene = GameScene(size:skView.bounds.size)
    if let scene = configureScene(scene) as? GameScene {
      skView.presentScene(scene)
    }
  }
  
  func createSegueScene(){
    let scene = SegueScene(size: skView.bounds.size)
    if let scene = configureScene(scene) as? SegueScene {
      skView.presentScene(scene)
    }
  }
  
  func createLevelSummaryScene() -> LevelSummaryScene{
    currentMenu?.cleanup()
    currentMenu = nil
    removeBackButton()
    removeMenuCurtain()
    let scene = LevelSummaryScene(size: skView.bounds.size)
    if let scene = configureScene(scene) as? LevelSummaryScene {
      skView.presentScene(scene)
    }
    return scene
  }
  
  
  // Helper method to get data needed for modals.
  // Called during the start of a game
  var degreeOfGlory: DegreeOfGlory = .none
  var bitsCollected: Int = 0
  func prefetchModalData(){
    guard let levelKey = NavigationManager.sharedInstance.currentLevelPath?.level.id else{ return }
    GlobalUserInteractiveQueue.async { [weak self] in
      let realm = Realm.mainInstance()
      guard let user = realm.object(ofType: User.self, forPrimaryKey: User.key),
        let data = user.levels.filter("levelKey = %@", levelKey).first else { return }
      
      self?.bitsCollected = data.bitsCollected
      self?.degreeOfGlory = DegreeOfGlory(rawValue: data.degreeOfGlory) ?? .none
    }
    
  }
}


