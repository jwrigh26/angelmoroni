//
//  WorldCell.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/14/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit



class WorldCell: GameCell {
  
  @IBOutlet weak var splashImage: UIImageView!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  var isLocked: Bool = false {
    didSet {
      if isLocked {
        splashImage.image = #imageLiteral(resourceName: "world_locked_image")
        hideBits()
        backgroundColor = GameColor.darkGray.color
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    //splashImage.backgroundColor = UIColor.red
    splashImage.make8BitFriendly()
    splashImage.contentMode = .scaleAspectFill
    splashImage.clipsToBounds = true
  }
  
  
  override func styleLabels(){
    let titleFontSize: CGFloat = Device.isPhone ? 21 : 32
    let titleFont = GameFont.pixelIntv.font(size: titleFontSize)
    
    titleLabel.font = titleFont
    titleLabel.textColor = GameColor.lightGray.color
    
    let bitLabelFontSize: CGFloat = Device.isPhone ? 14 : 21
    let bitLabelFont = GameFont.pixelIntv.font(size: bitLabelFontSize)
    let bitFontSize: CGFloat = Device.isPhone ? 17 : 24
    let bitFont = GameFont.pixelIntv.font(size: bitFontSize)
    
    scriptureBitHeaderLabel.font = bitLabelFont
    scriptureBitHeaderLabel.textColor = GameColor.lightGray.color
    scriptureBitHeaderLabel.text = CommonStrings.Bits.uppercased()
    
    scriptureBitLabel.font = bitFont
    scriptureBitLabel.textColor = GameColor.yellow.color
    scriptureBitLabel.text = ""
  }
  
  
}
