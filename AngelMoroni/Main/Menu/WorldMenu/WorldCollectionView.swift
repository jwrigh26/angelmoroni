//
//  WorldCollectionView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class WorldCollectionView: UICollectionView, CollectionView {
  
  let identifier = "WorldCell"
  
  func setupLayout(){
    
    guard let layout = self.collectionViewLayout as? CenterCellFlowLayout else { return }
    layout.frameSize = GameManager.sharedInstance.screenSize
    
    register(UINib(nibName: "WorldCell", bundle: nil), forCellWithReuseIdentifier: identifier)
  }
  
  func scrollInsetOffset(){
    let layout = collectionViewLayout as! CenterCellFlowLayout
    layout.scrollInsetOffset()
  }
  
  
  
  override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
    super.init(frame: CGRect.zero, collectionViewLayout: layout)
  }

  required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
}
