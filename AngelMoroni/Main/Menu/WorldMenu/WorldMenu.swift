//
//  StartGameViewController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit
import RealmSwift

final class WorldMenu: NSObject, Menu, UICollectionViewDataSource, UICollectionViewDelegate {
  
  
  var worlds: Results<World>!
  var collectionView: CollectionView!
  
  
  var didScrollToCell = false
  
  fileprivate var _collectionView: WorldCollectionView? {
    if let collectionView = self.collectionView as? WorldCollectionView {
      return collectionView
    }
    return nil
  }
  
  override init() {
    super.init()
    
    let realm = Realm.readOnlyInstance()
    worlds = realm.objects(World.self).sorted(byProperty: "order")
    let layout = CenterCellFlowLayout()
    collectionView = WorldCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    _collectionView?.setupLayout()
    _collectionView?.dataSource = self
    _collectionView?.delegate = self
  }

  
  func layoutSubViewForMenu(){
    _collectionView?.scrollInsetOffset()
    
  }
  
  func collectionViewLoaded(){
    scrollToWorld()
  }
  
  func onBack(){
    //print("On Back for World Menu!")
    NavigationManager.sharedInstance.transitionByGameState(.start)
    //collectionView.scrollTo(IndexPath(row: 3, section: 0))
  }
  
  func scrollToWorld(){
    if let world = NavigationManager.sharedInstance.currentWorldPath {
      collectionView.scrollTo(world.1)
      NavigationManager.sharedInstance.currentWorldPath = nil
    }
  }
  
  // MARK: Collection View Delegate

  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
    let world = worlds[indexPath.row]
    if !world.isLocked {
      //print("World is getting assigned to currentWorldPath and id = \(world.id)")
      NavigationManager.sharedInstance.currentWorldPath = (world, indexPath)
      NavigationManager.sharedInstance.transitionByGameState(.level)
    }
  }
  
  // MARK: Collection View Datasource
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return worlds.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let identifier = self.collectionView.identifier
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    let world = worlds[indexPath.row]
    let image = world.image
    
    if let cell = cell as? WorldCell {
      
      cell.title = "\(world.order + 1) - \(world.title)"
      
      let id = worlds[indexPath.row].id
      if  let dataSummary = MenuGameDataFeed.sharedInstance.worlds[id], !world.isLocked{
        cell.scripture(bits: dataSummary.collectedBits, total: dataSummary.totalBits)
        cell.degreeOfGlory = dataSummary.degreeOfGlory
        cell.splashImage.image = UIImage(named: image)
      }else{
        cell.isLocked = true
      }
      
      
    }
    return cell
  }

}
