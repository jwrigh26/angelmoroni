//
//  StartScene.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/13/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import SpriteKit

final class StartScene: BaseScene {
  
  var backgroundLayer: StartBackgroundLayer!
  
  override init(size: CGSize) {
    super.init(size: size)
    name = "StartScene"
    backgroundLayer = StartBackgroundLayer()
    backgroundLayer.zPosition = -1
    backgroundLayer.accelerateClouds = false
    addChild(backgroundLayer)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func didMove(to view: SKView){
    super.didMove(to: view)
    
    lastUpdateTime = 0
    dt = 0
    GameManager.sharedInstance.resetGameSpeedForMenu()

  }
  
  func moveCloudsUp(){
    
    guard backgroundLayer.position.y == 0 else { return }
    
    let y: CGFloat = screenSize.height * 0.2
    let endPosition = CGPoint(x: backgroundLayer.position.x, y: backgroundLayer.position.y + y)
    
    let whiteSpace = SKSpriteNode(color: GameColor.cloudGray.color, size: CGSize(width: screenSize.width, height: y))
    whiteSpace.anchorPoint = CGPoint(x: 0, y: 1)
    whiteSpace.position = endPosition
    addChild(whiteSpace)
    
    let move = SKAction.move(to: endPosition, duration: 0)
    backgroundLayer.run(move)
  }
  
  
  var lastUpdateTime: CFTimeInterval = 0
  var dt: CFTimeInterval = 0
  
  override func update(_ currentTime: TimeInterval) {
    guard enableUpdateLoop else { return }
    
    if lastUpdateTime > 0 {
      dt = currentTime - lastUpdateTime
    } else {
      dt = 0
    }
    lastUpdateTime = currentTime
    GameManager.sharedInstance.update(dt)
    backgroundLayer.update(dt)
    
  }
  
  override func willMove(from view: SKView) {
    backgroundLayer.removeAllActions()
    backgroundLayer.removeAllChildren()
  }
  
  override func onPause() {
    self.isPaused = true
    // Reset all the times!
    GameManager.sharedInstance.backgroundSpeed.resetValues()
    lastUpdateTime = 0
    dt = 0
  }
}
