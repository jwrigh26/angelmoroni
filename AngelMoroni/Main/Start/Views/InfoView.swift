//
//  InfoView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/1/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import SafariServices

final class InfoView: BaseView, SFSafariViewControllerDelegate {
  
  @IBOutlet var closeOverlay: InfoMenuOverlayControl!
  @IBOutlet var closeButton: UIImageView!
  @IBOutlet var facebookButton: InfoMenuButton!
  @IBOutlet var twitterButton: InfoMenuButton!
  @IBOutlet var aboutButton: InfoMenuButton!
  
  typealias InfoViewDismissed = () -> Void
  
  var dismissed: InfoViewDismissed?
  
  override func xibSetup() {
    loadViewFromNibAndPrepViewLayout("InfoView")
    backgroundColor = UIColor.clear
    view.backgroundColor = GameColor.cloudDarkBlue.color
    closeButton.image = PlusSign().createImage(CGSize(width: 48, height: 48))
    
    closeOverlay.onTouch = { [weak closeButton] in
      closeButton?.alpha = 0.5
    }
    
    closeOverlay.onTouchEnd = { [weak self] in
      self?.closeButton.alpha = 1.0
      self?.dismissView()
    }
    
    //closeOverlay.backgroundColor = GameColor.hartRed.color(alpha: 0.5)
  }
  
  override func layoutConstraints(){
    guard let parent = self.superview else { fatalError("parent has not been added") }
    let container = parent.layoutMarginsGuide
    self.topAnchor.constraint(equalTo: container.topAnchor, constant: 0).isActive = true
    self.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
    self.leftAnchor.constraint(equalTo: container.leftAnchor).isActive = true
    self.rightAnchor.constraint(equalTo: container.rightAnchor).isActive = true
    
  }
  
  override func setup() {
    let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissView))
    gestureRecognizer.cancelsTouchesInView = false
    gestureRecognizer.delegate = self
    view.addGestureRecognizer(gestureRecognizer)
    
    let size: CGFloat = Device.isPhone ? 28 : 48
    let font = ButtonFonts.normal(size: size).font
    
    aboutButton.setButtonTitle(title: "About", font: font)
    facebookButton.setButtonTitle(title: "FaceBook", font: font)
    twitterButton.setButtonTitle(title: "Twitter", font: font)
  }
  
  func dismissView(){

    UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
      self.view.alpha = 0.0
    }, completion: { _ in
      self.dismissed?()
    })
  }
  
  @IBAction func onFacebook(sender: Any){
    print("FB tapped")
    launchSafariVC(withURL: "https://www.facebook.com/angelcharmangame")
  }
  
  @IBAction func onTwitter(sender: Any){
    print("Twitter tapped")
    launchSafariVC(withURL: "https://twitter.com/justin_m_wright")
  }
  
  @IBAction func onAbout(sender: Any){
    print("About tapped")
    launchSafariVC(withURL: "http://angelcharman.com")
  }
  
  func launchSafariVC(withURL url: String){
    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.launchSafari), object: self, userInfo: [Notification.UrlKey : url])
  }
  
  
  
}

extension InfoView: UIGestureRecognizerDelegate {
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
    return touch.view == self.view
  }
}

fileprivate class PlusSign {
  
  let backgroundColor = GameColor.cloudDarkBlue.color
  let drawingColor = GameColor.cloudWhite.color
  let lineWidth: CGFloat = 10
  let cornerRadius: CGFloat = 2
  
  func createImage(_ size: CGSize) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
    backgroundColor.setFill()
    UIRectFill(CGRect(origin: .zero, size: size))
    var shape = UIBezierPath(roundedRect: CGRect(x: size.width/2 - lineWidth/2, y: 0, width: lineWidth, height: size.height), cornerRadius: cornerRadius)
    drawingColor.setFill()
    shape.fill()
    shape = UIBezierPath(roundedRect: CGRect(x: 0, y: size.height/2 - lineWidth/2, width: size.width, height: lineWidth), cornerRadius: cornerRadius)
    shape.fill()
    var image = UIGraphicsGetImageFromCurrentImageContext()
    image = image?.rotate(byDegrees: 45.0)
    UIGraphicsEndImageContext()
    return image!
  }
}

class InfoMenuButton: BaseTextButton {
  
  
  override func setColors() {
    super.setColors()
    textColor = GameColor.cloudWhite.color
    textHighlightColor = GameColor.cloudWhite.color(alpha: 0.3)
    textDisabledColor = GameColor.lightGray.color
    backgroundColor = UIColor.clear
  }
  
  override func setup() {
    super.setup()
    
  }
}

class InfoMenuOverlayControl: UIControl {
  
  typealias onTouchHandler = () -> Void
  typealias onTouchEndHandler = () -> Void
  
  var onTouch: onTouchHandler?
  var onTouchEnd: onTouchEndHandler?
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesBegan(touches, with: event)
    onTouch?()
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    super.touchesEnded(touches, with: event)
    onTouchEnd?()
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>?, with event: UIEvent?) {
    super.touchesCancelled(touches!, with: event)
    onTouchEnd?()
  }
  
  
}
