//
//  StartMenuButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/18/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class StartMenuButton: BaseTextButton {
  
  
  override func setColors() {
    super.setColors()
    textColor = GameColor.black.color(alpha: 0.77)
    textHighlightColor = GameColor.black.color(alpha: 0.3)
    textDisabledColor = GameColor.lightGray.color
    //backgroundColor = UIColor.red
  }
  
  override func setup() {
    super.setup()
    
    let bottom = screenSize.height * 0.1
    self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, bottom, 0)
  }
  
  override var intrinsicContentSize: CGSize {
    
    return CGSize(width: screenSize.width, height: screenSize.height)
  }
  
}
