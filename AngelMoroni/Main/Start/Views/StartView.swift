//
//  StartView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/18/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import QuartzCore

final class StartView: BaseView {
  
  @IBOutlet weak var splashImage: UIImageView!
  @IBOutlet weak var startButton: StartMenuButton!
  @IBOutlet weak var disabledView: UIView!
  
  let trophyButton = MenuActionButton(imageView: UIImageView(image: UIImage(named: "trophy")))
  let infoButton = MenuActionButton(imageView: UIImageView(image: UIImage(named: "info")))
  
  let navManager = NavigationManager.sharedInstance
  let font = Device.isPhone ? GameFont.pixelIntv.font(size: 24) : GameFont.pixelIntv.font(size: 40)
  let startTitle = "tap to start!"
  let loadingTitle = "Loading..."
  var infoView: InfoView?
  
  convenience init(parent view: UIView){
    self.init(frame: CGRect.zero)
    self.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(self)
    
    layoutConstraints()
    layoutMenuButtons()
    
    startButton.addTarget(self, action: #selector(StartView.startGame(sender:)), for: .touchUpInside)
    trophyButton.addTarget(self, action: #selector(StartView.goToAchievements(sender:)), for: .touchUpInside)
    infoButton.addTarget(self, action: #selector(StartView.goToInfo(sender:)), for: .touchUpInside)
  }
  
  func startGame(sender: AnyObject){
    NavigationManager.sharedInstance.transitionByGameState(.worldNoAnimate)
  }
  
  func goToAchievements(sender: AnyObject){
    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.presetnGKGameCenterViewController), object: self, userInfo: nil)
  }
  
  func goToInfo(sender: AnyObject){
    guard let parent = self.superview, infoView == nil else { return }
    infoView = InfoView(frame: screenBounds)
    
    infoView?.dismissed = { [weak self] in
      self?.infoView?.removeFromSuperview()
      self?.infoView = nil
    }
    
    infoView?.alpha = 0.0
    parent.addSubview(infoView!)
    infoView!.layoutConstraints()
    let prevY = infoView!.center.y
    infoView!.center.y = -screenSize.height/4
    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations:({
      self.infoView?.alpha = 1.0
      self.infoView?.center.y = prevY
    }), completion: nil)
   
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    //print("Summary View is init now!")
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func xibSetup() {
    loadViewFromNibAndPrepViewLayout("StartView")
    disabledView.backgroundColor = UIColor.clear
    startButton.setButtonTitle(title: loadingTitle.uppercased(), font: font)
    startButton.isEnabled = false
  }
  
  override func setup() {
    super.setup() // called in awakeFromNib
    
    let image = UIImage(named: "startbanner")
    splashImage.make8BitFriendly()
    splashImage.image = image
    startFlashingButton()
  }
  
  func readyToPlay(){
    //let d: CGFloat = random(min: 1, max: 3)
    //delay(Double(d)){ [weak self] in
    stopFlashingButton { [weak self] in
        self?.fadeOut()
    }
    //}
  }
  
  
  
  func fadeOut(){
    UIView.animate(withDuration: blinkTime, animations: { [weak self] in
      self?.startButton.alpha = 0.0
    }) { [weak self] _ in
      self?.fadeIn()
    }
  }
  
  func fadeIn(){
    startButton.setButtonTitle(title: startTitle, font: font)
    startButton.isEnabled = true
    UIView.animate(withDuration: 0.25){ [weak self] in
      self?.startButton.alpha = 1.0
    }
  }
  
  var blinkTime: Double = 0.5
  var isFlashing: Bool = false
  func startFlashingButton() {
    guard !isFlashing else { return }
    isFlashing = true
    startButton.alpha = 1.0
    let options: UIViewAnimationOptions = [.curveEaseInOut, .repeat, .autoreverse, .allowUserInteraction]
    UIView.animate(withDuration: blinkTime, delay: 0.25, options: options, animations: { [weak self] in
      self?.startButton.alpha = 0.0
      }, completion: nil)
  }
  
  func stopFlashingButton(callback: @escaping ()->Void){
    guard isFlashing else { return }
    isFlashing = false
    let options: UIViewAnimationOptions = [.curveEaseInOut, .beginFromCurrentState]
    UIView.animate(withDuration: blinkTime, delay: 0.0, options: options, animations: {
      self.startButton.alpha = 0.0
      }, completion: { _ in callback() })
  }
  
  func disableFlashingButtonAnimation(){
    isFlashing = false
    startButton.setButtonTitle(title: startTitle, font: font)
    startButton.isEnabled = true
    UIView.animate(withDuration: 0.0){
      self.startButton.alpha = 1.0
    }
    
  }
  
  func layoutMenuButtons(){
    trophyButton.translatesAutoresizingMaskIntoConstraints = false
    infoButton.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(trophyButton)
    view.addSubview(infoButton)
    
    //print("Trophy size \(trophyButton.intrinsicContentSize)")
    
    let views = ["trophy" : trophyButton, "info" : infoButton]
    var constraints = [NSLayoutConstraint]()
    
    let btmSpaceMultiplyFactor: CGFloat = Device.isPhone ? 0.11 : 0.106
    let bottomSpace = screenSize.height * btmSpaceMultiplyFactor
    let buttonSpace:CGFloat = Device.isPhone ? 32 : 48
    let metrics = ["bspace": bottomSpace, "space": buttonSpace]
    let vformat = "V:[info]-bspace-|"
    let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: vformat, options: [], metrics: metrics, views: views)
    constraints.append(contentsOf: vConstraints)
    
    let hformat = "H:[trophy]-space-[info]-16-|"
    let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: hformat, options: NSLayoutFormatOptions.alignAllCenterY, metrics: metrics, views: views)
    constraints.append(contentsOf: hConstraints)
    
    let dw = infoButton.intrinsicContentSize.width * 2 + 16 + buttonSpace + 16
    let dh = infoButton.intrinsicContentSize.height + 32
    let dwc = disabledView.widthAnchor.constraint(equalToConstant: dw)
    let dhc = disabledView.heightAnchor.constraint(equalToConstant: dh)
    let dv = disabledView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -(bottomSpace - 16))
    constraints.append(contentsOf: [dv, dwc, dhc])
    
    NSLayoutConstraint.activate(constraints)
  }
}
