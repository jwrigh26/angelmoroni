//
//  BackButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/27/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

class BackButton : UIControl {
  
  let buttonSize = CGSize(width: 86, height: 64) // 64,48
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  convenience init(_ screenSize: CGSize, padding: CGFloat = 8) {
    self.init(frame: CGRect.zero)
    let position = CGPoint(x: padding, y: screenSize.height - (buttonSize.height+8))
    self.frame = CGRect(x: position.x, y: position.y, width: buttonSize.width, height: buttonSize.height)
    self.sizeToFit()
  }
  
  override var intrinsicContentSize : CGSize {
    return CGSize(width: buttonSize.width, height: buttonSize.height)
  }
  
  func setUp(){
    self.backgroundColor = UIColor.orange
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    toggleButtonState(touches)
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    toggleButtonState(touches, touchEnded: true)
    sendAction(touches)
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    toggleButtonState(touches, touchEnded: true)
  }
  
  func sendAction(_ touches: Set<NSObject>){
    if touchInside(touches){
      sendActions(for: UIControlEvents.touchUpInside)
    }
  }
  
  func toggleButtonState(_ touches: Set<NSObject>, touchEnded: Bool = false){
    let touching = touchInside(touches)
    if touching && !touchEnded {
      scaleDownButton()
    }else{
      scaleUpButton()
    }
    
  }
  
  func touchInside(_ touches: Set<NSObject>)-> Bool {
    if let touch: UITouch = touches.first as? UITouch {
      let touchLocation: CGPoint = touch.location(in: self)
      if self.bounds.contains(touchLocation){
        return true
      }
      //if CGRectContainsPoint(view.convertRect(view.bounds, toCoordinateSpace: self), touchLocation){}
    }
    return false
  }
  
  func scaleDownButton(){
    self.transform = CGAffineTransform(scaleX: 0.90, y: 0.90)
  }
  
  func scaleUpButton(){
    self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
  }
  
  
  

}
