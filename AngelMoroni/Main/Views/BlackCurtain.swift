//
//  BlackCurtain.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/16/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class BlackCurtain: CALayer {
  
  override init(layer: Any) {
    super.init(layer: layer)
    setup()
  }
  
  convenience init(layer: AnyObject, opacity: Float){
    self.init(layer: layer)
    setup(opacity)
  }

  required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
  }
  
  fileprivate func setup(_ opacity: Float = 0.0){
    backgroundColor = UIColor.black.cgColor
    frame = UIScreen.main.bounds
    self.opacity = opacity
  }
}
