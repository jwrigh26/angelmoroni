//
//  GameCell.swift
//  AngelMoroni
//
//  Created by Justin Wright on 1/16/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class GameCell: UICollectionViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var scriptureBitHeaderLabel: UILabel!
  @IBOutlet weak var scriptureBitLabel: UILabel!
  @IBOutlet weak var starImageView: UIImageView!
  @IBOutlet weak var moonImageView: UIImageView!
  @IBOutlet weak var sunImageView: UIImageView!
  @IBOutlet weak var degreesOfGloryContainer: UIView!
  
  var title: String?{
    didSet {
      titleLabel.text = title
    }
  }
  
  var degreeOfGlory: DegreeOfGlory? {
    didSet {
      guard let value = degreeOfGlory else { return }
      resetImages()
      switch value {
      case .star:
        starImageView.image = #imageLiteral(resourceName: "star")
      case .moon:
        starImageView.image = #imageLiteral(resourceName: "star")
        moonImageView.image = #imageLiteral(resourceName: "moon")
      case .sun:
        starImageView.image = #imageLiteral(resourceName: "star")
        moonImageView.image = #imageLiteral(resourceName: "moon")
        sunImageView.image = #imageLiteral(resourceName: "sun")
      default:
        resetImages()
        
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    backgroundColor = GameColor.black.color
    layer.borderColor = GameColor.darkGray.color.cgColor
    layer.borderWidth = Device.isPhone ? 8 : 12
    styleLabels()
    resetImages()
  }
  
  func styleLabels(){
    
  }
  
  func scripture(bits: Int, total: Int){
    let b = bitsToString(bits)
    let t = bitsToString(total)
    scriptureBitLabel.text = "\(b)/\(t)"
  }
  
  func hideBits(){
    scriptureBitLabel.isHidden = true
    scriptureBitHeaderLabel.isHidden = true
    degreesOfGloryContainer.isHidden = true
  }
  
  func resetImages(){
    starImageView.make8BitFriendly()
    moonImageView.make8BitFriendly()
    sunImageView.make8BitFriendly()
    starImageView.image = UIImage(named: "star_black")
    moonImageView.image = UIImage(named: "moon_black")
    sunImageView.image = UIImage(named: "sun_black")
  }
  
  fileprivate func bitsToString(_ bits: Int) -> String{
    if bits >= 100 {
      return String(format: "%03d", bits)
    }
    return String(format: "%02d", bits)
  }
}
