//
//  ImageButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/24/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

class ImageButton: UIControl, CustomTouch {
  
  var imageView: UIImageView!
  
  override var isEnabled: Bool {
    didSet {
      isUserInteractionEnabled = isEnabled
    }
  }
  
  override var isUserInteractionEnabled: Bool {
    didSet {
      displayDisabledState()
    }
  }
  
  override init(frame: CGRect){
    super.init(frame: frame)
  }
  
  init(imageView: UIImageView){
    //print("ImageView Bounds = \(imageView.bounds)")
    super.init(frame: imageView.bounds)
    self.addSubview(imageView)
    self.imageView = imageView
    self.imageView.backgroundColor = UIColor.clear
    self.imageView.layer.magnificationFilter = kCAFilterNearest
    self.imageView.contentMode = .scaleAspectFit
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func touchInside(_ touches: Set<NSObject>) -> Bool {
    if let touch: UITouch = touches.first as? UITouch {
      let touchLocation: CGPoint = touch.location(in: self)
      if self.bounds.contains(touchLocation) {
        return true
      }
    }
    return false
  }
  
  override var intrinsicContentSize: CGSize {
    return self.imageView.bounds.size
  }
  
  
  func displayDisabledState(){
    print("Please override me with disabled state you want for the button")
  }
  
  func displayDownState(){
    print("Please override me with the down state you want for the button")
  }
  
  func displayUpState(){
    print("Please override me with the up state you want for the button")
  }
  
  func sendAction(_ touches: Set<NSObject>){
    if touchInside(touches) {
      sendActions(for: UIControlEvents.touchUpInside)
    }
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    displayDownState()
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    displayUpState()
    sendAction(touches)
  }
  
  override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
    displayUpState()
  }
  
}


protocol CustomTouch: class {
  
  func sendAction(_ touches: Set<NSObject>)
  func touchInside(_ touches: Set<NSObject>) -> Bool
  
}
