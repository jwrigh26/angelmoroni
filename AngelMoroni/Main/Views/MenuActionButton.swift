//
//  MenuActionButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/24/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

final class MenuActionButton: ImageButton {
  
  let downStateAlpha: CGFloat = 0.77
  let upStateAlpha: CGFloat = 0.3
  let disabledStateAlapha: CGFloat = 0.1
  
  override var isUserInteractionEnabled: Bool {
    didSet {
      alpha = isUserInteractionEnabled ? downStateAlpha : disabledStateAlapha
      displayDisabledState()
    }
  }
  
  override init(imageView: UIImageView){
    super.init(imageView: imageView)
    self.imageView.alpha = upStateAlpha
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  override func displayDownState(){
    self.imageView.alpha = downStateAlpha
  }
  
  override func displayUpState(){
    self.imageView.alpha = upStateAlpha
  }
  
  override func displayDisabledState() {
   
  }
}
