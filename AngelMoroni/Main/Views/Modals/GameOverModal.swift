//
//  PausedMenu.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import GameKit

final class GameOverModal: BaseModal, GameModalProtocol {
  
  static var numberOfTries:Int = 0
  
  override func xibSetup() {
    loadViewFromNibAndPrepViewLayout("GameOverModal")
  }
  
  override func setup() {
    super.setup()
    styleLabels()
  }
  
  
  @IBAction func onRetry(_ sender: AnyObject) {
    navManager.transitionByGameState(.retry)
    recordKeepTryingAchievement()
  }
  
  @IBAction func onMenu(_ sender: AnyObject) {
    // TODO: Do any saving
    navManager.transitionByGameState(.returnToMenu)
  }
  
  func styleLabels(){
    //let color = UIColor(netHex: GameColor.LightYellow.rawValue)
    
    if device.isLargeScreen {
      scriptureLabel.font = GameFont.pixelIntv.font(size: 36)
    }
  }
  
  func recordKeepTryingAchievement(){
    if GameOverModal.numberOfTries >= 3 {
      GameOverModal.numberOfTries = 0
    }
    
    GameOverModal.numberOfTries += 1
    let achievements = [AchievementsHelper.keepTryingAchievement(noOfTries: GameOverModal.numberOfTries)]
    GameKitHelper.sharedInstance.reportAchievements(achievements: achievements)
  }
  
}
