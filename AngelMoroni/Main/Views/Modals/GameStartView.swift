//
//  GameStartView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/26/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit


final class GameStartView: BaseView {
  
  @IBOutlet weak var startLabel: UILabel!
  
  var title: String? {
    didSet {
      startLabel.text = title
    }
  }
  
  override func xibSetup() {
    loadViewFromNibAndPrepViewLayout("GameStartView")
  }
  
  convenience init(parent view: UIView){
    self.init(frame: CGRect.zero)
    self.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(self)
    layoutConstraints()
    
  }
  
  override func setup() {
    super.setup()
    view.backgroundColor = UIColor(netHex: GameColor.black.rawValue)
    startLabel.textColor = UIColor(netHex: GameColor.cloudWhite.rawValue)
    styleLabels()
    
  }
  
  func styleLabels(){
    //let color = UIColor(netHex: GameColor.LightYellow.rawValue)
    let device = Device()
    if device.isLargeScreen {
      startLabel.font = GameFont.pixelIntv.font(size: 36)
    }
  }
  
  override func layoutConstraints(){
    guard let parent = self.superview else { fatalError("parent has not been added") }
    let container = UILayoutGuide()
    parent.addLayoutGuide(container)
    
    container.widthAnchor.constraint(equalToConstant: screenSize.width).isActive = true
    container.heightAnchor.constraint(equalToConstant: screenSize.height).isActive = true
    
    self.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
    self.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
    self.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
    self.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
  }
  
}
