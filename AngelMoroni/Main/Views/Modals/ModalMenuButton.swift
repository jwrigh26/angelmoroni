//
//  PauseMenuButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import QuartzCore

final class ModalMenuButton : UIButton {
  
  let darkColor = UIColor(netHex: GameColor.medGray.rawValue)
  let lightColor = UIColor(netHex: GameColor.lightGray.rawValue)
  
  
  let textLightColor = UIColor(netHex: GameColor.cloudWhite.rawValue)
  let textDarkColor = UIColor(netHex: GameColor.darkBlue.rawValue)
  
  override var isHighlighted: Bool {
    willSet(value) {
      if value {
        styleDownState()
      }else{
        styleDefaultState()
      }
      
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override func layoutSubviews() {
   
    super.layoutSubviews()
    
    
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    styleDefaultState()
    layer.borderWidth = 2.0
    titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
    contentEdgeInsets = UIEdgeInsetsMake(8, 10, 8, 10)
    let device = Device()
    if device.isSmallScreen {
      let font = GameFont.pixelIntv.font(size: 17)
      titleLabel?.font = font
    }else if device.isLargeScreen {
      let font = GameFont.pixelIntv.font(size: 24)
      titleLabel?.font = font
    }
  }
  
  fileprivate func toggleButtonState(){
    
  }
  
  fileprivate func styleDefaultState(){
    layer.borderColor = darkColor.cgColor
    backgroundColor = lightColor
    titleLabel?.textColor = UIColor.white
  }
  
  fileprivate func styleDownState(){
    layer.backgroundColor = darkColor.cgColor
    backgroundColor = darkColor
  }
  
  
}
