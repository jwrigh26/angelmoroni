//
//  PausedMenu.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import RealmSwift

final class PauseModal: BaseModal, GameModalProtocol {
  
  @IBOutlet weak var bitsTitleLabel: UILabel!
  @IBOutlet weak var bitsCounterLabel: UILabel!
  @IBOutlet weak var starImageView: UIImageView!
  @IBOutlet weak var moonImageView: UIImageView!
  @IBOutlet weak var sunImageView: UIImageView!
  
  var bits:Int = 0
  var degreeOfGlory = DegreeOfGlory.none
  
  var totalBits: String {
    let bits = GameManager.sharedInstance.scriptureMaster?.totalBits ?? 0
    return bitsToString(bits)
  }
  
  convenience init(parent view: UIView, bits: Int, degree: DegreeOfGlory){
    self.init(frame: CGRect.zero)
    self.bits = bits
    self.degreeOfGlory = degree
    
    self.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(self)
    layoutConstraints()
    
    setupNoValues()
    assignDegreeOfGlory(degree: degreeOfGlory)
    bitsCounterLabel.text = "\(bitsToString(bits))/\(totalBits)"
    
  }
  
  override func xibSetup() {
    loadViewFromNibAndPrepViewLayout("PauseModal")
  }
  
  override func setup() {
    super.setup()
    styleLabels()
  }
  
  @IBAction func onResume(_ sender: AnyObject) {

    navManager.transitionByGameState(.resume)
    
  }
  
  @IBAction func onRetry(_ sender: AnyObject) {
    navManager.transitionByGameState(.retry)
  }
  
  @IBAction func onMenu(_ sender: AnyObject) {
    // TODO: Do any saving
    navManager.transitionByGameState(.returnToMenu)
  }
  
  func styleLabels(){
    let color = GameColor.lightYellow.color
    
    bitsTitleLabel.textColor = color
    
    if device.isLargeScreen {
      var font = GameFont.pixelIntv.font(size: 21)
      
      bitsTitleLabel?.font = font
      bitsCounterLabel?.font = font
      
      
      font = GameFont.pixelIntv.font(size: 36)
      scriptureLabel.font = font
    }
  }
  
  func setupNoValues(){
    bitsTitleLabel.text = CommonStrings.BestBits.uppercased()
    bitsCounterLabel.text = "00/\(totalBits)"
    starImageView.make8BitFriendly()
    moonImageView.make8BitFriendly()
    sunImageView.make8BitFriendly()
    starImageView.image = UIImage(named: "star_black")
    moonImageView.image = UIImage(named: "moon_black")
    sunImageView.image = UIImage(named: "sun_black")
  }
  
  fileprivate func bitsToString(_ bits: Int) -> String{
    //let totalBits = GameManager.sharedInstance.scriptureMaster?.totalBits ?? 0
    if bits >= 100 {
      return String(format: "%03d", bits)
    }
    return String(format: "%02d", bits)
  }
  
  fileprivate func assignDegreeOfGlory(degree: DegreeOfGlory){
    
    let imageViews = [starImageView, moonImageView, sunImageView]
    var array = ["star", "moon", "sun"]
    
    for (index, imageView) in imageViews.enumerated(){
      imageView?.image = UIImage(named: array[index]+"_black")
    }
    
    var index = degree.rawValue - 1
    while index >= 0 {
      if imageViews.indices.contains(index) && array.indices.contains(index) {
        imageViews[index]?.image = UIImage(named: array[index])
      }
      index -= 1
    }
  }
  
  
  
}


