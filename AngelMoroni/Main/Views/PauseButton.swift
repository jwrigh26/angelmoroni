//
//  PauseButton.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/20/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

final class PauseButton: ImageButton {
  
  static let downImage = "pausebutton-down"
  static  let upImage = "pausebutton-up"
  
  override var isUserInteractionEnabled: Bool {
    didSet {
      alpha = isUserInteractionEnabled ? 0.5 : 1.0
      displayDisabledState()
    }
  }
  
  convenience init(){
    let imageView = UIImageView(image: UIImage(named: PauseButton.upImage), highlightedImage: UIImage(named: PauseButton.downImage))
    self.init(imageView: imageView)
  }
  
  
  override func displayDownState(){
    self.imageView.isHighlighted = true
  }
  
  override func displayUpState(){
    self.imageView.isHighlighted = false
  }
  
  override func displayDisabledState() {
    self.imageView.isHighlighted = false
  }
  
}





