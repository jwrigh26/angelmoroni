//
//  World1EnemyFactory.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

enum EnemySize: Int {
  case small, med, large
}

enum SpriteType {
  case undefined, enemy, scripture
}

protocol SpriteFactory {
  
  func createScriptureBit(_ bit: String?) -> ScriptureBitNode?
  func createEnemyPlatform(_ size: EnemySize) -> EnemyPlatformSprite
  func generateSmallEnemy(_ linkingUUID: String) -> EnemySprite
  func generateMedEnemy(_ linkingUUID: String) -> EnemySprite
  func generateLargeEnemy(_ linkingUUID: String) -> EnemySprite
  func createRocket(_ uuid: String) -> Rocket
}

extension SpriteFactory{
  
  func createScriptureBit(_ bit: String?) -> ScriptureBitNode?{
    guard let bit = bit else { return nil }
    let node = ScriptureBitNode(position: CGPoint.zero, text: bit)
    return node
  }
}




