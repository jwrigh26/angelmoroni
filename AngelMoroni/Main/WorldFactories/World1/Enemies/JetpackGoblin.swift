//
//  Imp.swift
//  AngelMoroni
//
//  Created by Justin Wright on 7/8/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit



final class JetpackGoblin: EnemySprite, Rocket {
  
  let flying = SKAction.repeatForever(SKAction.animate(with: GameSprite.gameSheet.JetPackGoblin(), timePerFrame: 0.16, resize: true, restore: false))
  var linkingUUID: String?
  var prevPosition: CGPoint = CGPoint.zero
  var alert = EnemyAlert.makeAlert()
  var particles: [Particle]  = [Particle]()
  var launched: Bool = false
  
  convenience init(linkingUUID: String){
    let creator = World1EnemyCreator()
    let texture = creator.jetpackGoblinTexture
    self.init(texture: texture, position: CGPoint.zero)
    self.linkingUUID = linkingUUID
    //anchorPoint = CGPointMake(0.5, 0.5)
    prevPosition = position
    position = CGPoint(x: screenSize.width + frame.width * 2, y: 0)
    characterState = .flying
    createParticles()
    //traceHitBox()
    
  }
  
  override func boundingBox() -> CGRect {
    let x = frame.origin.x + (frame.width * 0.3) / 2
    let y = frame.origin.y
    let width = frame.width * 0.7
    let height = frame.height * 0.7
    return CGRect(x: x, y: y, width: width, height: height)
  }
  
  
  
  func hitTest(_ playerHitBox: CGRect){
    if playerHitBox.intersects(hitBox){
      let _ = onHit()
    }
  }
  
  
  
  override func traceHitBox() {
   let rect = makeTraceShape(boundingBox())
    let x = floor(rect.frame.maxX) + (rect.frame.width * 0.3) / 2
    let y = floor(rect.frame.maxY)
    rect.position = CGPoint(x: x , y: y)
    addChild(rect)
  }
  
  override func getCharacterStateSKAction(_ state: CharacterState) -> stateSKAction {
    var action: SKAction?
    switch(state) {
    case .flying:
      action = flying
    default:
      action = flying
    }
    return (action, "currentEnemy\(self)Action")
  }
  
  
  func blastOff(_ position: CGPoint){
    guard !launched else { return }
    launched = true
    removeAllActions()
    characterState = .flying
    
    // Decide position
    //let y = random(min: frame.height/2, max: screenBounds.maxY - (frame.height + 32))
    let y = position.y
    self.position = CGPoint(x: screenBounds.width + frame.width/2, y: y)
    zPosition = 3
    
    let oscillate = SKAction.oscillation(amplitude: 5, timePeriod: 0.5, midPoint: self.position)
    //let rotateY = SKAction.rotateOnArc(node: &self, timePeriod: 0.25)
    let move = SKAction.moveTo(x: -(screenBounds.width + frame.width), duration: 2)
    move.timingMode = SKActionTimingMode.easeIn
    let block = SKAction.afterDelay(3){ [weak self] in
      self?.postRemoveRocket()
      self?.cleanup()
    }
    
    runParticles()
    run(SKAction.repeatForever(oscillate))
    //runAction(SKAction.repeatActionForever(rotateY))
    run(move)
    run(block)
  }
  
  
  func createParticles(){
    guard let p1 = Particle(fileNamed: GameManager.sharedInstance.jetPuffParticle),
      let p2 = Particle(fileNamed: GameManager.sharedInstance.jetSmokeParticle) else { return }
    
    let x: CGFloat = Device.isPhone ? self.frame.width/2 - 5 : boundingBox().width/2 - 20
    let y: CGFloat = Device.isPhone ? self.frame.height/2 - 5 : boundingBox().height/2 - 10
    
    p1.position = CGPoint(x: x, y: y)
    p1.particleTexture?.filteringMode = .nearest
    
    p2.position = p1.position
    p2.particleTexture?.filteringMode = .nearest
    
    p1.zPosition = -1
    p2.zPosition = 0
    
    particles.append(p1)
    particles.append(p2)
  }
  
  func runParticles(){
    particles.forEach{
      $0.resetSimulation()
      if $0.parent == nil {
        addChild($0)
      }
    }
    
    if let parent = self.parent as? GameLayer, let scene = parent.scene as? GameScene , particles.indices.contains(1) {
      particles[1].targetNode = scene.particleLayer
    }
    
  }
  
  func stopParticles(){
    particles.forEach{
      $0.cleanup()
    }
  }
  
  override func cleanup() {
    stopParticles()
    removeAllActions()
    GameManager.sharedInstance.gameQueue.sync(flags: .barrier, execute: { [weak self] in
      self?.removeFromParent()
    })
    
  }
  
}

