//
//  Imp.swift
//  AngelMoroni
//
//  Created by Justin Wright on 7/8/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

final class Oni: PlatformLinkableEnemySprite {
  
  override var idleAction: SKAction? {
    return OniAction.breathing
  }
  
  
  override var randomActionArray: [SKAction]? {
    return [OniAction.blink, OniAction.breathing, OniAction.idle]
  }
  
  convenience init(linkingUUID: String){
    let creator = World1EnemyCreator()
    let texture = creator.oniTexture
    self.init(texture: texture, position: CGPoint.zero)
    self.linkingUUID = linkingUUID
    self.posOffset = CGPoint(x: 0, y: -4)
    //traceHitBox()
  }
  
  override func boundingBox() -> CGRect {
    let x = frame.origin.x + frame.width / 2
    let y = frame.origin.y
    let width = frame.width * 0.6
    let height = frame.height * 0.9
    
    return CGRect(x: x, y: y, width: width, height: height)
  }
  
  override func traceHitBox() {
    let b = boundingBox()
    let rect = makeTraceShape(boundingBox())
    let x = floor(b.origin.x)
    let y = floor(rect.frame.maxY)
    rect.position = CGPoint(x: x , y: y)
    addChild(rect)
  }
  
}
