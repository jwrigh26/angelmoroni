//
//  Word1PlatformSprite.swift
//  AngelMoroni
//
//  Created by Justin Wright on 7/3/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

final class World1PlatformSprite: EnemyPlatformSprite {
  
  convenience init(platform: Platform, position: CGPoint){
    let creator = World1PlatformCreator()
    let texture = creator.generateTexture(platform)
    self.init(texture: texture, position: position)
    self.platform = platform
    //traceHitBox()
  }
  
  override func traceHitBox() {
    let rect = makeTraceShape(frame)
    let x = floor(rect.frame.maxX)
    let y = floor(rect.frame.maxY)
    rect.position = CGPoint(x: x, y: y)
    addChild(rect)
  }
  
}


struct World1PlatformCreator {
  
  func generateTexture(_ platform: Platform) -> SKTexture {
    return randomPlatform(platform, 1, 5)
  }
  
  let randomPlatform = { (platform: Platform, min: Int, max: Int) -> SKTexture in
    let sheet = GameManager.sharedInstance.gameSheet
    let r = random(min: min, max: max)
    // TODO: TEMP Change
    let name = "Platforms/world_1_platform_\(platform.rawValue)-\(r)"
//    let name = "Platforms/world_1_platform_\(3)-\(1)"
//    print("Platform Generator size : \(platform) and file \(name)")
    
    return sheet.textureAtlas.textureNamed(name)
  }
  
}
