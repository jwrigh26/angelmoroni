//
//  World1EnemyFactory.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/28/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit



struct World1SpriteFactory: SpriteFactory{
  
  func createRocket(_ uuid: String) -> Rocket {
    return JetpackGoblin(linkingUUID: uuid)
  }
  
  func createEnemyPlatform(_ size: EnemySize) -> EnemyPlatformSprite{
    let r = random(min: Platform.small.rawValue, max: Platform.xLarge.rawValue)
    let p = Platform(rawValue: r)
    let platform = World1PlatformSprite(platform: p!, position: CGPoint.zero)
    let enemiesAllowed = platform.calculateNumberOfEnemiesAllowed(size)
    platform.enemySize = size
    
    for e in enemiesAllowed {
      var enemy: EnemySprite?
      
      // enemy = generateLargeEnemy(platform.uuid)
      
      switch e {
      case .small:
        enemy = generateSmallEnemy(platform.uuid)
      case .med:
        enemy = generateMedEnemy(platform.uuid)
      case .large:
        enemy = generateLargeEnemy(platform.uuid)
      }
      
      platform.linkedEnemies?.append(enemy!)
    }
    
    return platform
  }
  
  func generateSmallEnemy(_ linkingUUID: String) -> EnemySprite {
    let r = random(min: 1, max: 10)
    if r <= 3 {
      return Goblin(linkingUUID: linkingUUID)
    }
    return LittleImp(linkingUUID: linkingUUID)
    
  }
  
  func generateMedEnemy(_ linkingUUID: String) -> EnemySprite {
    let r = random(min: 1, max: 100)
    switch r {
    case 1...14:
      return SlurpeeImp(linkingUUID: linkingUUID)
    case 15...40:
      return Skeleton(linkingUUID: linkingUUID)
    default:
      return Imp(linkingUUID: linkingUUID)
    }
    //return Skeleton(linkingUUID: linkingUUID)
  }
  
  func generateLargeEnemy(_ linkingUUID: String) -> EnemySprite {
    return Oni(linkingUUID: linkingUUID)
  }
  
  
}



struct World1EnemyCreator {
  
  let sheet = GameManager.sharedInstance.gameSheet
  
  var littleImpTexture: SKTexture {
    return sheet.Enemies_LittleImp_littlimp_01()
  }
  
  var impTexture: SKTexture {
    return sheet.Enemies_Imp_medimp_01()
  }
  
  var slurpeeImpTexture: SKTexture {
    return sheet.Enemies_SlurpeeImp_slurpee_imp_01()
  }
  
  var goblinTexture: SKTexture {
    return sheet.Enemies_Goblin_goblin_01()
  }
  
  var oniTexture: SKTexture {
    return sheet.Enemies_Oni_oni_01()
  }
  
  var skeletonTexture: SKTexture {
    return sheet.Enemies_skeleton_skeleton_01()
  }
  
  var jetpackGoblinTexture: SKTexture {
    return sheet.Enemies_JetpackGoblin_jetpack_goblin_01()
  }
}





