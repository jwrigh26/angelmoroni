//
//  NSNotificationProxy.swift
//  AngelMoroni
//
//  Created by Justin Wright on 2/7/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation

typealias NotificationHandler = (Foundation.Notification) -> ()

class NSNotificationProxy {
  
  var handler: NotificationHandler
  var notificationKey: String
  var object: AnyObject?
  
  init(notificationKey: String, handler: @escaping NotificationHandler) {
    self.handler = handler
    self.notificationKey = notificationKey
    self.start()
  }
  
  convenience init(notificationKey: String, object: AnyObject, handler: @escaping NotificationHandler){
    self.init(notificationKey: notificationKey, handler: handler)
    self.object = object
  }
  
  deinit {
    stop()
  }
  
  
  dynamic func start(){
    NotificationCenter.default.addObserver(self, selector: #selector(NSNotificationProxy.notificationHandler(_:)), name: NSNotification.Name(rawValue: notificationKey), object: object)
  }
  
  dynamic func stop(){
    NotificationCenter.default.removeObserver(self)
  }
  
  dynamic func notificationHandler(_ notification: Foundation.Notification){
    handler(notification)
  }
}



protocol NotificationListener {
  
  
  func listenForNotifications()
  
}
