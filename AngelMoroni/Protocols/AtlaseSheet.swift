//
//  AtalasSheet.swift
//  AngelMoroni
//
//  Created by Justin Wright on 2/14/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


protocol AtlaseSheet: class {
  var textureAtlas: SKTextureAtlas! { get set }
  init()
}

extension AtlaseSheet {
  
  init(textureAtlas: SKTextureAtlas){
    self.init()
    self.textureAtlas = textureAtlas
    
  }
  
}