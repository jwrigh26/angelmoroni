//
//  CleanupProtocol.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/15/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit


protocol CleanupProtocol: class {
  func cleanup()
  func removeFromParent()
}


protocol GameLoopReady: class {
  func update(_ dt: CFTimeInterval)
  func didEvaluateActions()
  func didFinishUpdate()
}
