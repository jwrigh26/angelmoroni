//
//  CustomXibView.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

protocol CustomXibView: class {
  
  var view: UIView { get set }
  
  func xibSetup()
  func prepViewLayout()
  func loadViewFromNib(_ nibName: String) -> UIView
  func loadViewFromNibAndPrepViewLayout(_ nibName: String)
}

extension CustomXibView {
  
  func loadViewFromNib(_ nibName: String) -> UIView {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: nibName, bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
  }
  
  func prepViewLayout(){
    guard let me = self as? UIView else { return }
    view.frame = me.bounds
    view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, .flexibleHeight]
    me.addSubview(view)
  }
  
  func loadViewFromNibAndPrepViewLayout(_ nibName: String) {
    view = loadViewFromNib(nibName)
    prepViewLayout()
  }
  
}




