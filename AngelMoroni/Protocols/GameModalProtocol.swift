//
//  GameModalProtocol.swift
//  AngelMoroni
//
//  Created by Justin Wright on 3/26/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

protocol GameModalProtocol {
  var scriptureLabel: UILabel! { get }
  var device: Device { get }
  func styleLabels()
  
}

extension GameModalProtocol {
  
  
}
