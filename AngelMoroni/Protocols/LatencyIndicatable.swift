//
//  LatencyIndicatable.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/18/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

protocol LatencyIndicatable {
  
  var indicator: UIActivityIndicatorView? { get }
  
  func startIndicator()
  func stopIndicator()
  func createIndicator(style: UIActivityIndicatorViewStyle) -> UIActivityIndicatorView?
  func layoutIndicator(indicator: UIActivityIndicatorView?)
  func addIndicator(view: UIView)
}

extension LatencyIndicatable {
  
  func startStatusBarIndicator(){
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }
  
  func stopStatusBarIndicator(){
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
  
  
  func startIndicator(){
    if let i = indicator { i.startAnimating() }
    startStatusBarIndicator()
  }
  
  func stopIndicator(){
    if let i = indicator { i.stopAnimating() }
    stopStatusBarIndicator()
  }
  
  
  func addIndicator(view: UIView){
    if let indicator = self.indicator {
      view.addSubview(indicator)
      layoutIndicator(indicator: indicator)
    }
  }
  
  func createIndicator(style: UIActivityIndicatorViewStyle)-> UIActivityIndicatorView? {
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: style)
    indicator.alpha = 1.0
    indicator.hidesWhenStopped = true
    //indicator.stopAnimating()
    indicator.sizeToFit()
    return indicator
  }
  
  func layoutIndicator(indicator: UIActivityIndicatorView?){
    
    if let indicator = indicator, let parent = indicator.superview {
      indicator.translatesAutoresizingMaskIntoConstraints = false
      
      let cx = NSLayoutConstraint(
        item: indicator,
        attribute: .centerX,
        relatedBy: .equal,
        toItem: parent,
        attribute: .centerX,
        multiplier: 1.0,
        constant: 0.0)
      
      let cy = NSLayoutConstraint(
        item: indicator,
        attribute: NSLayoutAttribute.centerY,
        relatedBy: .equal,
        toItem: parent,
        attribute: .centerY,
        multiplier: 1.0,
        constant: 0.0)
      
      NSLayoutConstraint.activate([cx, cy])
      parent.setNeedsDisplay()
    }
  }
}
