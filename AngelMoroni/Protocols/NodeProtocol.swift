//
//  GameNode.swift
//  HelloWorldSwift
//
//  Created by Wright, Justin M on 8/29/15.
//  Copyright © 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

//var printedBoundingBox: Bool = false

protocol NodeProtocol {
  
  var nodeBounds: CGRect! { get set }
  var nodeFrame: CGRect { get }
  var nodePosition: CGPoint! { get set}
  var frame: CGRect { get }
  func convertChildPosition(_ node: SKNode) -> CGPoint
  func convertChildBoundingBox(_ sprite: SpriteProtocol?, point: CGPoint) -> CGRect
}


extension NodeProtocol {
  
  // MARK: Utils
  var nodeFrame: CGRect {
    return CGRect(x: frame.origin.x, y: frame.origin.y, width: nodeBounds.width, height: nodeBounds.height)
  }
  
  func convertChildPosition(_ node: SKNode) -> CGPoint {
    guard let me = self as? SKNode else { return CGPoint.zero }
    //let convertPoint = me.convertPoint(me.position, fromNode: n)
    let pos = node.position
    let convertPoint = CGPoint(x: me.position.x + pos.x, y: me.position.y + pos.y)
    return convertPoint
  }
  
  func convertChildBoundingBox(_ sprite: SpriteProtocol?, point: CGPoint) -> CGRect {
    guard let sprite = sprite else { return CGRect.zero }
    
    let frame = sprite.frame
    
    let box = sprite.boundingBox()
    let xDiff = box.origin.x - frame.origin.x
    let yDiff = box.origin.y - frame.origin.y
    let newFrame = CGRect(x: point.x + xDiff, y: point.y + yDiff, width: box.width, height: box.height)
    
//    if !printedBoundingBox {
//      printedBoundingBox = true
//      print("Frame \(frame)")
//      print("BoundingBox = \(box)")
//      print("XDiff = \(xDiff) and yDiff \(yDiff)")
//      print("Point \(point)")
//      print("newFrame \(newFrame)")
//      print("")
//    }
    
    
    
    return newFrame
  }
  
}
