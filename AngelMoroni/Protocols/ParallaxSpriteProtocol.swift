//
//  ParallaxSpriteProtocol.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/14/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import SpriteKit

enum ParallaxPosition: Int {
  case back = 1, middleBack, middleFront, front
}

protocol ParallaxSpriteProtocol : class {
  
  static func generateFileName(_ parallaxPosition: ParallaxPosition)-> String
  
  var scrollOrder: Int { get }
  var linkedSprite: SKSpriteNode? { get set }
  var velocity: CGPoint { get set }
  var offset: CGFloat { get }
  var yPos: CGFloat { get }
  var position: CGPoint { get set }
  var size: CGSize { get set }
  var parallxPosition:ParallaxPosition { get }
  
  var scrollSpeed: CGFloat { get }
  
  func setScrollPosition()
  func resetScrollPosition(_ complete: ( ()-> Void)? )
  func move(_ dt: CFTimeInterval, velocity: CGPoint)
  
  
}

extension ParallaxSpriteProtocol {
  
  func setScrollPosition(){
    guard scrollOrder > 0 else { position = CGPoint(x: 0, y: yPos); return }
    let x:Int = scrollOrder * Int(size.width - offset)
    position = CGPoint(x: CGFloat(x), y: yPos)
  }
  
  func resetScrollPosition(_ complete: ( ()-> Void)? ){
    guard let linkedSprite = linkedSprite
      , self.position.x < -(self.size.width) else { return }
    
    let linkedPos = linkedSprite.position
    let lmx = linkedSprite.frame.maxX
    position = linkedPos + CGPoint(x: lmx - offset, y: position.y)
    complete?()
  }
  
  func move(_ dt: CFTimeInterval, velocity: CGPoint){
    let amountToMove = velocity * CGFloat(dt)
    
    let lx = linkedSprite!.position.x
    let lmx = linkedSprite!.frame.maxX - offset
    let sw = size.width
    
    
    if (lx <= sw) {
      if scrollOrder == 0 {
        position.x = lmx - amountToMove.x
        return
      }
      position.x = lmx
    }else{
      position -= amountToMove // Moves from right to left
    }
  }
  
}


