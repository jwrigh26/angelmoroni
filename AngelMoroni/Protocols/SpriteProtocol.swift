//
//  GameSprite.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/20/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit


func == (lhs: SpriteProtocol, rhs: SpriteProtocol) -> Bool {
  return lhs.hashValue == rhs.hashValue
}

protocol SpriteProtocol: class  {

  var hash:Int { get }
  var hashValue:Int { get }
  
  var screenSize: CGSize { get set }
  var dirtyNode: SKSpriteNode! { get }
  var position: CGPoint { get set }
  var frame: CGRect { get }

  func addToParent(_ parent: SKNode)
  func boundingBox()-> CGRect
  func didEvaluateActions()
  func update(_ dt: CFTimeInterval)
  
  
}

extension SpriteProtocol where Self: SKNode {

  
  func addToParent(_ parent: SKNode){
      parent.addChild(self)
  }
}






protocol Hitable {
  var hitBox: CGRect { get }
  var hit: Bool { get set }
  func onHit() -> Bool
  
}

protocol Traceable {
  func traceHitBox()
  func makeTraceShape(_ rect: CGRect) -> SKShapeNode
}

extension Traceable {
  
  func makeTraceShape(_ rect: CGRect) -> SKShapeNode {
    let scale = Device.Scale
    let shape = SKShapeNode(rectOf: CGSize(width: rect.width/scale, height: rect.height/scale))
    shape.drawDebugBox(UIColor.red)
    return shape
  }
  
}


