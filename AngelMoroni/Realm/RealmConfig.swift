//
//  RealmConfig.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/27/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift

enum RealmConfig {
  
  static let defaultUserName = "defaultuser_v1.0"
  
  fileprivate static let staticConfig =  Realm.Configuration(
    fileURL: Bundle.main.url(forResource: "static_v1.0", withExtension: "realm"),
    readOnly: true,
    objectTypes: [World.self, Level.self, Scripture.self]
  )
  
  fileprivate static func mainConfig(userName: String) -> Realm.Configuration {
    return Realm.Configuration(
      fileURL: URL.inDocumentsFolder(fileName: "\(userName).realm"),
      schemaVersion: 1,
      objectTypes: [User.self, LevelData.self]
    )
  }
  
  fileprivate func isFileReachable(userName: String) -> Bool {
    let url = URL.inDocumentsFolder(fileName: "\(userName).realm")
    let boo = ConfigHelper.isFileReachable(url: url)
    //print("Is File Reachable for \(userName)? It is Reachable = \(boo)")
    return boo
  }
  
  private func copyMainData(for userName: String){
    let r = RealmConfig.defaultUserName
    ConfigHelper.copyInitialData(from: Bundle.main.url(forResource: r, withExtension: "realm")!, to: RealmConfig.mainConfig(userName: userName).fileURL!)
  }

  case main(userName: String), readOnly
  
  var configuration: Realm.Configuration {
    switch self {
    case .main(let userName):
      
      if !isFileReachable(userName: userName) {
        //print("File for user \(userName) is not reachable")
        copyMainData(for: userName)
      }
      return RealmConfig.mainConfig(userName: userName)
      
    case .readOnly:
      return RealmConfig.staticConfig
    }
  }
  
}

struct ConfigHelper {
  static func isFileReachable(url fileURL: URL) -> Bool {
    var isReachable = false
    do {
      if try fileURL.checkPromisedItemIsReachable()  {
        isReachable = true
      }
    }catch{
      //print("CheckPromisedItem failed \(error)")
      //print("Data is not Reachable!")
    }
    return isReachable
  }
  
  static func copyInitialData(from: URL, to fileURL: URL){
    //guard !ConfigHelper.isFileReachable(url: fileURL) else { return }
    do {
      _ = try? FileManager.default.removeItem(at: fileURL as URL)
      try FileManager.default.copyItem(at: from as URL, to: fileURL as URL)
      
      //print("We are copying the item over")
      
    }catch {
      print("Could not copy config file over \(error)")
    }
  }
}

extension Realm {
  
  static func readOnlyInstance() -> Realm {
    do {
      return try Realm(configuration: RealmConfig.readOnly.configuration)
    } catch {
      fatalError("Realm failed. Could not get the readonly realm file!")
    }
  }
  
  
  // At this time we are only using a deafult user
  static func mainInstance() -> Realm {
    do {
      //print("Creating mainInstance \(Thread.isMainThread)")
      return try Realm(configuration: RealmConfig.main(userName: "defaultUser").configuration)
    } catch {
      fatalError("Realm failed. Could not get the main realm file!")
    }
  }
  
  static func newInstance() -> Realm {
    do {
      return try Realm()
    } catch {
      fatalError("Realm failed. This means something very bad happen. Closing app now. Goodbye.")
    }
  }
  
  func safeWrite(block: @escaping ()->Void){
    do {
      try self.write{ block() }
    }catch{
      fatalError("Real is usually safe. It failed writing this time. Ironic this is called safe write ;)")
    }
  }
  
}


extension URL {
  static func inDocumentsFolder(fileName: String) -> URL {
    let url = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0], isDirectory: true)
      .appendingPathComponent(fileName)
    return url
  }
}
