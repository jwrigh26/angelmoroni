//
//  RealmConfig.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/27/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift


var copyMainDataRunOnce: Void = {
  ConfigHelper.copyInitialData(from: Bundle.main.url(forResource: "default_v1.0", withExtension: "realm")!, to: RealmConfig.staticConfig.fileURL!)
}()

enum RealmConfig {
  
  fileprivate static let staticConfig =  Realm.Configuration(
    fileURL: Bundle.main.url(forResource: "static_v1.0", withExtension: "realm"),
    readOnly: true,
    objectTypes: [World.self, Level.self, Scripture.self]
  )
  
  fileprivate static let mainConfig = Realm.Configuration(
    fileURL: URL.inDocumentsFolder(fileName: "main.realm"),
    schemaVersion: 1
  )

  case main, readOnly
  
  var configuration: Realm.Configuration {
    switch self {
    case .main:
      _ = copyMainDataRunOnce
      return RealmConfig.mainConfig
    case .readOnly:
      return RealmConfig.staticConfig
    }
  }
  
}

struct ConfigHelper {
  static func isDataReachable(url fileURL: URL) -> Bool {
    var isReachable = false
    do {
      if try fileURL.checkPromisedItemIsReachable()  {
        isReachable = true
      }
    }catch{
      //print("CheckPromisedItem failed \(error)")
      print("Data is not Reachable!")
    }
    return isReachable
  }
  
  
  static func copyInitialData(from: URL, to fileURL: URL){
    guard !ConfigHelper.isDataReachable(url: fileURL) else { return }
    
    do {
      _ = try? FileManager.default.removeItem(at: fileURL as URL)
      try FileManager.default.copyItem(at: from as URL, to: fileURL as URL)
      
      print("We are copying the item over")
      
    }catch {
      print("Could not copy config file over \(error)")
    }
  }
  
}


extension URL {
  static func inDocumentsFolder(fileName: String) -> URL {
    let url = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0], isDirectory: true)
      .appendingPathComponent(fileName)
    return url
  }
}
