//
//  BMGSwiftyExtensions.swift
//  BMGlyphSwiftyLabel
//
//  Created by Justin Wright on 5/21/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation


// http://stackoverflow.com/questions/24092884/get-nth-character-of-a-string-in-swift-programming-language
extension String {
  var length : Int {
    return self.characters.count
  }
  
  subscript(integerIndex: Int) -> Character {
    let index = characters.index(startIndex, offsetBy: integerIndex)
    return self[index]
  }
  
  subscript(integerRange: Range<Int>) -> String {
    let start = characters.index(startIndex, offsetBy: integerRange.lowerBound)
    let end = characters.index(startIndex, offsetBy: integerRange.upperBound)
    let range = start..<end
    return self[range]
  }
}

// http://stackoverflow.com/questions/30839202/unichar-variable-in-swift
//extension unichar : UnicodeScalarLiteralConvertible {
//  public typealias UnicodeScalarLiteralType = UnicodeScalar
//  
//  public init(unicodeScalarLiteral scalar: UnicodeScalar) {
//    self.init(scalar.value)
//  }
//}

// http://stackoverflow.com/questions/24102044/how-can-i-get-the-unicode-code-points-of-a-character
extension Character
{
  func unicodeScalarCodePoint() -> UInt32
  {
    let characterString = String(self)
    let scalars = characterString.unicodeScalars
    
    return scalars[scalars.startIndex].value
  }
  
  // added by me
  var unicode: UInt16 {
    return UInt16(self.unicodeScalarCodePoint())
  }
}


