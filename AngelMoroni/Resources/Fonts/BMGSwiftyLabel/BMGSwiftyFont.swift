//
//  BMGSwiftyFont.swift
//  BMGlyphSwiftyLabel
//
//  Created by Justin Wright on 5/18/16.
//  Inspired by Stéphane QUERAUD on 20/02/2016.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import SpriteKit
import SWXMLHash

open class BMGSwiftyFont {
  
  public typealias ParseCallback = (_ success: Bool) -> Void
  
  
  
  var lineHeight: Int = 0
  var kernings = [String : Int]()
  var chars = [String : Int]()
  var charsTextures = [String : SKTexture]()
  var textureAtlas: SKTextureAtlas?
  var name: String
  
  
  open class func fontWithTextureAtlas(_ name: String, textureAtlas: SKTextureAtlas) -> BMGSwiftyFont {
    return BMGSwiftyFont(name: name, textureAtlas: textureAtlas)
  }
  
  open class func fontWithName(_ name: String) -> BMGSwiftyFont {
    return BMGSwiftyFont(name: name)
  }
  
  public init(name: String, textureAtlas: SKTextureAtlas? = nil) {
    
    if let textureAtlas = textureAtlas {
      self.textureAtlas = textureAtlas
    }else {
      self.textureAtlas = SKTextureAtlas(named: name)
    }
    
    self.name = name // For reference during label init:coder
    parseFile(name)
  }
  
  func getSuffixForDevice() -> String {
    var suffix: String = ""
    var scale: CGFloat = 0
    
    if (UIScreen.main.responds(to: #selector(getter: UIScreen.nativeScale))) {
      scale = UIScreen.main.nativeScale
    }else {
      scale = UIScreen.main.scale
    }
    
    if scale == 2.0 {
      suffix = "@2x"
    }
    else if scale > 2.0 && scale <= 3.0 {
      suffix = "@3x"
    }
    
    return suffix
  }
  
  func xAdvance(_ charId: unichar) -> Int {
    guard let v = chars["xadvance_\(Int(charId))"] else { return 0 }
    return v
  }
  
  func xOffset(_ charId: unichar) -> Int {
    guard let v = chars["xoffset_\(Int(charId))"] else { return 0 }
    return v
  }
  
  func yOffset(_ charId: unichar) -> Int {
    guard let v = chars["yoffset_\(Int(charId))"] else { return 0 }
    return v
  }
  
  func kerningForFirst(_ first: unichar, second: unichar) -> Int {
    guard let v = kernings["\(Int(first))/\(Int(second))"] else { return 0 }
    return v
  }
  
  func textureFor(_ charId: Int) -> SKTexture {
    guard let textureAtlas = textureAtlas else {
      assert(false, "TextureAtlas needs to be set before calling.")
      return SKTexture()
    }
    return textureAtlas.textureNamed("\(charId)")
  }
  
  
  // MARK: XML Parsing Starts here.
  
  
  func parseChars(_ xml: XMLIndexer){
    
    
    for elem in xml["font"]["chars"]["char"] {
      
      if let charId = elem.element?.attribute(by: "id"),
        let xadvance = elem.element?.attribute(by: "xadvance"),
        let xoffset = elem.element?.attribute(by: "xoffset"),
        let yoffset = elem.element?.attribute(by: "yoffset") {
        
        let x:String = "xoffset_" + charId.text
        let y:String = "yoffset_" + charId.text
        let xa:String = "xadvance_" + charId.text
        
        chars[x] = Int(xoffset.text)
        chars[y] = Int(yoffset.text)
        chars[xa] = Int(xadvance.text)
        charsTextures[charId.text] = textureFor(Int(charId.text)!)
      }
      
      
      // Cannot do things like this because of memory leaks around ContiguousArrayStorage bug found in Xcode 7.3.
      //      chars["xoffset_\(charId)"] = xoffset
      //      chars["yoffset_\(charId)"] = yoffset
      //      chars["xadvance_\(charId)"] = xadvance
      //      charsTextures["\(charId)"] = textureFor(charId)
    }
    
  }
  
  func parseKernings(_ xml: XMLIndexer){
    
    for elem in xml["font"]["kernings"]["kerning"] {
      
      if let first =  elem.element?.attribute(by: "first"),
        let second = elem.element?.attribute(by: "second"),
        let amount = elem.element?.attribute(by: "amount") {
        kernings[ first.text + "/" + second.text] = Int(amount.text)
      }
    }
  }
  
  func parseLineHeight(_ xml: XMLIndexer){
    if let lh = xml["font"]["common"].element?.attribute(by: "lineHeight") {
      //print("Line Height \(lh) and txt \(lh.text)")
      lineHeight = Int(lh.text)!
    }
  }
  
  func parseFile(_ name: String){
    
    
    //let fontfile: String = "\(name)\(getSuffixForDevice())"
    let fontfile: String = name + getSuffixForDevice()
    let path: URL = Bundle.main.url(forResource: fontfile, withExtension: "xml")!
    let data: Data = try! Data(contentsOf: path)
    let xml = SWXMLHash.parse(data)
    
    parseKernings(xml)
    parseChars(xml)
    parseLineHeight(xml)
  }
  
  
  
}
