//
//  BMGSwiftyLabel.swift
//  BMGlyphSwiftyLabel
//
//  Created by Justin Wright on 5/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import SpriteKit


open class BMGSwiftyLabel: SKNode {
  
  public enum HorizontalAlignment: Int { case centered, right, left }
  public enum VerticalAlignment: Int { case top, middle, bottom }
  public enum TextJustify: Int { case center, right, left }
  
  open var font : BMGSwiftyFont!
  
  fileprivate var _text: String?
  fileprivate var _horizontalAlignment: HorizontalAlignment
  fileprivate var _verticalAlignment: VerticalAlignment
  fileprivate var _textJustify: TextJustify
  fileprivate var _color: SKColor
  fileprivate var _colorBlendFactor: CGFloat
  fileprivate var _totalSize: CGSize!
  fileprivate var _fontName: String!
  fileprivate var _textureAtlas: SKTextureAtlas?
  
  open var boundingBox: CGRect {
    let af = calculateAccumulatedFrame()
    return CGRect(x: frame.origin.x, y: frame.origin.y, width: af.width, height: af.height)
  }
  
  open var textJustify: TextJustify? {
    didSet {
      _textJustify = textJustify ?? .center
      justifyText()
    }
  }
  
  open var horizontalAlignment: HorizontalAlignment? {
    didSet {
      _horizontalAlignment = horizontalAlignment ?? .centered
      justifyText()
    }
  }
  
  open var verticalAlignment: VerticalAlignment? {
    didSet {
      _verticalAlignment = verticalAlignment ?? .middle
      justifyText()
    }
  }
  
  open var text: String? {
    didSet {
      setLabelText(text)
    }
  }
  
  open var color: SKColor? {
    didSet {
      guard let color = color else { return }
      _color = color
      children.forEach{ ($0 as? SKSpriteNode)?.color = color }
    }
  }
  
  open var colorBlendFactor: CGFloat = 0 {
    didSet {
      colorBlendFactor = min(colorBlendFactor, 1.0)
      colorBlendFactor = max(colorBlendFactor, 0.0)
      _colorBlendFactor = colorBlendFactor
      children.forEach{ ($0 as? SKSpriteNode)?.colorBlendFactor = colorBlendFactor }
    }
  }
  
  
  public convenience init(text: String, font: BMGSwiftyFont) {
    self.init()
    self.font = font
    setLabelText(text)
  }
  
  public override init(){
    _horizontalAlignment = .centered
    _verticalAlignment = .middle
    _textJustify = .left
    _color = SKColor(red: 1, green: 1, blue: 1, alpha: 1)
    _colorBlendFactor = 1.0
    
    super.init()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  fileprivate func setLabelText(_ text: String?){
    guard let text = text , text != _text else { return }
    _text = text
    updateLabel()
    justifyText()
  }
  
  // MARK: JustifyText
  
  fileprivate func justifyText(){
    let shift = shiftAlignment()
    
    guard let text = _text , _textJustify != .left else { return }
    
    var numberNodes: Int = 0; var nodePosition: Int = 0; var widthForLine: Int = 0
    var c: unichar
    for i in 0...text.length {
      c = i != text.length ? text[i].unicode : "\n"[0].unicode
      
      if c == "\n"[0].unicode {
        if numberNodes > 0 {
          while nodePosition < numberNodes {
            justifyNode(nodePosition, shift: shift, widthForLine: widthForLine)
            nodePosition += 1
          }
        }
        
        widthForLine = 0
        
      }else{
        if let node = children[numberNodes] as? SKSpriteNode {
          numberNodes += 1
          widthForLine = Int(node.position.x + node.size.width)
        }
      }
      
    }
  }
  
  // MARK: Helper methods for JustifyText
  
  fileprivate func justifyNode(_ nodePosition: Int, shift: CGPoint, widthForLine: Int){
    if let node = children[nodePosition] as? SKSpriteNode {
      var x: CGFloat = node.position.x
      
      if textJustify == .right {
        x += _totalSize.width - CGFloat(widthForLine) + shift.x
      }else{
        x += (_totalSize.width - CGFloat(widthForLine)) / 2 + (shift.x / 2)
      }
      
      node.position = CGPoint(x: x, y: node.position.y)
    }
  }
  
  
  fileprivate func shiftAlignment() -> CGPoint {
    var shift: CGPoint = CGPoint.zero
    
    switch _horizontalAlignment {
    case .left:
      shift.x = 0
    case .right:
      shift.x = -_totalSize.width
    case .centered:
      shift.x = -_totalSize.width / 2
    }
    
    switch _verticalAlignment {
    case .top:
      shift.y = 0
    case .bottom:
      shift.y = -_totalSize.height
    case .middle:
      shift.y = -_totalSize.height / 2
    }
    
    let sprites = children.filter{ $0 is SKSpriteNode }
    sprites.forEach{ shiftSpritePosition(($0 as! SKSpriteNode), shift: shift) }
    
    return shift
  }
  
  fileprivate func shiftSpritePosition(_ sprite: SKSpriteNode, shift: CGPoint) {
    if let originalPos = (sprite.userData?.object(forKey: "originalPosition") as? NSValue)?.cgPointValue {
      sprite.position = CGPoint(x: originalPos.x + shift.x, y: originalPos.y - shift.y)
    }
  }
  
  
  // MARK: UpdateLabel
  
  fileprivate func updateLabel(){
    guard let text = _text else { return }

    let childCount = children.count
    let linesCount = text.components(separatedBy: "\n").count - 1
    let scaleFactor = UIScreen.main.scale

    var lastCharId: unichar = 0
    var pos = CGPoint.zero
    var size = CGSize(width: 0, height: adjustedHeight(scaleFactor))
    var realCharCount: Int = 0
    var letter: SKSpriteNode?

    removeUnusedSKSpriteNodes(childCount: childCount, linesCount: linesCount)
    
    for i in 0..<text.length {
      
      let c = text[i]
      if c == "\n"[0] {
        let h = adjustedHeight(scaleFactor)
        pos.y -= h; pos.x = 0
        size.height += h
      }
      // re-use existing SKSpriteNode and re-assign the correct rexture
      else {
        
        if realCharCount < childCount {
          if let l = children[realCharCount] as? SKSpriteNode {
            l.texture = font.charsTextures["\(c.unicode)"]
            l.size = l.texture!.size()
            letter = l
          }
        }else{
          
          if let texture = font.charsTextures["\(c.unicode)"] {
            letter = SKSpriteNode(texture: texture)
            addChild(letter!)
          }
        }
        
        if letter != nil {
          styleLetter(&letter!, pos: &pos, c: c.unicode, l: lastCharId, scaleFactor: scaleFactor)
        }
        
        if size.width < pos.x { size.width = pos.x }
        realCharCount += 1
      }
      
      lastCharId = c.unicode
    }
    
    _totalSize = size
    
  }
  
  // MARK: Helper methods for UpdateLabel
  
  fileprivate func styleLetter(_ letter: inout SKSpriteNode, pos: inout CGPoint, c: unichar, l: unichar, scaleFactor: CGFloat){
    letter.colorBlendFactor = _colorBlendFactor
    letter.color = _color
    letter.anchorPoint = CGPoint.zero
    let x: CGFloat = pos.x + CGFloat(font.xOffset(c) + font.kerningForFirst(l, second: c)) / scaleFactor
    let y: CGFloat = pos.y - ( letter.size.height + CGFloat(font.yOffset(c)) / scaleFactor )
    letter.position = CGPoint(x: x, y: y)
    letter.userData = [ "originalPosition" : NSValue(cgPoint: letter.position)]
    
    pos.x += CGFloat(font.xAdvance(c) + font.kerningForFirst(l, second: c)) / scaleFactor
    
  }
  
  fileprivate func adjustedHeight(_ scaleFactor: CGFloat) -> CGFloat {
    guard let text = _text , text.length > 0 else { return 0 }
    return CGFloat(font.lineHeight) / scaleFactor
  }
  
  fileprivate func removeUnusedSKSpriteNodes(childCount: Int, linesCount: Int){
    guard let text = _text , (text.length - linesCount < childCount) && (childCount > 0) else { return }
    let length = text.length - linesCount
    
    for i in (length..<childCount).reversed() {
      if let sprite = children[i - 1] as? SKSpriteNode {
        sprite.removeFromParent()
      }
    }
  }
  
}



