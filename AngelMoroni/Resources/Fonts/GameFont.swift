//
//  GameFont.swift
//  AngelMoroni
//
//  Created by Justin Wright on 9/18/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit

enum GameFont: String {
  case pixelEmulator = "PixelEmulator"
  case pixelIntv = "PixelIntv"
  
  func font(size: CGFloat) -> UIFont {
    return UIFont(name: self.rawValue, size: size)!
  }
}

