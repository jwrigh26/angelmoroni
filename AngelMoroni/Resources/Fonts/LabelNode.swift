//
//  ScriptureBitFont.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/18/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import SpriteKit

/*

func testFont(){
let node = SKLabelNode(fontNamed: "PixelIntv")
node.text = "WE BELIEVE IN GOD"
node.fontSize = 25
node.position = CGPoint(x:200, y: 200)
node.fontColor = UIColor(netHex: 0x43280B)
self.addChild(node)
}

*/

struct GameLabelNode: LabelNode {
  let fntColor: UIColor
  let fntName: String
  
  
  func createLabel(_ text: String, size: CGFloat) -> SKLabelNode {
    let textNode = SKLabelNode(fontNamed: fntName)
    let str = text.uppercased()
    textNode.text = str
    textNode.fontSize = Device.Scale * size
    textNode.fontColor = fntColor
    textNode.verticalAlignmentMode = .center
    textNode.horizontalAlignmentMode = .center
    return textNode
  }
  
  init(fntName: String, fntColor: UIColor) {
    self.fntName = fntName; self.fntColor = fntColor;
  }
  
  init(fntColor: UIColor) {
    self.fntColor = fntColor
    self.fntName = GameFont.pixelIntv.rawValue
  }
}

struct GameOutlineLabelNode: LabelNode {
  let fntColor: UIColor
  let fntName: String
  var lineWidth: CGFloat = Device.isPhone ? 4 : 5
  
  
  func createLabel(_ text: String, size: CGFloat) -> SKLabelNode {
    let textNode = MKOutlinedLabelNode(fontNamed: fntName, fontSize: Device.Scale * size, lineWidth: self.lineWidth)
    textNode.borderColor = UIColor(netHex: GameColor.darkGray.hex)
    textNode.horizontalAlignmentMode = .center
    textNode.fontColor = fntColor
    textNode.outlinedText = text
    return textNode
  }
  
  init(fntName: String, fntColor: UIColor) {
    self.fntName = fntName; self.fntColor = fntColor;
  }
  
  init(fntColor: UIColor) {
    self.fntColor = fntColor
    self.fntName = GameFont.pixelIntv.rawValue
  }
}


protocol  LabelNode {
  var fntName: String { get }
  var fntColor: UIColor { get }
  func createLabel(_ text: String, size: CGFloat) -> SKLabelNode
  init(fntName: String, fntColor: UIColor)
  init(fntColor: UIColor)
}


//    bitsLabelNode = MKOutlinedLabelNode(fontNamed: GameFont.PixelIntv.font, fontSize: Device.Scale * 10, lineWidth: 5)
//    bitsLabelNode.borderColor = UIColor(netHex: GameColor.DarkGray.hex)
//    bitsLabelNode.horizontalAlignmentMode = .Center
//    bitsLabelNode.fontColor = UIColor(netHex: GameColor.CloudWhite.hex)
//    bitsLabelNode.outlinedText = bitsToString()
//    let pos2 = CGPoint(x: bitsLabelNode.frame.width/2 + titleLabelNode.frame.maxX + 16, y: y)
//    bitsLabelNode.position = pos2
//    addChild(bitsLabelNode)
