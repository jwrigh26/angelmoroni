//
//  MKOutlinedLabelNode.swift
//
//  Created by Mario Klaver on 13-8-2015.
//  Copyright (c) 2015 Endpoint ICT. All rights reserved.
//

import UIKit
import SpriteKit

class MKOutlinedLabelNode: SKLabelNode {
  
  var borderColor: UIColor = UIColor.black
  
  var outlinedText: String! {
    didSet { drawText() }
  }
  
  var lineWidth: CGFloat!
  var lineOffsetX: CGFloat = 1
  
  fileprivate var border: SKShapeNode?
  
  required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
  
  override init() {
    super.init()
  }
  
  init(fontNamed fontName: String!, fontSize: CGFloat, lineWidth: CGFloat = 4) {
    super.init(fontNamed: fontName)
    self.fontSize = fontSize
    self.lineWidth = lineWidth
  }
  
  func drawText() {
    if let borderNode = border {
      borderNode.removeFromParent()
      border = nil
    }
    
    if let text = outlinedText {
      self.text = text
      if let path = createBorderPathForText() {
        let border = SKShapeNode()
        
        border.strokeColor = borderColor
        border.lineWidth = lineWidth;
        border.path = path
        border.position = positionBorder(border)
        addChild(border)
        
        self.border = border
      }
    }
  }
  
  fileprivate func getTextAsCharArray() -> [UniChar] {
    var chars = [UniChar]()
    
    for codeUnit in text!.utf16 {
      chars.append(codeUnit)
    }
    return chars
  }
  
  fileprivate func createBorderPathForText() -> CGPath? {
    let chars = getTextAsCharArray()
    let borderFont = CTFontCreateWithName(self.fontName as CFString?, self.fontSize, nil)
    
    var glyphs = Array<CGGlyph>(repeating: 0, count: chars.count)
    let gotGlyphs = CTFontGetGlyphsForCharacters(borderFont, chars, &glyphs, chars.count)
    
    if gotGlyphs {
      var advances = Array<CGSize>(repeating: CGSize(), count: chars.count)
      
      CTFontGetAdvancesForGlyphs(borderFont, .horizontal, glyphs, &advances, chars.count)
      
      let letters = CGMutablePath()
      var xPosition = 0 as CGFloat
      for index in 0...(chars.count - 1) {
        let letter = CTFontCreatePathForGlyph(borderFont, glyphs[index], nil)
        let t = CGAffineTransform(translationX: xPosition , y: 0)
        
        if let path = letter {
          //CGPathAddPath(letters, &t, letter)
          letters.addPath(path, transform: t)
        }
        
        
        xPosition = xPosition + advances[index].width
      }
      
      return letters
    } else {
      return nil
    }
  }
  
  fileprivate func positionBorder(_ border: SKShapeNode) -> CGPoint {
    let sizeText = self.calculateAccumulatedFrame()
    let sizeBorder = border.calculateAccumulatedFrame()
    let offsetX = (sizeBorder.width - sizeText.width) / 2
    
    return CGPoint(x: -(sizeBorder.width / 2) + offsetX+lineOffsetX, y: 1)
  }
}
