//
//  Spark.swift
//  HelloWorldSwift
//
//  Created by Wright, Justin M on 9/16/15.
//  Copyright © 2015 Wright, Justin M. All rights reserved.
//

import SpriteKit

final class ScriptureBitParticlePool {
  
  var particles: [Particle]
  let pool: Pool<Particle>
  weak var node: SKNode?
  
  
  
  init() {
    particles = [Particle]()
    for index in 1...10 {
      let spark:Particle = Particle(fileNamed: GameManager.sharedInstance.scriptureBitParticle)!
      spark.id = index
      particles.append(spark)
    }
    pool = Pool<Particle>(items: particles)
  }
  
  
  
  
  func fetchParticle()-> Particle? {
    let spark = pool.getFromPool()
    return spark
  }
  
  func returnParticle(_ particle: Particle) {
    particle.cleanup()
    pool.returnToPool(particle)
  }
  
  func showParticle(_ box: CGRect) {
    guard let particle = fetchParticle(),
      let node = self.node else { return }
    let sparkPosition = CGPoint(x: box.origin.x + box.width/2, y: box.origin.y + box.height)
    particle.position = sparkPosition
    particle.isHidden = false
    particle.particlePositionRange = CGVector(dx: box.width, dy: box.height)
    particle.targetNode = node
    particle.advanceSimulationTime(1)
    particle.resetSimulation()
    
    node.addChild(particle)
    let lifeSpan: CGFloat = CGFloat(particle.numParticlesToEmit) / particle.particleBirthRate +
      particle.particleLifetime + particle.particleLifetimeRange / 2
    let time = TimeInterval(lifeSpan)
    node.run(SKAction.wait(forDuration: time), completion: { [weak self] () -> Void in
      self?.returnParticle(particle)
      
      })
  }
  
  
}

final class Particle: SKEmitterNode,  CleanupProtocol {
  
  var id:Int = 0
  
  deinit {
    //print("Spark was deinit")
  }
  
  func cleanup(){
    
    self.resetSimulation()
    self.targetNode = nil
    self.removeAllChildren()
    self.removeAllActions()
    self.removeFromParent()
    NSObject.cancelPreviousPerformRequests(withTarget: self)
  }
  
}

struct StarField {
  
  //let rotateAction = SKAction.repeatActionForever(SKAction.sequence([
  //   SKAction.rotateByAngle(CGFloat(-M_PI_4), duration: 1), SKAction.rotateByAngle(CGFloat(M_PI_4), duration: 1)]))
  
  func emitterNode ( _ speed: CGFloat, lifetime: CGFloat, scale: CGFloat, birthRate: CGFloat, color: SKColor) -> SKEmitterNode {
    
    let star = SKSpriteNode(imageNamed: "whiteBit.png")
    let textureView = SKView()
    let texture = textureView.texture(from: star)
    texture?.filteringMode = .nearest
    
    let emitterNode = SKEmitterNode()
    emitterNode.particleTexture = texture!
    emitterNode.particleBirthRate = birthRate
    emitterNode.particleColor = color
    emitterNode.particleLifetime = lifetime
    emitterNode.particleSpeed = speed
    emitterNode.particleScale = scale
    emitterNode.particleColorBlendFactor = 1
    emitterNode.emissionAngle = -0
    let x = screenBounds.maxX
    let y = screenBounds.midY
    
    emitterNode.position = CGPoint(x: x, y: y)
    emitterNode.particlePositionRange = CGVector(dx: screenBounds.maxX, dy: screenBounds.maxY)
    emitterNode.particleSpeedRange = 16.0
    emitterNode.particleRotation = CGFloat(-M_PI_2)
    emitterNode.particleRotationRange = CGFloat(M_PI_2)
    emitterNode.particleRotationSpeed = 0.5
    
    let alphaSequence = SKKeyframeSequence(keyframeValues: [1.0, 0.0, 0.5, 0.0, 1.0], times: [0.0,0.25, 0.4, 0.5, 0.70])
    emitterNode.particleAlphaSequence = alphaSequence
    emitterNode.advanceSimulationTime(TimeInterval(lifetime))
    return emitterNode
  }
  
}
