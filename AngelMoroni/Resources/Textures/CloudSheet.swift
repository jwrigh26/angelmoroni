// ---------------------------------------
// Sprite definitions for 'CloudSheet'
// Generated with TexturePacker 4.2.3
//
// http://www.codeandweb.com/texturepacker
// ---------------------------------------

import SpriteKit


class CloudSheet {

    // sprite names
    let CLOUD_B2_01 = "cloud_b2_01"
    let CLOUD_B2_02 = "cloud_b2_02"
    let CLOUD_B2_03 = "cloud_b2_03"
    let CLOUD_B3_01 = "cloud_b3_01"
    let CLOUD_B3_02 = "cloud_b3_02"
    let CLOUD_B3_03 = "cloud_b3_03"
    let CLOUD_B3_04 = "cloud_b3_04"
    let CLOUD_B3_05 = "cloud_b3_05"
    let CLOUD_B3_06 = "cloud_b3_06"
    let CLOUD_B4_01 = "cloud_b4_01"
    let CLOUD_B4_02 = "cloud_b4_02"
    let CLOUD_B4_03 = "cloud_b4_03"
    let CLOUD_B4_04 = "cloud_b4_04"
    let CLOUD_B4_05 = "cloud_b4_05"
    let CLOUD_B4_06 = "cloud_b4_06"
    let SUN         = "sun"


    // load texture atlas
    let textureAtlas = SKTextureAtlas(named: "CloudSheet")


    // individual texture objects
    func cloud_b2_01() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B2_01) }
    func cloud_b2_02() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B2_02) }
    func cloud_b2_03() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B2_03) }
    func cloud_b3_01() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B3_01) }
    func cloud_b3_02() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B3_02) }
    func cloud_b3_03() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B3_03) }
    func cloud_b3_04() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B3_04) }
    func cloud_b3_05() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B3_05) }
    func cloud_b3_06() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B3_06) }
    func cloud_b4_01() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B4_01) }
    func cloud_b4_02() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B4_02) }
    func cloud_b4_03() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B4_03) }
    func cloud_b4_04() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B4_04) }
    func cloud_b4_05() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B4_05) }
    func cloud_b4_06() -> SKTexture { return textureAtlas.textureNamed(CLOUD_B4_06) }
    func sun() -> SKTexture         { return textureAtlas.textureNamed(SUN) }


    // texture arrays for animations
    func cloud_b2_() -> [SKTexture] {
        return [
            cloud_b2_01(),
            cloud_b2_02(),
            cloud_b2_03()
        ]
    }

    func cloud_b3_() -> [SKTexture] {
        return [
            cloud_b3_01(),
            cloud_b3_02(),
            cloud_b3_03(),
            cloud_b3_04(),
            cloud_b3_05(),
            cloud_b3_06()
        ]
    }

    func cloud_b4_() -> [SKTexture] {
        return [
            cloud_b4_01(),
            cloud_b4_02(),
            cloud_b4_03(),
            cloud_b4_04(),
            cloud_b4_05(),
            cloud_b4_06()
        ]
    }


}
