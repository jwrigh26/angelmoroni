//
//  GameSheet-Angel.swift
//  AngelMoroni
//
//  Created by Justin Wright on 4/15/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

extension GameSheet {
  
  // texture arrays for animations
  func angelFly()-> [SKTexture] {
    return [
      AngelMoroni_am_01(),
      AngelMoroni_am_02(),
      AngelMoroni_am_03(),
      AngelMoroni_am_03(),
      AngelMoroni_am_02(),
      AngelMoroni_am_01()
    ]
  }
  
  func angelWalk()-> [SKTexture] {
    return [
      AngelMoroni_am_21(),
      AngelMoroni_am_22(),
      AngelMoroni_am_23(),
      AngelMoroni_am_22()
    ]
  }
  
  func angelTakeoff()-> [SKTexture] {
    return [
      AngelMoroni_am_22(),
      //AngelMoroni_am_24(),
      //AngelMoroni_am_25(),
      AngelMoroni_am_26(),
      //AngelMoroni_am_27(),
      //AngelMoroni_am_28(),
      AngelMoroni_am_01()
    ]
  }
  
  func angelTouchdown()-> [SKTexture] {
    return [
      AngelMoroni_am_01(),
      //AngelMoroni_am_28(),
      //AngelMoroni_am_27(),
      AngelMoroni_am_26(),
      //AngelMoroni_am_25(),
      //AngelMoroni_am_24(),
      AngelMoroni_am_22()
    ]
  }
  
  
  
  func angelTakeDamage()-> [SKTexture] {
    return [
      AngelMoroni_am_01(),
      AngelMoroni_am_06(),
      AngelMoroni_am_07(),
      AngelMoroni_am_07(),
      AngelMoroni_am_08(),
      AngelMoroni_am_08()
    ]
  }
  
  
  
  func angelRecovering()-> [SKTexture] {
    return [
      AngelMoroni_am_13(),
      AngelMoroni_am_13(),
      AngelMoroni_am_14(),
      AngelMoroni_am_14(),
      AngelMoroni_am_15(),
      AngelMoroni_am_15(),
      AngelMoroni_am_16(),
      AngelMoroni_am_16()
    ]
  }
  
}