//
//  GameSheet-Enemies.swift
//  AngelMoroni
//
//  Created by Justin Wright on 7/19/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import SpriteKit

extension GameSheet {
  
  
  func LittleImpBreath() -> [SKTexture] {
    return [
      Enemies_LittleImp_littlimp_01(),
      Enemies_LittleImp_littlimp_05(),
      Enemies_LittleImp_littlimp_01()
    ]
  }
  
  func LittleImpGrowl() -> [SKTexture] {
    return [
      Enemies_LittleImp_littlimp_01(),
      Enemies_LittleImp_littlimp_02(),
      Enemies_LittleImp_littlimp_03(),
      Enemies_LittleImp_littlimp_03(),
      Enemies_LittleImp_littlimp_04(),
      Enemies_LittleImp_littlimp_01()
    ]
  }
  
  func LittleImpJump() -> [SKTexture] {
    return [
      Enemies_LittleImp_littlimp_01(),
      Enemies_LittleImp_littlimp_05(),
      Enemies_LittleImp_littlimp_06(),
      Enemies_LittleImp_littlimp_07(),
      Enemies_LittleImp_littlimp_05(),
      Enemies_LittleImp_littlimp_01()
    ]
  }
  
  func GoblinBreath() -> [SKTexture] {
    return [
      Enemies_Goblin_goblin_01(),
      Enemies_Goblin_goblin_04(),
      Enemies_Goblin_goblin_01()
    ]
  }
  
  func GoblinHeadTurn() -> [SKTexture] {
    return [
      Enemies_Goblin_goblin_01(),
      Enemies_Goblin_goblin_03(),
      Enemies_Goblin_goblin_01()
    ]
  }
  
  func GoblinBlink() -> [SKTexture] {
    return [
      Enemies_Goblin_goblin_01(),
      Enemies_Goblin_goblin_02(),
      Enemies_Goblin_goblin_01()
    ]
  }
  
  func ImpBreath() -> [SKTexture] {
    return [
      Enemies_Imp_medimp_01(),
      Enemies_Imp_medimp_02(),
      Enemies_Imp_medimp_01()
    ]
  }
  
  func ImpBlink() -> [SKTexture] {
    return [
      Enemies_Imp_medimp_01(),
      Enemies_Imp_medimp_03(),
      Enemies_Imp_medimp_01()
    ]
  }
  
  func SkeletonBreath() -> [SKTexture] {
    return [
      Enemies_skeleton_skeleton_01(),
      Enemies_skeleton_skeleton_02(),
      Enemies_skeleton_skeleton_01()
    ]
  }
  
  func SkeletonLook1() -> [SKTexture] {
    return [
      Enemies_skeleton_skeleton_01(),
      Enemies_skeleton_skeleton_03(),
      Enemies_skeleton_skeleton_03(),
      Enemies_skeleton_skeleton_01()
    ]
  }
  
  func SkeletonLook2() -> [SKTexture] {
    return [
      Enemies_skeleton_skeleton_01(),
      Enemies_skeleton_skeleton_03(),
      Enemies_skeleton_skeleton_04(),
      Enemies_skeleton_skeleton_04(),
      Enemies_skeleton_skeleton_01()
    ]
  }
  
  func OniBreath() -> [SKTexture] {
    return [
      Enemies_Oni_oni_01(),
      Enemies_Oni_oni_03(),
      Enemies_Oni_oni_01()
    ]
  }
  
  func OniBlink() -> [SKTexture] {
    return [
      Enemies_Oni_oni_01(),
      Enemies_Oni_oni_02(),
      Enemies_Oni_oni_01()
    ]
  }
  
  func SlurpeeImpBlink() -> [SKTexture] {
    return [
      Enemies_SlurpeeImp_slurpee_imp_01(),
      Enemies_SlurpeeImp_slurpee_imp_04(),
      Enemies_SlurpeeImp_slurpee_imp_01()
    ]
  }
  
  func SlurpeeImpSlurp() -> [SKTexture] {
    return [
      Enemies_SlurpeeImp_slurpee_imp_01(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_01(),
    ]
  }
  
  func SlurpeeImpDoubleSlurp() -> [SKTexture] {
    return [
      Enemies_SlurpeeImp_slurpee_imp_01(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_03(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_02(),
      Enemies_SlurpeeImp_slurpee_imp_01(),
      
    ]
  }
  
  func JetPackGoblin() -> [SKTexture] {
    return [
      Enemies_JetpackGoblin_jetpack_goblin_01(),
      Enemies_JetpackGoblin_jetpack_goblin_02(),
      Enemies_JetpackGoblin_jetpack_goblin_03()
    ]
  }
  
  
}

