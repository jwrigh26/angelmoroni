// ---------------------------------------
// Sprite definitions for 'GameSheet'
// Generated with TexturePacker 4.2.3
//
// http://www.codeandweb.com/texturepacker
// ---------------------------------------

import SpriteKit


class GameSheet {

    // sprite names
    let ANGELMORONI_AM_01                       = "AngelMoroni/am_01"
    let ANGELMORONI_AM_02                       = "AngelMoroni/am_02"
    let ANGELMORONI_AM_03                       = "AngelMoroni/am_03"
    let ANGELMORONI_AM_06                       = "AngelMoroni/am_06"
    let ANGELMORONI_AM_07                       = "AngelMoroni/am_07"
    let ANGELMORONI_AM_08                       = "AngelMoroni/am_08"
    let ANGELMORONI_AM_13                       = "AngelMoroni/am_13"
    let ANGELMORONI_AM_14                       = "AngelMoroni/am_14"
    let ANGELMORONI_AM_15                       = "AngelMoroni/am_15"
    let ANGELMORONI_AM_16                       = "AngelMoroni/am_16"
    let ANGELMORONI_AM_17                       = "AngelMoroni/am_17"
    let ANGELMORONI_AM_21                       = "AngelMoroni/am_21"
    let ANGELMORONI_AM_22                       = "AngelMoroni/am_22"
    let ANGELMORONI_AM_23                       = "AngelMoroni/am_23"
    let ANGELMORONI_AM_24                       = "AngelMoroni/am_24"
    let ANGELMORONI_AM_25                       = "AngelMoroni/am_25"
    let ANGELMORONI_AM_26                       = "AngelMoroni/am_26"
    let ANGELMORONI_AM_27                       = "AngelMoroni/am_27"
    let ANGELMORONI_AM_28                       = "AngelMoroni/am_28"
    let CLOUDS_CLOUD_B2_01                      = "Clouds/cloud_b2_01"
    let CLOUDS_CLOUD_B2_02                      = "Clouds/cloud_b2_02"
    let CLOUDS_CLOUD_B2_03                      = "Clouds/cloud_b2_03"
    let CLOUDS_CLOUD_B3_01                      = "Clouds/cloud_b3_01"
    let CLOUDS_CLOUD_B3_02                      = "Clouds/cloud_b3_02"
    let CLOUDS_CLOUD_B3_03                      = "Clouds/cloud_b3_03"
    let CLOUDS_CLOUD_B3_04                      = "Clouds/cloud_b3_04"
    let CLOUDS_CLOUD_B3_05                      = "Clouds/cloud_b3_05"
    let CLOUDS_CLOUD_B3_06                      = "Clouds/cloud_b3_06"
    let CLOUDS_CLOUD_B4_01                      = "Clouds/cloud_b4_01"
    let CLOUDS_CLOUD_B4_02                      = "Clouds/cloud_b4_02"
    let CLOUDS_CLOUD_B4_03                      = "Clouds/cloud_b4_03"
    let CLOUDS_CLOUD_B4_04                      = "Clouds/cloud_b4_04"
    let CLOUDS_CLOUD_B4_05                      = "Clouds/cloud_b4_05"
    let CLOUDS_CLOUD_B4_06                      = "Clouds/cloud_b4_06"
    let CLOUDS_SUN                              = "Clouds/sun"
    let ENEMIES_GOBLIN_GOBLIN_01                = "Enemies/Goblin/goblin_01"
    let ENEMIES_GOBLIN_GOBLIN_02                = "Enemies/Goblin/goblin_02"
    let ENEMIES_GOBLIN_GOBLIN_03                = "Enemies/Goblin/goblin_03"
    let ENEMIES_GOBLIN_GOBLIN_04                = "Enemies/Goblin/goblin_04"
    let ENEMIES_IMP_MEDIMP_01                   = "Enemies/Imp/medimp_01"
    let ENEMIES_IMP_MEDIMP_02                   = "Enemies/Imp/medimp_02"
    let ENEMIES_IMP_MEDIMP_03                   = "Enemies/Imp/medimp_03"
    let ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_01 = "Enemies/JetpackGoblin/jetpack_goblin_01"
    let ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_02 = "Enemies/JetpackGoblin/jetpack_goblin_02"
    let ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_03 = "Enemies/JetpackGoblin/jetpack_goblin_03"
    let ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_04 = "Enemies/JetpackGoblin/jetpack_goblin_04"
    let ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_05 = "Enemies/JetpackGoblin/jetpack_goblin_05"
    let ENEMIES_LITTLEIMP_LITTLIMP_01           = "Enemies/LittleImp/littlimp_01"
    let ENEMIES_LITTLEIMP_LITTLIMP_02           = "Enemies/LittleImp/littlimp_02"
    let ENEMIES_LITTLEIMP_LITTLIMP_03           = "Enemies/LittleImp/littlimp_03"
    let ENEMIES_LITTLEIMP_LITTLIMP_04           = "Enemies/LittleImp/littlimp_04"
    let ENEMIES_LITTLEIMP_LITTLIMP_05           = "Enemies/LittleImp/littlimp_05"
    let ENEMIES_LITTLEIMP_LITTLIMP_06           = "Enemies/LittleImp/littlimp_06"
    let ENEMIES_LITTLEIMP_LITTLIMP_07           = "Enemies/LittleImp/littlimp_07"
    let ENEMIES_ONI_ONI_01                      = "Enemies/Oni/oni_01"
    let ENEMIES_ONI_ONI_02                      = "Enemies/Oni/oni_02"
    let ENEMIES_ONI_ONI_03                      = "Enemies/Oni/oni_03"
    let ENEMIES_SLURPEEIMP_SLURPEE_IMP_01       = "Enemies/SlurpeeImp/slurpee_imp_01"
    let ENEMIES_SLURPEEIMP_SLURPEE_IMP_02       = "Enemies/SlurpeeImp/slurpee_imp_02"
    let ENEMIES_SLURPEEIMP_SLURPEE_IMP_03       = "Enemies/SlurpeeImp/slurpee_imp_03"
    let ENEMIES_SLURPEEIMP_SLURPEE_IMP_04       = "Enemies/SlurpeeImp/slurpee_imp_04"
    let ENEMIES_WARNING                         = "Enemies/Warning"
    let ENEMIES_SKELETON_SKELETON_01            = "Enemies/skeleton/skeleton_01"
    let ENEMIES_SKELETON_SKELETON_02            = "Enemies/skeleton/skeleton_02"
    let ENEMIES_SKELETON_SKELETON_03            = "Enemies/skeleton/skeleton_03"
    let ENEMIES_SKELETON_SKELETON_04            = "Enemies/skeleton/skeleton_04"
    let HUD_HART                                = "HUD/hart"
    let HUD_HART_GONE                           = "HUD/hart_gone"
    let PLATFORMS_WORLD_1_PLATFORM_2_1          = "Platforms/world_1_platform_2-1"
    let PLATFORMS_WORLD_1_PLATFORM_2_2          = "Platforms/world_1_platform_2-2"
    let PLATFORMS_WORLD_1_PLATFORM_2_3          = "Platforms/world_1_platform_2-3"
    let PLATFORMS_WORLD_1_PLATFORM_2_4          = "Platforms/world_1_platform_2-4"
    let PLATFORMS_WORLD_1_PLATFORM_2_5          = "Platforms/world_1_platform_2-5"
    let PLATFORMS_WORLD_1_PLATFORM_3_1          = "Platforms/world_1_platform_3-1"
    let PLATFORMS_WORLD_1_PLATFORM_3_2          = "Platforms/world_1_platform_3-2"
    let PLATFORMS_WORLD_1_PLATFORM_3_3          = "Platforms/world_1_platform_3-3"
    let PLATFORMS_WORLD_1_PLATFORM_3_4          = "Platforms/world_1_platform_3-4"
    let PLATFORMS_WORLD_1_PLATFORM_3_5          = "Platforms/world_1_platform_3-5"
    let PLATFORMS_WORLD_1_PLATFORM_4_1          = "Platforms/world_1_platform_4-1"
    let PLATFORMS_WORLD_1_PLATFORM_4_2          = "Platforms/world_1_platform_4-2"
    let PLATFORMS_WORLD_1_PLATFORM_4_3          = "Platforms/world_1_platform_4-3"
    let PLATFORMS_WORLD_1_PLATFORM_4_4          = "Platforms/world_1_platform_4-4"
    let PLATFORMS_WORLD_1_PLATFORM_4_5          = "Platforms/world_1_platform_4-5"
    let PLATFORMS_WORLD_1_PLATFORM_5_1          = "Platforms/world_1_platform_5-1"
    let PLATFORMS_WORLD_1_PLATFORM_5_2          = "Platforms/world_1_platform_5-2"
    let PLATFORMS_WORLD_1_PLATFORM_5_3          = "Platforms/world_1_platform_5-3"
    let PLATFORMS_WORLD_1_PLATFORM_5_4          = "Platforms/world_1_platform_5-4"
    let PLATFORMS_WORLD_1_PLATFORM_5_5          = "Platforms/world_1_platform_5-5"
    let SCRIPTURES_SCRIPTURE_BIT_END            = "Scriptures/scripture_bit_end"
    let SCRIPTURES_SCRIPTURE_BIT_MIDDLE         = "Scriptures/scripture_bit_middle"
    let SCRIPTURES_SCRIPTURE_BIT_START          = "Scriptures/scripture_bit_start"
    let SUMMARY_MOON                            = "Summary/moon"
    let SUMMARY_MOON_BLACK                      = "Summary/moon-black"
    let SUMMARY_STAR                            = "Summary/star"
    let SUMMARY_STAR_BLACK                      = "Summary/star-black"
    let SUMMARY_SUN                             = "Summary/sun"
    let SUMMARY_SUN_BLACK                       = "Summary/sun-black"


    // load texture atlas
    let textureAtlas = SKTextureAtlas(named: "GameSheet")


    // individual texture objects
    func AngelMoroni_am_01() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_01) }
    func AngelMoroni_am_02() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_02) }
    func AngelMoroni_am_03() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_03) }
    func AngelMoroni_am_06() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_06) }
    func AngelMoroni_am_07() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_07) }
    func AngelMoroni_am_08() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_08) }
    func AngelMoroni_am_13() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_13) }
    func AngelMoroni_am_14() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_14) }
    func AngelMoroni_am_15() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_15) }
    func AngelMoroni_am_16() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_16) }
    func AngelMoroni_am_17() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_17) }
    func AngelMoroni_am_21() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_21) }
    func AngelMoroni_am_22() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_22) }
    func AngelMoroni_am_23() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_23) }
    func AngelMoroni_am_24() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_24) }
    func AngelMoroni_am_25() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_25) }
    func AngelMoroni_am_26() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_26) }
    func AngelMoroni_am_27() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_27) }
    func AngelMoroni_am_28() -> SKTexture                       { return textureAtlas.textureNamed(ANGELMORONI_AM_28) }
    func Clouds_cloud_b2_01() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B2_01) }
    func Clouds_cloud_b2_02() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B2_02) }
    func Clouds_cloud_b2_03() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B2_03) }
    func Clouds_cloud_b3_01() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B3_01) }
    func Clouds_cloud_b3_02() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B3_02) }
    func Clouds_cloud_b3_03() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B3_03) }
    func Clouds_cloud_b3_04() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B3_04) }
    func Clouds_cloud_b3_05() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B3_05) }
    func Clouds_cloud_b3_06() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B3_06) }
    func Clouds_cloud_b4_01() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B4_01) }
    func Clouds_cloud_b4_02() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B4_02) }
    func Clouds_cloud_b4_03() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B4_03) }
    func Clouds_cloud_b4_04() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B4_04) }
    func Clouds_cloud_b4_05() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B4_05) }
    func Clouds_cloud_b4_06() -> SKTexture                      { return textureAtlas.textureNamed(CLOUDS_CLOUD_B4_06) }
    func Clouds_sun() -> SKTexture                              { return textureAtlas.textureNamed(CLOUDS_SUN) }
    func Enemies_Goblin_goblin_01() -> SKTexture                { return textureAtlas.textureNamed(ENEMIES_GOBLIN_GOBLIN_01) }
    func Enemies_Goblin_goblin_02() -> SKTexture                { return textureAtlas.textureNamed(ENEMIES_GOBLIN_GOBLIN_02) }
    func Enemies_Goblin_goblin_03() -> SKTexture                { return textureAtlas.textureNamed(ENEMIES_GOBLIN_GOBLIN_03) }
    func Enemies_Goblin_goblin_04() -> SKTexture                { return textureAtlas.textureNamed(ENEMIES_GOBLIN_GOBLIN_04) }
    func Enemies_Imp_medimp_01() -> SKTexture                   { return textureAtlas.textureNamed(ENEMIES_IMP_MEDIMP_01) }
    func Enemies_Imp_medimp_02() -> SKTexture                   { return textureAtlas.textureNamed(ENEMIES_IMP_MEDIMP_02) }
    func Enemies_Imp_medimp_03() -> SKTexture                   { return textureAtlas.textureNamed(ENEMIES_IMP_MEDIMP_03) }
    func Enemies_JetpackGoblin_jetpack_goblin_01() -> SKTexture { return textureAtlas.textureNamed(ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_01) }
    func Enemies_JetpackGoblin_jetpack_goblin_02() -> SKTexture { return textureAtlas.textureNamed(ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_02) }
    func Enemies_JetpackGoblin_jetpack_goblin_03() -> SKTexture { return textureAtlas.textureNamed(ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_03) }
    func Enemies_JetpackGoblin_jetpack_goblin_04() -> SKTexture { return textureAtlas.textureNamed(ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_04) }
    func Enemies_JetpackGoblin_jetpack_goblin_05() -> SKTexture { return textureAtlas.textureNamed(ENEMIES_JETPACKGOBLIN_JETPACK_GOBLIN_05) }
    func Enemies_LittleImp_littlimp_01() -> SKTexture           { return textureAtlas.textureNamed(ENEMIES_LITTLEIMP_LITTLIMP_01) }
    func Enemies_LittleImp_littlimp_02() -> SKTexture           { return textureAtlas.textureNamed(ENEMIES_LITTLEIMP_LITTLIMP_02) }
    func Enemies_LittleImp_littlimp_03() -> SKTexture           { return textureAtlas.textureNamed(ENEMIES_LITTLEIMP_LITTLIMP_03) }
    func Enemies_LittleImp_littlimp_04() -> SKTexture           { return textureAtlas.textureNamed(ENEMIES_LITTLEIMP_LITTLIMP_04) }
    func Enemies_LittleImp_littlimp_05() -> SKTexture           { return textureAtlas.textureNamed(ENEMIES_LITTLEIMP_LITTLIMP_05) }
    func Enemies_LittleImp_littlimp_06() -> SKTexture           { return textureAtlas.textureNamed(ENEMIES_LITTLEIMP_LITTLIMP_06) }
    func Enemies_LittleImp_littlimp_07() -> SKTexture           { return textureAtlas.textureNamed(ENEMIES_LITTLEIMP_LITTLIMP_07) }
    func Enemies_Oni_oni_01() -> SKTexture                      { return textureAtlas.textureNamed(ENEMIES_ONI_ONI_01) }
    func Enemies_Oni_oni_02() -> SKTexture                      { return textureAtlas.textureNamed(ENEMIES_ONI_ONI_02) }
    func Enemies_Oni_oni_03() -> SKTexture                      { return textureAtlas.textureNamed(ENEMIES_ONI_ONI_03) }
    func Enemies_SlurpeeImp_slurpee_imp_01() -> SKTexture       { return textureAtlas.textureNamed(ENEMIES_SLURPEEIMP_SLURPEE_IMP_01) }
    func Enemies_SlurpeeImp_slurpee_imp_02() -> SKTexture       { return textureAtlas.textureNamed(ENEMIES_SLURPEEIMP_SLURPEE_IMP_02) }
    func Enemies_SlurpeeImp_slurpee_imp_03() -> SKTexture       { return textureAtlas.textureNamed(ENEMIES_SLURPEEIMP_SLURPEE_IMP_03) }
    func Enemies_SlurpeeImp_slurpee_imp_04() -> SKTexture       { return textureAtlas.textureNamed(ENEMIES_SLURPEEIMP_SLURPEE_IMP_04) }
    func Enemies_Warning() -> SKTexture                         { return textureAtlas.textureNamed(ENEMIES_WARNING) }
    func Enemies_skeleton_skeleton_01() -> SKTexture            { return textureAtlas.textureNamed(ENEMIES_SKELETON_SKELETON_01) }
    func Enemies_skeleton_skeleton_02() -> SKTexture            { return textureAtlas.textureNamed(ENEMIES_SKELETON_SKELETON_02) }
    func Enemies_skeleton_skeleton_03() -> SKTexture            { return textureAtlas.textureNamed(ENEMIES_SKELETON_SKELETON_03) }
    func Enemies_skeleton_skeleton_04() -> SKTexture            { return textureAtlas.textureNamed(ENEMIES_SKELETON_SKELETON_04) }
    func HUD_hart() -> SKTexture                                { return textureAtlas.textureNamed(HUD_HART) }
    func HUD_hart_gone() -> SKTexture                           { return textureAtlas.textureNamed(HUD_HART_GONE) }
    func Platforms_world_1_platform_2_1() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_2_1) }
    func Platforms_world_1_platform_2_2() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_2_2) }
    func Platforms_world_1_platform_2_3() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_2_3) }
    func Platforms_world_1_platform_2_4() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_2_4) }
    func Platforms_world_1_platform_2_5() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_2_5) }
    func Platforms_world_1_platform_3_1() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_3_1) }
    func Platforms_world_1_platform_3_2() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_3_2) }
    func Platforms_world_1_platform_3_3() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_3_3) }
    func Platforms_world_1_platform_3_4() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_3_4) }
    func Platforms_world_1_platform_3_5() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_3_5) }
    func Platforms_world_1_platform_4_1() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_4_1) }
    func Platforms_world_1_platform_4_2() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_4_2) }
    func Platforms_world_1_platform_4_3() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_4_3) }
    func Platforms_world_1_platform_4_4() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_4_4) }
    func Platforms_world_1_platform_4_5() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_4_5) }
    func Platforms_world_1_platform_5_1() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_5_1) }
    func Platforms_world_1_platform_5_2() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_5_2) }
    func Platforms_world_1_platform_5_3() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_5_3) }
    func Platforms_world_1_platform_5_4() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_5_4) }
    func Platforms_world_1_platform_5_5() -> SKTexture          { return textureAtlas.textureNamed(PLATFORMS_WORLD_1_PLATFORM_5_5) }
    func Scriptures_scripture_bit_end() -> SKTexture            { return textureAtlas.textureNamed(SCRIPTURES_SCRIPTURE_BIT_END) }
    func Scriptures_scripture_bit_middle() -> SKTexture         { return textureAtlas.textureNamed(SCRIPTURES_SCRIPTURE_BIT_MIDDLE) }
    func Scriptures_scripture_bit_start() -> SKTexture          { return textureAtlas.textureNamed(SCRIPTURES_SCRIPTURE_BIT_START) }
    func Summary_moon() -> SKTexture                            { return textureAtlas.textureNamed(SUMMARY_MOON) }
    func Summary_moon_black() -> SKTexture                      { return textureAtlas.textureNamed(SUMMARY_MOON_BLACK) }
    func Summary_star() -> SKTexture                            { return textureAtlas.textureNamed(SUMMARY_STAR) }
    func Summary_star_black() -> SKTexture                      { return textureAtlas.textureNamed(SUMMARY_STAR_BLACK) }
    func Summary_sun() -> SKTexture                             { return textureAtlas.textureNamed(SUMMARY_SUN) }
    func Summary_sun_black() -> SKTexture                       { return textureAtlas.textureNamed(SUMMARY_SUN_BLACK) }


    // texture arrays for animations
    func AngelMoroni_am_() -> [SKTexture] {
        return [
            AngelMoroni_am_01(),
            AngelMoroni_am_02(),
            AngelMoroni_am_03(),
            AngelMoroni_am_06(),
            AngelMoroni_am_07(),
            AngelMoroni_am_08(),
            AngelMoroni_am_13(),
            AngelMoroni_am_14(),
            AngelMoroni_am_15(),
            AngelMoroni_am_16(),
            AngelMoroni_am_17(),
            AngelMoroni_am_21(),
            AngelMoroni_am_22(),
            AngelMoroni_am_23(),
            AngelMoroni_am_24(),
            AngelMoroni_am_25(),
            AngelMoroni_am_26(),
            AngelMoroni_am_27(),
            AngelMoroni_am_28()
        ]
    }

    func Clouds_cloud_b2_() -> [SKTexture] {
        return [
            Clouds_cloud_b2_01(),
            Clouds_cloud_b2_02(),
            Clouds_cloud_b2_03()
        ]
    }

    func Clouds_cloud_b3_() -> [SKTexture] {
        return [
            Clouds_cloud_b3_01(),
            Clouds_cloud_b3_02(),
            Clouds_cloud_b3_03(),
            Clouds_cloud_b3_04(),
            Clouds_cloud_b3_05(),
            Clouds_cloud_b3_06()
        ]
    }

    func Clouds_cloud_b4_() -> [SKTexture] {
        return [
            Clouds_cloud_b4_01(),
            Clouds_cloud_b4_02(),
            Clouds_cloud_b4_03(),
            Clouds_cloud_b4_04(),
            Clouds_cloud_b4_05(),
            Clouds_cloud_b4_06()
        ]
    }

    func Enemies_Goblin_goblin_() -> [SKTexture] {
        return [
            Enemies_Goblin_goblin_01(),
            Enemies_Goblin_goblin_02(),
            Enemies_Goblin_goblin_03(),
            Enemies_Goblin_goblin_04()
        ]
    }

    func Enemies_Imp_medimp_() -> [SKTexture] {
        return [
            Enemies_Imp_medimp_01(),
            Enemies_Imp_medimp_02(),
            Enemies_Imp_medimp_03()
        ]
    }

    func Enemies_JetpackGoblin_jetpack_goblin_() -> [SKTexture] {
        return [
            Enemies_JetpackGoblin_jetpack_goblin_01(),
            Enemies_JetpackGoblin_jetpack_goblin_02(),
            Enemies_JetpackGoblin_jetpack_goblin_03(),
            Enemies_JetpackGoblin_jetpack_goblin_04(),
            Enemies_JetpackGoblin_jetpack_goblin_05()
        ]
    }

    func Enemies_LittleImp_littlimp_() -> [SKTexture] {
        return [
            Enemies_LittleImp_littlimp_01(),
            Enemies_LittleImp_littlimp_02(),
            Enemies_LittleImp_littlimp_03(),
            Enemies_LittleImp_littlimp_04(),
            Enemies_LittleImp_littlimp_05(),
            Enemies_LittleImp_littlimp_06(),
            Enemies_LittleImp_littlimp_07()
        ]
    }

    func Enemies_Oni_oni_() -> [SKTexture] {
        return [
            Enemies_Oni_oni_01(),
            Enemies_Oni_oni_02(),
            Enemies_Oni_oni_03()
        ]
    }

    func Enemies_SlurpeeImp_slurpee_imp_() -> [SKTexture] {
        return [
            Enemies_SlurpeeImp_slurpee_imp_01(),
            Enemies_SlurpeeImp_slurpee_imp_02(),
            Enemies_SlurpeeImp_slurpee_imp_03(),
            Enemies_SlurpeeImp_slurpee_imp_04()
        ]
    }

    func Enemies_skeleton_skeleton_() -> [SKTexture] {
        return [
            Enemies_skeleton_skeleton_01(),
            Enemies_skeleton_skeleton_02(),
            Enemies_skeleton_skeleton_03(),
            Enemies_skeleton_skeleton_04()
        ]
    }

    func Platforms_world_1_platform_2_() -> [SKTexture] {
        return [
            Platforms_world_1_platform_2_1(),
            Platforms_world_1_platform_2_2(),
            Platforms_world_1_platform_2_3(),
            Platforms_world_1_platform_2_4(),
            Platforms_world_1_platform_2_5()
        ]
    }

    func Platforms_world_1_platform_3_() -> [SKTexture] {
        return [
            Platforms_world_1_platform_3_1(),
            Platforms_world_1_platform_3_2(),
            Platforms_world_1_platform_3_3(),
            Platforms_world_1_platform_3_4(),
            Platforms_world_1_platform_3_5()
        ]
    }

    func Platforms_world_1_platform_4_() -> [SKTexture] {
        return [
            Platforms_world_1_platform_4_1(),
            Platforms_world_1_platform_4_2(),
            Platforms_world_1_platform_4_3(),
            Platforms_world_1_platform_4_4(),
            Platforms_world_1_platform_4_5()
        ]
    }

    func Platforms_world_1_platform_5_() -> [SKTexture] {
        return [
            Platforms_world_1_platform_5_1(),
            Platforms_world_1_platform_5_2(),
            Platforms_world_1_platform_5_3(),
            Platforms_world_1_platform_5_4(),
            Platforms_world_1_platform_5_5()
        ]
    }


}
