//
//  FadeInOutAnimationController.swift
//  AngelMoroni
//
//  Created by Justin Wright on 12/28/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import UIKit

class FadeInOutAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
  
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.35
  }
  
  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    // obtain state from the context
    let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
    let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
    let finalFrame = transitionContext.finalFrame(for: toViewController!)
    
    // obtain the container view
    let containerView = transitionContext.containerView
    
    // add the view and set initial state
    containerView.addSubview(toViewController!.view)
    toViewController?.view.frame = finalFrame
    toViewController?.view.alpha = 0.0
    
    let duration = transitionDuration(using: transitionContext) / 2
    
    UIView.animate(withDuration: duration, animations: { _ in
      fromViewController?.view.alpha = 0.0
      }, completion: { _ in
        UIView.animate(withDuration: duration, animations: { _ in
          toViewController?.view.alpha = 1.0
          }, completion: { _ in
            transitionContext.completeTransition(true)
        }) 
    }) 
    
  }
}
