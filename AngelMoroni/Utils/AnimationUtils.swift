//
//  AnimationUtils.swift
//  AngelMoroni
//
//  Created by Justin Wright on 11/28/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation
import QuartzCore

func fadeInOut(_ layer: CALayer?){
  guard let layer = layer else { return }
  let fade = CAKeyframeAnimation(keyPath: "opacity")
  fade.duration = 2.0
  fade.repeatCount = 0
  fade.autoreverses = false
  fade.values = [0.0, 1.0, 1.0, 0.0]
  fade.keyTimes = [0.0, 0.5, 0.5, 1.0]
  fade.fillMode = kCAFillModeBoth
  layer.add(fade, forKey: nil)
}

func fadeIn(_ duration: CGFloat, layer: CALayer?, complete:(()->())? ){
  guard let layer = layer else { return }
  
  let fade = CAKeyframeAnimation(keyPath: "opacity")
  fade.duration = CFTimeInterval(duration)
  fade.repeatCount = 0
  fade.autoreverses = false
  let d = NSNumber(value: Double(duration))
  fade.values = [0.0, d]
  //fade.keyTimes = [0.0, NSNumber(duration)]
  fade.fillMode = kCAFillModeBoth
  fade.keyTimes = [0, 1]
  GlobalMainQueue.async{
    layer.opacity = 1.0 // after animation runs need to save state of change.
    layer.add(fade, forKey: nil)
    delay(Double(duration)){
      complete?()
    }
  }
}

func fadeOut(_ duration: CGFloat, layer: CALayer?, complete:(()->())?){
  guard let layer = layer else { return }
  layer.opacity = 0.0
  let fade = CAKeyframeAnimation(keyPath: "opacity")
  fade.duration = CFTimeInterval(duration)
  fade.repeatCount = 0
  fade.autoreverses = false
  fade.values = [1.0, 0.0]
  let d = NSNumber(value: Double(duration))
  fade.keyTimes = [0.0, d]
  fade.fillMode = kCAFillModeBoth
  GlobalMainQueue.async{
    layer.opacity = 0.0 // after animation runs need to save state of change.
    layer.add(fade, forKey: nil)
    delay(Double(duration)){
      complete?()
    }
  }
}
