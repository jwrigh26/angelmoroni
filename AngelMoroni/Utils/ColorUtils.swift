//
//  ColorUtils.swift
//  XtraPoint
//
//  Created by Wright, Justin M on 3/13/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import Foundation
import UIKit

class ColorUtils : NSObject {

  // rbgValue - define hex color value
  // alpha - define transparency value
  // returns - CGColor
  // example
  // UIColorFromHex(0x444444, alpha: 0.7)
  static func colorFromHex(_ rgbValue:UInt32, alpha:Double=1.0)->UIColor {
    let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
    let blue = CGFloat(rgbValue & 0xFF)/256.0
    
    return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
  }
  
  func getImageWithColor(_ color: UIColor, size: CGSize) -> UIImage {
    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    UIGraphicsBeginImageContextWithOptions(size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
  }
  
  func get1x1SquareWithColor(_ color: UIColor)->UIImage {
    return getImageWithColor(color, size: CGSize(width: 1, height: 1))
  }
  
  
}



// For Images
//extension UIImage {
//  func imageWithColor(tintColor: UIColor) -> UIImage {
//    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
//    
//    let context = UIGraphicsGetCurrentContext() as CGContextRef
//    CGContextTranslateCTM(context, 0, self.size.height)
//    CGContextScaleCTM(context, 1.0, -1.0);
//    CGContextSetBlendMode(context, CGBlendMode.Normal)
//    
//    let rect = CGRectMake(0, 0, self.size.width, self.size.height) as CGRect
//    CGContextClipToMask(context, rect, self.CGImage)
//    tintColor.setFill()
//    CGContextFillRect(context, rect)
//    
//    let newImage = UIGraphicsGetImageFromCurrentImageContext() as UIImage
//    UIGraphicsEndImageContext()
//    
//    return newImage
//  }
//  
//  class func imageWithColor(color:UIColor)->UIImage {
//    let rect = CGRectMake(0, 0, 1.0, 1.0)
//    UIGraphicsBeginImageContext(rect.size)
//    let context = UIGraphicsGetCurrentContext()
//    CGContextSetFillColorWithColor(context, color.CGColor)
//    CGContextFillRect(context, rect)
//    let image = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//    return image
//  }
//}
