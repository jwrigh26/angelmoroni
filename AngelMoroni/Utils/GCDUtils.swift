//
//  GCDUtils.swift
//  HelloWorldSwift
//
//  Created by Wright, Justin M on 9/16/15.
//  Copyright © 2015 Wright, Justin M. All rights reserved.
//

import Foundation

var GlobalMainQueue: DispatchQueue {
  return DispatchQueue.main
}


/*
 * GlobalUserInteractiveQueue
 *
 * A QOS class which indicates work performed by this thread
 * is interactive with the user.
 */
var GlobalUserInteractiveQueue: DispatchQueue {
  return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
}

/*
 * GlobalUserInitiatedQueue
 *
 * A QOS class which indicates work performed by this thread
 * was initiated by the user and that the user is likely waiting for the
 * results.
 */
var GlobalUserInitiatedQueue: DispatchQueue {
  return DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated)
}


/*
 * GlobalUtilityQueue
 *
 * A QOS class which indicates work performed by this thread
 * may or may not be initiated by the user and that the user is unlikely to be
 * immediately waiting for the results.
 */
var GlobalUtilityQueue: DispatchQueue {
  return DispatchQueue.global(qos: DispatchQoS.QoSClass.utility)
}

/*
 * GlobalBackgroundQueue
 *
 * A QOS class which indicates work performed by this thread was not
 * initiated by the user and that the user may be unaware of the results.
 */
var GlobalBackgroundQueue: DispatchQueue {
  return DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
}


func createSerialQueue(_ name: String)-> DispatchQueue {
  return DispatchQueue(label: name, attributes: [])
}

func createConCurrentQueue(_ name: String)-> DispatchQueue {
  return DispatchQueue(label: name, attributes: DispatchQueue.Attributes.concurrent)
}

func delay(_ delay: Double, closure: @escaping ()->()){
  GlobalMainQueue.asyncAfter(
    deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

// Inspired by https://gist.github.com/maicki/7622108
// This is a swiftier version
class GCDTimer {
  
  typealias TimerHandler = (_ timer: GCDTimer?) -> Void
  
  var _timer: DispatchSource?
  var queue = GlobalBackgroundQueue
  var handler: TimerHandler?
  
  init(queue: DispatchQueue, handler: @escaping TimerHandler){
    self.queue = queue
    self.handler = handler
  }
  
  func createDispatchTimer(interval i: Double, queue: DispatchQueue) -> DispatchSource?{
    
    //let iInt64 = Int64(i * Double(NSEC_PER_SEC))
    //let iUnt64 = UInt64(iInt64)
    //let ull: CUnsignedLongLong = 1
    //let dispatchTime = DispatchTime.now() + Double(iInt64) / Double(NSEC_PER_SEC)//
    let timer: DispatchSource = DispatchSource.makeTimerSource(flags: DispatchSource.TimerFlags(rawValue: UInt(0)), queue: queue) as! DispatchSource
    //let seconds = Int((ull * NSEC_PER_SEC) / 10)
    //let leeway  = DispatchTimeInterval.seconds(seconds)
    //timer.scheduleOneshot(deadline: dispatchTime, leeway: leeway)
    //timer.setTimer(start: dispatchTime, interval: iUnt64, leeway: (ull * NSEC_PER_SEC)/10)
    //timer.set
    //timer.setEventHandler(handler: block)
    
    timer.scheduleRepeating(deadline: DispatchTime.now(), interval: i)
    
    //let worker = DispatchWorkItem(qos: queue, flags: <#T##DispatchWorkItemFlags#>, block: <#T##() -> ()#>)
    timer.setEventHandler { [weak self] in
      self?.handler?(self)
    }
    
    timer.resume()
    return timer
  }
  
  func startTimer(_ interval: Double = 1.0){
    _timer = createDispatchTimer(interval: interval, queue: self.queue)
  }
  
  func cancelTimer(){
    guard let timer = _timer else { return }
    timer.cancel()
    _timer = nil
  }
}
