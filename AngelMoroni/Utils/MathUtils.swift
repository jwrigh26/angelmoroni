//
//  MathUtils.swift
//  AngelMoroni
//
//  Created by Wright, Justin M on 6/21/15.
//  Copyright (c) 2015 Wright, Justin M. All rights reserved.
//

import Foundation
import CoreGraphics

func random(min: Int, max: Int) -> Int {
  let number = ceil(fabs(CGFloat.random(min: CGFloat(min-1), max: CGFloat(max))))
  return Int(number)
}

func random(min: CGFloat, max: CGFloat) -> CGFloat {
  let number = ceil(fabs(CGFloat.random(min: CGFloat(min-1), max: CGFloat(max))))
  return number
}

func roundToHundredth(_ x: CGFloat) -> CGFloat {
  return round(x * 100.0) / 100.0
}


#if !(arch(x86_64) || arch(arm64))
  func atan2(y: CGFloat, x: CGFloat) -> CGFloat {
    return CGFloat(atan2f(Float(y), Float(x)))
  }
  
  func sqrt(a: CGFloat) -> CGFloat {
    return CGFloat(sqrtf(Float(a)))
  }
#endif






