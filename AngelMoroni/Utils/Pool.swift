//
//  Pool.swift
//  HelloWorldSwift
//
//  Created by Wright, Justin M on 9/16/15.
//  Copyright © 2015 Wright, Justin M. All rights reserved.
//

import Foundation

class Pool<T> {
  fileprivate var data = [T]()
  fileprivate let poolQ = createSerialQueue("poolQ")
  //private let semaphore: dispatch_semaphore_t
  
  init(items:[T]){
    data.reserveCapacity(items.count)
    for item in items {
      data.append(item)
    }
    //semaphore = dispatch_semaphore_create(items.count)
  }
  
  func getFromPool()-> T? {
    var result:T?
   
    if data.count > 0 {
    //if dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER) == 0{
      poolQ.sync(execute: {() in
        result = self.data.remove(at: 0)
      })
    }
    
    return result
  }
  
  func returnToPool(_ item: T) {
    poolQ.async(flags: .barrier, execute: {() in
      self.data.append(item)
      //dispatch_semaphore_signal(self.semaphore)
    })
  }
  
  var count: Int {
    return data.count
  }
  
}
