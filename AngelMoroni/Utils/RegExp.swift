//
//  RegExp.swift
//  AngelMoroni
//
//  Created by Justin Wright on 10/24/15.
//  Copyright © 2015 Justin Wright. All rights reserved.
//

import Foundation

enum RegExpError: Error {
  case syntax
}

struct RegExp {
  
  
  var regexp: NSRegularExpression!
  
  // Failable init - will return nil if regexp syntax is misformed
  init?(pattern: String, options: NSRegularExpression.Options){
    do {
      regexp = try NSRegularExpression(pattern: pattern, options: options)
    } catch {
      print("Regular Expression Failed")
      print(error)
      return nil
    }
  }
  
  // Simple boolean match test
  func isMatching(_ string: String) -> Bool {
    let allStringRange = fullRangeForString(string)
    let nbMathes = regexp.numberOfMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: allStringRange)
    return nbMathes > 0
  }
  
  func isMatching(_ range: NSRange, string: String) -> Bool {
    let nbMathes = regexp.numberOfMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: range)
    return nbMathes > 0
  }
  
  
  // Returns the first matching part of a String
  func match(_ string: String) -> (Bool, String?) {
    
    let allStringRange = fullRangeForString(string)
    
    if let res = regexp.firstMatch(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: allStringRange) {
      let stringAsNS = string as NSString
      let firstMatch = stringAsNS.substring(with: res.range)
      return (true, firstMatch)
    }else{
      return (false, nil)
    }
    
  }
  
  
  
  // Returns all matches (including capture groups) as an array of String
  func allMatches(_ string: String) -> [String] {
    
    var matches = [String]()
    
    let stringAsNS = string as NSString
    
    regexp.enumerateMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: fullRangeForString(string)){
      (textCheckingResult: NSTextCheckingResult?, flags: NSRegularExpression.MatchingFlags, stop: UnsafeMutablePointer<ObjCBool>) in
      
      for i in 0..<textCheckingResult!.numberOfRanges {
        
        let subMatch = stringAsNS.substring(with: textCheckingResult!.rangeAt(i))
        matches.append(subMatch as String)
        
      }
      
    }
    return matches
  }
  
  
  
  func findRangeOfAllMatches(_ string: String) -> [NSRange?]{
    
    var matches = [NSRange?]()
    
    regexp.enumerateMatches(in: string, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: fullRangeForString(string)) { (result:NSTextCheckingResult?, flags:NSRegularExpression.MatchingFlags, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
      
      // TODO: Come back here an evaluate if we need to itterate of number of ranges
      //for _ in 0..<result!.numberOfRanges {
        
        //print("Match at location = \(result?.range.location), length = \(result?.range.length)")
        matches.append(result?.range)
      //}
    }
    return matches
  }
  
  
  fileprivate func fullRangeForString(_ string: String) -> NSRange {
    return NSRange(location: 0, length: string.characters.count)
  }
  
}

infix operator =~

func =~ (left: String, right: RegExp) -> (Bool, String?) {
  return right.match(left)
}

func =~ (left: RegExp, right: String) -> (Bool, String?) {
  return left.match(right)
}


