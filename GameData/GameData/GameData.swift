//
//  World1.swift
//  GameData
//
//  Created by Justin Wright on 9/27/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift


enum JsonValue {
  static let scriptures = "scriptures"
  static let book = "book"
  static let chapter = "chapter"
  static let verse = "verse"
  static let text = "text"
  static let bits = "bits"
  static let volume = "volume"
}

class GameData {
  
  var title: String!
  var order: Int
  var isLocked: Bool = false
  
  var scriptures = [Scripture]()
  var levels = [Level]()
  
  lazy var world: World = {
    let obj = World()
    obj.id = UUID().uuidString
    obj.isLocked = self.isLocked
    obj.title = self.title
    obj.order = self.order
    return obj
  }()
  
  init(worldTitle title: String, worldNumber order: Int, locked: Bool = true){
    self.title = title
    self.order = order
    self.isLocked = locked
    
  }
  
  func createLevels(jsonfile file: String){
    makeScriptures(file: file)
    makeLevels()
  }
  
  
  
  func makeScriptures(file: String){
    guard let path = Bundle.main.path(forResource: file, ofType: "json") else {
     print("Error with parsing")
      return
    }
    
    let data = try! Data(contentsOf: URL(fileURLWithPath: path), options: NSData.ReadingOptions.mappedIfSafe)
    let json = JSON(data: data, options: JSONSerialization.ReadingOptions.allowFragments)
    
    let count = json[JsonValue.scriptures].count
    //print("Count = \(count)")
    
    for i in 0..<count {
      let obj = json[JsonValue.scriptures][i]
      
      let book = obj[JsonValue.book].stringValue
      let chapter = obj[JsonValue.chapter].intValue
      let verse = obj[JsonValue.verse].intValue
      let text = obj[JsonValue.text].stringValue
      let bits = obj[JsonValue.bits].stringValue
      let volume = obj[JsonValue.volume].stringValue
      
      
      let scripture = Scripture()
      scripture.id = UUID().uuidString
      scripture.book = book
      scripture.chapter = chapter
      scripture.verse = verse
      scripture.text = text
      scripture.bitText = bits
      scripture.volume = volume
      scriptures.append(scripture)
      
    }
  }
  
  func makeLevels(){
    let count = scriptures.count
    for i in 0..<count {
      let level = Level()
      level.id = UUID().uuidString
      level.order = i
      level.scripture = scriptures[i]
      
      world.levels.append(level)
    }
  }
}
