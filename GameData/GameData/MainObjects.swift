//
//  MainObjects.swift
//  GameData
//
//  Created by Justin Wright on 10/2/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift

class User : Object {
  dynamic var id = "1"
  
  dynamic var name = ""
  
  let levels = List<LevelData>()
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  override static func indexedProperties() -> [String] {
    return ["name"]
  }
}


class LevelData: Object {
  dynamic var id = UUID().uuidString
  dynamic var levelKey = ""
  dynamic var worldKey = ""
  dynamic var degreeOfGlory = 0
  dynamic var complete = false
  dynamic var bitsCollected = 0
  dynamic var harts = 0
  
  let user = LinkingObjects(fromType: User.self, property: "levels")
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  override static func indexedProperties() -> [String] {
    return ["levelKey, worldKey"]
  }
}
