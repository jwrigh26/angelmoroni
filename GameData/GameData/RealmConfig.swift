//
//  RealmConfig.swift
//  GameData
//
//  Created by Justin Wright on 10/2/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift

var realmDefaultUserName = "defaultuser_v1.0"
var copyMainDataRunOnce: Void = {
  ConfigHelper.copyInitialData(from: Bundle.main.url(forResource: realmDefaultUserName, withExtension: "realm")!, to: RealmConfig.staticConfig.fileURL!)
}()

enum RealmConfig {
  
  fileprivate static let staticConfig =  Realm.Configuration(
    fileURL: Bundle.main.url(forResource: "static_v1.0", withExtension: "realm"),
    readOnly: true,
    objectTypes: [World.self, Level.self, Scripture.self]
  )
  
  fileprivate static func mainConfig(userName: String) -> Realm.Configuration {
    return Realm.Configuration(
      fileURL: URL.inDocumentsFolder(fileName: userName),
      schemaVersion: 1,
      objectTypes: [User.self, LevelData.self]
    )
  }
  
  case main(userName: String), readOnly
  
  var configuration: Realm.Configuration {
    switch self {
    case .main(let userName):
      
      // find out if current signed in user realm file exist.
      if !isFileReachable(userName: userName) {
        print("It is not reachable so we are creating from default user")
        
        
        // _ = copyMainDataRunOnce
        
        
        // This is just for making the game data
        var config = Realm.Configuration()
        config.fileURL = config.fileURL?.deletingLastPathComponent().appendingPathComponent("\(realmDefaultUserName).realm")
        Realm.Configuration.defaultConfiguration = config
        return Realm.Configuration.defaultConfiguration
      }
      
      return RealmConfig.mainConfig(userName: userName)
    
      
    case .readOnly:
      
      //return RealmConfig.staticConfig
      
      
      // This is just for making the game data
      var config = Realm.Configuration()
      config.fileURL = config.fileURL?.deletingLastPathComponent().appendingPathComponent("static_v1.0.realm")
      Realm.Configuration.defaultConfiguration = config
      return Realm.Configuration.defaultConfiguration
    }
  }
  
  
  
  
  
  private func isFileReachable(userName: String) -> Bool {
    
    let url = URL.inDocumentsFolder(fileName: "\(userName).realm")
    let boo = ConfigHelper.isFileReachable(url: url)
    print("Is File Reachable for \(userName)? It is Reachable = \(boo)")
    return boo
  }
  
}




struct ConfigHelper {
  
  static func isFileReachable(url fileURL: URL) -> Bool {
    var isReachable = false
    do {
      if try fileURL.checkPromisedItemIsReachable()  {
        isReachable = true
      }
    }catch{
      //print("CheckPromisedItem failed \(error)")
      print("Data is not Reachable!")
    }
    return isReachable
  }
  
  
  static func copyInitialData(from: URL, to fileURL: URL){
    guard !ConfigHelper.isFileReachable(url: fileURL) else { return }
    
    do {
      _ = try? FileManager.default.removeItem(at: fileURL as URL)
      try FileManager.default.copyItem(at: from as URL, to: fileURL as URL)
      
      print("We are copying the item over")
      
    }catch {
      print("Could not copy config file over \(error)")
    }
  }
  
}


extension URL {
  static func inDocumentsFolder(fileName: String) -> URL {
    let url = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0], isDirectory: true)
      .appendingPathComponent(fileName)
    return url
  }
}
