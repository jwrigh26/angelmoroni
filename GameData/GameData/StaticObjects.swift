//
//  StaticObjects.swift
//  GameData
//
//  Created by Justin Wright on 9/27/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import Foundation
import RealmSwift

class World : Object {
  dynamic var id = UUID().uuidString
  
  dynamic var isLocked = true
  dynamic var title = ""
  dynamic var order = 0
  let levels = List<Level>()
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  override static func indexedProperties() -> [String] {
    return ["title", "order"]
  }
  
  var image: String {
    return "world_\(order)_image"
  }
}

class Level : Object {
  dynamic var id = UUID().uuidString
  dynamic var scripture: Scripture? = nil
  dynamic var order = 0
  
  let world = LinkingObjects(fromType: World.self, property: "levels")
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  override static func indexedProperties() -> [String] {
    return ["order"]
  }
  
  // Read only
  lazy var title: String = {
    guard let book = self.scripture?.book, let chapter = self.scripture?.chapter, let verse = self.scripture?.verse else { return "" }
    return "\(book) \(chapter):\(verse)"
  }()
  
  override static func ignoredProperties() -> [String] {
    return ["title"]
  }
}

class Scripture : Object {
  dynamic var id = UUID().uuidString
  dynamic var volume = ""
  dynamic var book = ""
  dynamic var chapter = 0
  dynamic var verse = 0
  dynamic var text = ""
  dynamic var bitText = ""
  
  override static func primaryKey() -> String? {
    return "id"
  }
  
  // Total count minus brakes in scripture
  lazy var totalBits: Int = {
    // Filter out </br> for total bits
    let array = self.bits.filter{ $0 != "<br/>" }
    return array.count
  }()
  
  // Array of bits of the scripture
  lazy var bits: [String] = {
    let array = self.bitText.components(separatedBy: "%")
    return array
  }()
  
  override static func ignoredProperties() -> [String] {
    return ["totalBits", "bits"]
  }
  
}
