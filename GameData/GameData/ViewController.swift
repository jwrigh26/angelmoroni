//
//  ViewController.swift
//  GameData
//
//  Created by Justin Wright on 9/27/16.
//  Copyright © 2016 Justin Wright. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

  
  override func viewDidLoad() {
    super.viewDidLoad()
    print(Realm.Configuration.defaultConfiguration.fileURL!)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func createMainRealm(){
    
    let defaultUser = User()
    defaultUser.name = realmDefaultUserName
    let username = defaultUser.name
    
//    var config = Realm.Configuration()
//    config.fileURL = config.fileURL?.deletingLastPathComponent().appendingPathComponent("\(username).realm")
    
    let realm = try! Realm(configuration: RealmConfig.main(userName: username).configuration)
    
    try! realm.write {
      realm.add(defaultUser)
    }
    
    let user = realm.objects(User.self).first
    print("User Name = \(user?.name)")
    print("User Name primarykey = \(user?.id)")
    
  }

  func createStaticRealm() {
    // Going to generate all realm for game here
    
    // TODO: Order these starting at zero
    
    let world1 = GameData(worldTitle: "Cloud Ruins", worldNumber: 0, locked: false)
    world1.createLevels(jsonfile: "world1")
    
    let world2 = GameData(worldTitle: "Sky Castle", worldNumber: 1)
    let world3 = GameData(worldTitle: "Forest of Thieves", worldNumber: 2)
    //let world4 = GameData(worldTitle: "Red Sea Peril", worldNumber: 4)
    //let world5 = GameData(worldTitle: "Stratosphere Surfing", worldNumber: 5)
    
    //var config = Realm.Configuration()
    //config.fileURL = config.fileURL?.deletingLastPathComponent().appendingPathComponent("static.realm")
    //Realm.Configuration.defaultConfiguration = config
    
    let realm = try! Realm(configuration: RealmConfig.readOnly.configuration)
    
    
    
    try! realm.write {
      print("Writing code")
      
      realm.add(world1.world)
      world1.scriptures.forEach{ realm.add($0) }
      world1.levels.forEach{ realm.add($0) }
      
      realm.add(world2.world)
      realm.add(world3.world)
    }
    
    testStaticRealm()
  }
  
  func testStaticRealm(){
    print("---- Test Static Realm ----\n")
    let realm = try! Realm()
    let worlds = realm.objects(World.self)
    for world in worlds {
      print("World: \(world.title) and Order: \(world.order)")
    }
    print("")
    
    let result = realm.objects(World.self).filter("order = 1")
    let count = result.first!.levels.count
    let levels = result.first!.levels
    print("World 1: \(result.first?.title) has Levels: \(count)")
    print("")
    print("<--------Print out Levels -------------->")
    print("")
    
    for level in levels {
      print("Level \(level.order) Title: \(level.title) and World: \(level.world.first!.title)")
      print(level.scripture!.text)
      print("Total Bits \(level.scripture!.totalBits) and bit count \(level.scripture!.bits.count)")
      print("")
    }
  }
  
  @IBAction func createStatic(_ sender: AnyObject) {
    print("Create")
    createStaticRealm()
  }
  
  
  
  @IBAction func createMain(_ sender: AnyObject) {
    createMainRealm()
    
  }

}

